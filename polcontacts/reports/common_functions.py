# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from os import path, sep

import frappe
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import stringWidth, registerFont
from PIL import Image
from time import strftime
import re
from frappe import _

# **Funcio per importar les fonts**

def fonts_import():

    fonts_directory = path.dirname(path.abspath(__file__)) + sep + 'Fonts'

    registerFont(TTFont('OpenSans-Regular', path.join(fonts_directory, 'OpenSans-Regular.ttf')))
    registerFont(TTFont('OpenSans-Bold', path.join(fonts_directory, 'OpenSans-Bold.ttf')))
    registerFont(TTFont('OpenSans-Italic', path.join(fonts_directory, 'OpenSans-Italic.ttf')))
    registerFont(TTFont('OpenSans-BoldItalic', path.join(fonts_directory, 'OpenSans-BoldItalic.ttf')))

# **Function to generate header data**

def header_data(doctype, report_title):
    ## Import Docs
    association = frappe.get_doc("Association", doctype.association)
    federation = frappe.get_doc("Federation", doctype.federation)

    # Header data
    header_data = dict()
    header_data["tournament_name"] = doctype.tournament_name
    header_data["association_name"] = association.association_name
    header_data["tournament_date"] = doctype.tournament_date
    header_data["document_name"] = report_title
    header_data["association_logo"] = association.logo
    header_data["federation_logo"] = federation.logo
    return header_data

# **Funcio per dibuixar capcalera i titol columnes**

def draw_header(canvas, data):

    # **Guardem la alcada i la amplada total de la pagina**

    amplada, height = A4

    # **Dibuixem el quadre exterior**
    canvas.saveState()
    canvas.rect(data["report_configuration"].left_margin * cm, height - data["report_configuration"].upper_margin * cm - 75, amplada - data["report_configuration"].left_margin * cm - data["report_configuration"].right_margin * cm, 75, fill=0)

    # **Dibuixem els logos**
    alcadaLogos = min((data["report_configuration"].logo_size, 65))

    # Carreguem logo Esquerra
    logo_not_found = False
    if data["report_configuration"].left_logo == "Federation Logo":
        try:
            logo_1_directory = frappe.get_site_path() + "/" +  str(data["header"]["federation_logo"])
            logo_1 = Image.open(logo_1_directory)
        except:
            logo_not_found = True
    elif data["report_configuration"].left_logo == "Association Logo":
        try:
            logo_1_directory = frappe.get_site_path() + "/" +  str(data["header"]["association_logo"])
            logo_1 = Image.open(logo_1_directory)
        except:
            logo_not_found = True
    elif data["report_configuration"].left_logo == "Logo 1":
        try:
            logo_1_directory = frappe.get_site_path() + "/" +  str(data["report_configuration"].logo_1)
            logo_1 = Image.open(logo_1_directory)
        except:
            logo_not_found = True
    else:
        try:
            logo_1_directory = frappe.get_site_path() + "/" +  str(data["report_configuration"].logo_2)
            logo_1 = Image.open(logo_1_directory)
        except:
            logo_not_found = True

    # Dibuixem logo Esquerra
    if logo_not_found:
        canvas.drawCentredString(data["report_configuration"].left_margin * cm + 75/2 + 5, height - data["report_configuration"].upper_margin * cm - 75/2 - 1, _('blank'))
    else:
        ampladaLogo1, alcadaLogo1 = logo_1.size
        ampladaLogo1 = alcadaLogos * ampladaLogo1 / alcadaLogo1
        canvas.drawImage(logo_1_directory, data["report_configuration"].left_margin * cm + 5, height - data["report_configuration"].upper_margin * cm - 75/2 - alcadaLogos/2 - 1, height=alcadaLogos, width=ampladaLogo1)

    # Logo Dret
    logo_not_found = False
    if data["report_configuration"].right_logo == "Federation Logo":
        try:
            logo_2_directory = frappe.get_site_path() + "/" + str(data["header"]["federation_logo"])
            logo_2 = Image.open(logo_2_directory)
        except:
            logo_not_found = True
    elif data["report_configuration"].right_logo == "Association Logo":
        try:
            logo_2_directory = frappe.get_site_path() + "/" + str(data["header"]["association_logo"])
            logo_2 = Image.open(logo_2_directory)
        except:
            logo_not_found = True
    elif data["report_configuration"].right_logo == "Logo 1":
        try:
            logo_2_directory = frappe.get_site_path() + "/" + str(data["report_configuration"].logo_1)
            logo_2 = Image.open(logo_2_directory)
        except:
            logo_not_found = True
    else:
        try:
            logo_2_directory = frappe.get_site_path() + "/" + str(data["report_configuration"].logo_2)
            logo_2 = Image.open(logo_2_directory)
        except:
            logo_not_found = True
            
    # Dibuixem logo Dret
    if logo_not_found:
        canvas.drawCentredString(amplada - data["report_configuration"].right_margin * cm - 75/2 - 5, height - data["report_configuration"].upper_margin * cm - 75/2 - 1, _('blank'))
    else:
        ampladaLogo2, alcadaLogo2 = logo_2.size
        ampladaLogo2 = alcadaLogos * ampladaLogo2 / alcadaLogo2
        canvas.drawImage(logo_2_directory, amplada - data["report_configuration"].right_margin * cm - ampladaLogo2 - 5, height - data["report_configuration"].upper_margin * cm - 75/2 - alcadaLogos/2 - 1, height=alcadaLogos, width=ampladaLogo2)

    # **Dibuixem els textes de capcalera**
    # Si volem imprimir el dia actual automaticament, substituir textesCapcalera[2] per strftime("%d/%m/%Y")
#    canvas.setFont('FreeSerifBold', 12)
    canvas.setFont('Helvetica-Bold', 12)
    canvas.drawCentredString(data["report_configuration"].left_margin * cm + (amplada - data["report_configuration"].left_margin * cm - data["report_configuration"].right_margin * cm) / 2, height - data["report_configuration"].upper_margin * cm - 20, data["header"]["tournament_name"].upper())
#    canvas.setFont('FreeSerif', 12)
    canvas.setFont('Helvetica', 12)
    canvas.drawCentredString(data["report_configuration"].left_margin * cm + (amplada - data["report_configuration"].left_margin * cm - data["report_configuration"].right_margin * cm) / 2, height - data["report_configuration"].upper_margin * cm - 35, data["header"]["association_name"].upper())
#    canvas.setFont('FreeSerif', 11)
    canvas.setFont('Helvetica', 11)
    canvas.drawCentredString(data["report_configuration"].left_margin * cm + (amplada - data["report_configuration"].left_margin * cm - data["report_configuration"].right_margin * cm) / 2, height - data["report_configuration"].upper_margin * cm - 50, str(data["header"]["tournament_date"]))
#    canvas.setFont('FreeSerif', 11)
    canvas.setFont('Helvetica', 11)
    canvas.drawCentredString(data["report_configuration"].left_margin * cm + (amplada - data["report_configuration"].left_margin * cm - data["report_configuration"].right_margin * cm) / 2, height - data["report_configuration"].upper_margin * cm - 65, data["header"]["document_name"])

    canvas.restoreState()

    return data["report_configuration"].upper_margin * cm + 75


# **Funcio per dibuixar peu pagina**

def draw_footer(canvas, data, totalPagines):

    # **Guardem la alcada i la amplada total de la pagina**

    amplada, height = A4

    # **Dibuixem linia de peu de pagina**

    canvas.line(data["report_configuration"].left_margin * cm, data["report_configuration"].lower_margin * cm + data["report_configuration"].footer_height * cm - 4, amplada - data["report_configuration"].right_margin * cm, data["report_configuration"].lower_margin * cm + data["report_configuration"].footer_height * cm - 4)

    # **Dibuixem els textes del peu de pagina**

    #canvas.setFont('FreeSerif', min((data["report_configuration"].footer_text_size, data["report_configuration"].footer_height - 1)))
    canvas.setFont('Helvetica', min((data["report_configuration"].footer_text_size, data["report_configuration"].footer_height * cm - 1)))
    canvas.drawString(data["report_configuration"].left_margin * cm, data["report_configuration"].lower_margin * cm + data["report_configuration"].footer_height * cm / 2 - data["report_configuration"].footer_text_size * 0.15, _('Document generated with AVIUM'))
    canvas.drawRightString(amplada - data["report_configuration"].right_margin * cm, data["report_configuration"].lower_margin * cm + data["report_configuration"].footer_height * cm / 2 - data["report_configuration"].footer_text_size * 0.15, _('Page') + ' ' + str(canvas.getPageNumber()))
#    canvas.drawRightString(amplada - data["report_configuration"].right_margin * cm, data["report_configuration"].lower_margin * cm + data["report_configuration"].footer_height * cm / 2 - data["report_configuration"].footer_text_size * 0.15, _('Page') + ' ' + str(canvas.getPageNumber()) + '/' + str(totalPagines))


# **Funcio per ordenar una llista o tupla per lletra i numero**

def ordenarLletraNumero(data):

    dataSplit = []
    dataSorted = []
    counter = 0

    for i in data:
        dataSplit.append((re.findall('[^0-9]+', i), re.findall('[0-9]+', i), counter))
        counter += 1

    dataSplitSortedNumero = sorted(dataSplit, key=lambda xifra: int(xifra[1][0]))

    dataSplitSortedLletraNumero = sorted(dataSplitSortedNumero, key=lambda lletra: lletra[0])

    for i in dataSplitSortedLletraNumero:
        dataSorted.append(data[i[2]])

    return dataSorted


# **Funcio per ordenar una llista o tupla per numero i lletra**

def ordenarNumeroLletra(data):

    dataSplit = []
    dataSorted = []
    counter = 0

    for i in data:
        dataSplit.append((re.findall('[^0-9]+', i), re.findall('[0-9]+', i), counter))
        counter += 1

    dataSplitSortedLletra = sorted(dataSplit, key=lambda lletra: lletra[0])

    dataSplitSortedNumeroLletra = sorted(dataSplitSortedLletra, key=lambda xifra: int(xifra[1][0]))

    for i in dataSplitSortedNumeroLletra:
        dataSorted.append(data[i[2]])

    return dataSorted


# **Cut text to fit an specific size**

def text_truncate(text, text_width, font, size):
    if text==None:
        return ""

    truncated_text = ''

    for letter_index in range(len(text)):

        if stringWidth(text[:letter_index+1], font, size) <= text_width - 2.25:
            truncated_text += text[letter_index]
        else:
            if truncated_text[-1] != ' ':
                truncated_text += '.'
            break

    return truncated_text


# **Split text in lines**

def split_text_in_lines(text, lines_width, font, size):

    words = text.split()

    lines = []
    line = ''

    for word in words:
        if line == '':
            line += word
        else:
            if stringWidth(line + ' ' + word, font, size) <= lines_width:
                line += ' ' + word
            else:
                lines.append(line)
                line = word

    lines.append(line)

    return lines

#None to empty
def n_e(str):
    if str is None:
        return ""
    else:
        return str