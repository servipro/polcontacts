# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from common_functions import split_text_in_lines, fonts_import, text_truncate, n_e

from frappe import _

from os import path, sep

from PIL import Image

from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase.pdfmetrics import stringWidth

def drawpdf(canvas, data):

# **Page dimensions**
    width, height = A4

# **Internal Parameters**
    # *Printing Parameters*
    text_size = 9
    title_size = 14
    count_text_size = 14
    conditions_text_size = 7

    fonts_import()
    font = 'OpenSans-Regular'
    bold_font = 'OpenSans-Bold'
    italic_font = 'OpenSans-Italic'

    page_left_margin = 15
    page_right_margin = 15
    page_upper_margin = 15
    page_lower_margin = 15

    text_left_margin = 15
    text_right_margin = 15
    text_upper_margin = 2
    text_lower_margin = 8

    conditions_text_left_margin = 5
    conditions_text_right_margin = 5
    conditions_text_upper_margin = 3
    conditions_text_lower_margin = 5

    title_box_height = 45
    box_line_gap = 10
    box_logo_gap = 14

    count_box_gap = 5

    conditions_boxes_gap = 1
    conditions_paragraph_extra_gap = 1

    gap_form_filling_text = 5


    # *Lines and boxes position calculations*

    box_height = height - page_upper_margin - page_lower_margin

    old_middle_line_position = height/ 2 - page_upper_margin/ 2 + page_lower_margin/ 2

    box_text_line_width = width - page_right_margin - text_right_margin - page_left_margin - text_left_margin
    box_distance_between_lines = (box_height - title_box_height - text_upper_margin - text_lower_margin) / 38.0
    box_text_vertical_offset = (box_distance_between_lines-text_size)/2.0 + text_size*0.17

    lines_array = (8, 15.5, 20, 21, 23, 28.5, 29.5, 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0)
    lines_height = [
        page_lower_margin + text_lower_margin + index * box_distance_between_lines
        for index
        in lines_array
    ]

    interest_boxes_array = (23.37, 23.97, 24.57, 25.17, 25.77, 26.37)
    interest_boxes_height = [
        page_lower_margin + text_lower_margin + index * box_distance_between_lines + box_text_vertical_offset
        for index
        in interest_boxes_array
    ]
    interest_boxes_dimension = 0.6*box_distance_between_lines
    interest_boxes_positions = (
        (0.04,0.3),
        (0.04,0.3,0.63),
        (0.04,0.63),
        (0.04,0.3,0.63),
        (0.04,0.3,0.63),
        (0.04,0.3,0.63),
    )

    check_boxes_array = (13.37, 14.37, 15.37, 16.37)
    check_boxes_height = [
        page_lower_margin + text_lower_margin + index * box_distance_between_lines + box_text_vertical_offset
        for index
        in check_boxes_array
    ]
    check_boxes_dimension = 0.6*box_distance_between_lines
    check_boxes_positions = (
        (0.04,),
        (0.04,),
        (0.245,0.40,0.555),
        (0.15,0.35,0.535),
    )

    count_box_array = (4, 4, 4, 4, 4, 4)
    count_box_dimension = (box_text_line_width - (len(count_box_array) - 1) * count_box_gap) / sum(count_box_array)
    count_box_height = page_lower_margin + text_lower_margin + 17.2 * box_distance_between_lines + box_text_vertical_offset

    conditions_boxes_gap = 3

    # *Text definition and position calculations*

    form_texts_array = (37.0, 36.0, 35.0, 34.0, 33.0, 32.0, 31.0, 30.0, 29.5, 28.5, 27.5, 26.5, 25.9, 25.3, 24.7, 24.1, 23.5, 23.0, 22.0, 21.0, 20.0, 19.0, 18.5, 16.5, 15.5, 14.5, 13.5, 12.5, 12.0, 11.0, 9.0, 7.0, 5.0)

    form_texts_height = [
        page_lower_margin + text_lower_margin + index * box_distance_between_lines + box_text_vertical_offset
        for index
        in form_texts_array
    ]

    other_affiliations_text_in_lines = split_text_in_lines(
        _("(civic, sportive, cultural, unions, association...)"),
        stringWidth(_("(civic, sportive, cultural, unions, association...)"),font,text_size)/1.7,
        font,
        text_size
    )

    count_text_part = _("Please, proceed with the paymant of all invoices send by PDeCAT in concept affiliation fee until new notifications through count number-IBAN")
    count_text_part_in_lines = split_text_in_lines(count_text_part, box_text_line_width, font, text_size)

    form_texts = (
        (
            (_("Name"),0,bold_font),
            (_("Surname"),0.4,bold_font),
        ),(
            (_("Address"),0,bold_font),
            (_("Region"),0.5,bold_font),
        ),(
            (_("City"),0,bold_font),
            (_("Postal Code"),0.5,bold_font),
        ),(
            (_("Phone"),0,bold_font),
            (_("Mobile phone"),0.3,bold_font),
            (_("Email"),0.6,bold_font),
        ),(
            (_("Facebook"),0,bold_font),
            (_("Instagram"),0.3,bold_font),
            (_("Twitter"),0.6,bold_font),
        ),(
            (_("Sex"),0,bold_font),
            (_("DNI"),0.2,bold_font),
            (_("Birthday"),0.5,bold_font),
        ),(
            (_("Job"),0,bold_font),
            (_("Studies"),0.5,bold_font),
        ),(
            (_("Other affiliations"),0,bold_font),
            (other_affiliations_text_in_lines[0],stringWidth(_("Other affiliations"),bold_font,text_size)/box_text_line_width,font),
        ),(
            (other_affiliations_text_in_lines[1],0,font),
        ),(
            (_("Territory associations affiliation"),0,bold_font),
        ),(
            (_("Fields of Interest")+"*",0,bold_font),
            (_("(Put an 'I' for information interest or 'A' for affiliation interest)"),stringWidth(_("Fields of interest*"),bold_font,text_size)/box_text_line_width,font),
        ),(
            (_("Economic"),0.05,font),
            (_("Social Diversity and Citizenship"),0.31,font),
            (_("International Relations"),0.64,font)
        ),(
            (_("Geographic and Environment"),0.05,font),
            (_("Feminism and Equality"),0.31,font),
            (_("Law and Justice"),0.64,font),
        ),(
            (_("Health"),0.05,font),
            (_("Education"),0.31,font),
            (_("Digital Politics"),0.64,font),
        ),(
            (_("Universities, research and knowledge transfer"),0.05,font),
            (_("Institutional Relations"),0.64,font),
        ),(
            (_("Culture and Language"),0.05,font),
            (_("Sports"),0.31,font),
            (_("Agriculture Stockbreeding and Fishing"),0.64,font),
        ),(
            (_("Security"),0.05,font),
            (_("Older People"),0.31,font),
        ),(
            ("*"+_("Description in art. 50 Estatuts PDeCAT"),0.64,font),
        ),(
            (_("DIRECT BILLING FORM"),0,bold_font),
        ),(
            (_("Entity"),0,bold_font),
            (_("Address"),0.5,bold_font),
        ),(
            (_("City"),0,bold_font),
            (_("Postal code"),0.5,bold_font),
        ),(
            (count_text_part_in_lines[0],0,font),
        ),(
            (count_text_part_in_lines[1],0,font),
        ),(
            (_("General price"),0,font),
            (_("Reduced price"),0.2,font),
            (_("Social price"),0.4,font),
            (str(_("Other price"))+" .........................................................................",0.58,font),
        ),(
            (_("Payment form")+": "+_("Annual"),0,font),
            (_("Quarterly"),0.285,font),
            (_("Biannual"),0.44,font),
        ),(
            (_("I've read, known and accepted the Estatus, the Ethic Code and Transparency and all derived normes."),0.05,font),
        ),(
            (_("I declare I'm older than 18 years."),0.05,font),
        ),(
            (_("We propose you as an associate") + ":",0,bold_font),
        ),(
            ("*" + _("Proposal person should meet all requirements for the associate older than 6 months."),0,font),
        ),(
            (_("Name and surname"),0,bold_font),
            (_("DNI"),0.5,bold_font),
            (_("Signature"),0.75,bold_font),
        ),(
            (_("Name and surname"),0,bold_font),
            (_("DNI"),0.5,bold_font),
            (_("Signature"),0.75,bold_font),
        ),(
            (_("Application Date"),0,bold_font),
            (_("Applicant Signature"),0.5,bold_font),
        ),(
            (_("from"),.15,font),
            (_("in"),0.35,font),
        ),
    )
    if not data["arguments"]["empty"]:

        filling_texts_array = (37.0, 36.0, 35.0, 34.0, 33.0, 32.0, 31.0, 28.5, 26.5, 25.9, 25.3, 24.7, 24.1, 23.5, 21.0, 20.0, 16.6, 16.5, 15.5)
        filling_texts_height = [
            page_lower_margin + text_lower_margin + index * box_distance_between_lines + box_text_vertical_offset
            for index
            in filling_texts_array
        ]

        affiliation_territory = ", ".join([n_e(data["doc"].territory), n_e(data["doc"].region1), n_e(data["doc"].region2)])

        job = ", ".join([job.job for job in data["doc"].job_table])
        job = text_truncate(
            job,
            box_text_line_width/2 - stringWidth(_("Job")+"  ",bold_font,text_size),
            font,
            text_size
        )

        studies_table = [study.studies + "(" + study.level + ")" for study in data["doc"].studies_table]
        studies = ", ".join(studies_table)
        studies = text_truncate(
            studies,
            box_text_line_width/2 - stringWidth(_("Studies")+" ",bold_font,text_size),
            font,
            text_size
        )

        general_fee = reduced_fee = social_fee = other_fee = ""

        if data["doc"].ordinary_fee.lower() == "general":
            general_fee = "X"
        elif data["doc"].ordinary_fee.lower() == "reduida":
            reduced_fee = "X"
        elif data["doc"].ordinary_fee.lower() == "social":
            social_fee = "X"
        else:
            other_fee = data["doc"].ordinary_fee

        if data["doc"].extra_ordinary > 0:
            extra_fee_text = _("Extra") + "-" + "{0:.2f}".format(data["doc"].extra_ordinary) + u"\u20AC"
            if other_fee == "":
                other_fee = extra_fee_text
            else:
                other_fee += " / " + extra_fee_text

        annual_ordinary_period = biannual_ordinary_period = quarterly_ordinary_period = ""
        if data["doc"].ordinary_period == "Annual":
            annual_ordinary_period = "X"
        elif data["doc"].ordinary_period == "Biannual":
            biannual_ordinary_period = "X"
        elif data["doc"].ordinary_period == "Quarterly":
            quarterly_ordinary_period = "X"

        interest_economic = interest_social = interest_international = interest_geographic = interest_institutional = interest_education = interest_culture = interest_sports = interest_agriculture = interest_security = interest_health = interest_lower_education = interest_digital_politics = interest_universities_research = interest_older_people = interest_feminism_equality = interest_law_justice = ""
        for interest in data["doc"].interests:
            if interest.field_of_interest == "Economic":
                if interest.level == "Information":
                    interest_economic = "I"
                else:
                    interest_economic = "A"
            elif interest.field_of_interest == "Social Diversity and Citizenship":
                if interest.level == "Information":
                    interest_social = "I"
                else:
                    interest_social = "A"
            elif interest.field_of_interest == "International Relations":
                if interest.level == "Information":
                    interest_international = "I"
                else:
                    interest_international = "A"
            elif interest.field_of_interest == "Geographic and Environment":
                if interest.level == "Information":
                    interest_geographic = "I"
                else:
                    interest_geographic = "A"
            elif interest.field_of_interest == "Institutional Relations":
                if interest.level == "Information":
                    interest_institutional = "I"
                else:
                    interest_institutional = "A"
            elif interest.field_of_interest == "Culture and Language":
                if interest.level == "Information":
                    interest_culture = "I"
                else:
                    interest_culture = "A"
            elif interest.field_of_interest == "Sports":
                if interest.level == "Information":
                    interest_sports = "I"
                else:
                    interest_sports = "A"
            elif interest.field_of_interest == "Agriculture Stockbreeding and Fishing":
                if interest.level == "Information":
                    interest_agriculture = "I"
                else:
                    interest_agriculture = "A"
            elif interest.field_of_interest == "Security":
                if interest.level == "Information":
                    interest_security = "I"
                else:
                    interest_security = "A"
            elif interest.field_of_interest == "Health":
                if interest.level == "Information":
                    interest_health = "I"
                else:
                    interest_health = "A"
            elif interest.field_of_interest == "Education":
                if interest.level == "Information":
                    interest_lower_education = "I"
                else:
                    interest_lower_education = "A"
            elif interest.field_of_interest == "Digital Politics":
                if interest.level == "Information":
                    interest_digital_politics = "I"
                else:
                    interest_digital_politics = "A"
            elif interest.field_of_interest == "Universities, research and knowledge transfer":
                if interest.level == "Information":
                    interest_universities_research = "I"
                else:
                    interest_universities_research = "A"
            elif interest.field_of_interest == "Older People":
                if interest.level == "Information":
                    interest_older_people = "I"
                else:
                    interest_older_people = "A"
            elif interest.field_of_interest == "Feminism and Equality":
                if interest.level == "Information":
                    interest_feminism_equality = "I"
                else:
                    interest_feminism_equality = "A"
            elif interest.field_of_interest == "Law and Justice":
                if interest.level == "Information":
                    interest_law_justice= "I"
                else:
                    interest_law_justice = "A"

        filling_texts = (
            (
                (data["doc"].contact_name, 0+(stringWidth(_("Name"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].surname1+" "+(data["doc"].surname2 or ""), 0.4+(stringWidth(_("Surnname"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
            ),(
                (data["doc"].address, 0+(stringWidth(_("Address"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].region, 0.5+(stringWidth(_("Region"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
            ),(
                (data["doc"].city, 0+(stringWidth(_("City"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].postal_code, 0.5+(stringWidth(_("Postal code"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
            ),(
                (data["doc"].phone, 0+(stringWidth(_("Phone"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].mobile_phone, 0.3+(stringWidth(_("Mobile phone"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].email, 0.6+(stringWidth(_("Email"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
            ),(
                (data["doc"].facebook, 0+(stringWidth(_("Facebook"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].instagram, 0.3+(stringWidth(_("Instagram"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].twitter, 0.6+(stringWidth(_("Twitter"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
            ),(
                (_(data["doc"].sex), 0+(stringWidth(_("Sex"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (data["doc"].dni, 0.2+(stringWidth(_("DNI"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
                (str(data["doc"].birthdate), 0.5+(stringWidth(_("Birthday"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
            ),(
                (job, 0 + (stringWidth(_("Job"), bold_font, text_size) + gap_form_filling_text) / box_text_line_width,font),
                (studies, 0.5 + (stringWidth(_("Studies"), bold_font, text_size) + gap_form_filling_text) / box_text_line_width,font),
            ),(
                (affiliation_territory, 0+(stringWidth(_("Territory associations affiliation"),bold_font,text_size)+gap_form_filling_text)/box_text_line_width, font),
            ),(
                (interest_economic, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_economic,font,text_size)/2)/box_text_line_width, font),
                (interest_social, 0.3+(-interest_boxes_dimension/2-stringWidth(interest_social,font,text_size)/2)/box_text_line_width, font),
                (interest_international, 0.63+(-interest_boxes_dimension/2-stringWidth(interest_international,font,text_size)/2)/box_text_line_width, font),
            ),(
                (interest_geographic, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_geographic,font,text_size)/2)/box_text_line_width, font),
                (interest_feminism_equality, 0.3+(-interest_boxes_dimension/2-stringWidth(interest_feminism_equality,font,text_size)/2)/box_text_line_width, font),
                (interest_law_justice, 0.63+(-interest_boxes_dimension/2-stringWidth(interest_law_justice,font,text_size)/2)/box_text_line_width, font),
            ),(
                (interest_health, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_health,font,text_size)/2)/box_text_line_width, font),
                (interest_lower_education, 0.3+(-interest_boxes_dimension/2-stringWidth(interest_lower_education,font,text_size)/2)/box_text_line_width, font),
                (interest_digital_politics, 0.63+(-interest_boxes_dimension/2-stringWidth(interest_digital_politics,font,text_size)/2)/box_text_line_width, font),
            ),(
                (interest_universities_research, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_universities_research,font,text_size)/2)/box_text_line_width, font),
                (interest_institutional, 0.63+(-interest_boxes_dimension/2-stringWidth(interest_institutional,font,text_size)/2)/box_text_line_width, font),
            ),(
                (interest_culture, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_culture,font,text_size)/2)/box_text_line_width, font),
                (interest_sports, 0.3+(-interest_boxes_dimension/2-stringWidth(interest_sports,font,text_size)/2)/box_text_line_width, font),
                (interest_agriculture, 0.63+(-interest_boxes_dimension/2-stringWidth(interest_agriculture,font,text_size)/2)/box_text_line_width, font),
            ),(
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
                (interest_older_people, 0.3+(-interest_boxes_dimension/2-stringWidth(interest_older_people,font,text_size)/2)/box_text_line_width, font),
            ),(
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
            ),(
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
                (interest_security, 0.04+(-interest_boxes_dimension/2-stringWidth(interest_security,font,text_size)/2)/box_text_line_width, font),
            ),(
                (other_fee, 0.685, font),
            ),(
                (general_fee, 0.134, font),
                (reduced_fee, 0.334, font),
                (social_fee, 0.519, font),
            ),(
                (annual_ordinary_period, 0.229, font),
                (quarterly_ordinary_period, 0.384, font),
                (biannual_ordinary_period, 0.539, font),
            )
        )

    conditions_text_part_1 = (
        "Amb la signatura d’aquesta butlleta vostè autoritza al PARTIT DEMÒCRATA EUROPEU CATALÀ (PDeCAT) a tramitar la seva alta com a persona associada al partit i accepta el tractament de les seves dades personals de conformitat amb la seva política de privadesa. La política de privadesa es pot consultar al web www.partitdemocrata.cat. En compliment de la normativa vigent sobre protecció de dades, li informem que el responsable del tractament de les seves personals i bancàries, és el PARTIT DEMÒCRATA EUROPEU CATALÀ (PDeCAT), C/ Provença, núm. 339 (08037 Barcelona), NIF. Núm. G66848755. La finalitat del tractament de les seves dades és la dur a terme la gestió administrativa i organitzativa de la seva vinculació al partit, i si s’escau, realitzar el cobrament de quotes.",
    )
    conditions_text_part_2 = (
        "Mitjançant la signatura d’aquesta butlleta vostè autoritza al PARTIT DEMÒCRATA EUROPEU CATALÀ (PDeCAT) a enviar instruccions a la seva entitat bancària per efectuar càrrecs en el seu compte seguint les instruccions del partit.",
        "Pot exercir els drets d’accés, rectificació, supressió, limitació, oposició i portabilitat de les seves dades personals, en el domicili del Partit Demòcrata, C/Provença, núm. 339, (08037) Barcelona, telèfon 93 236 31 00 o sol•licitar-ho mitjançant l’enviament d’un correu electrònic a contacte@partitdemocrata.cat",
    )
    conditions_text_part_1_in_lines = [split_text_in_lines(text,box_text_line_width-conditions_text_left_margin-conditions_text_right_margin,font,conditions_text_size) for text in conditions_text_part_1]
    conditions_text_part_2_in_lines = [split_text_in_lines(text,box_text_line_width-conditions_text_left_margin-conditions_text_right_margin,font,conditions_text_size) for text in conditions_text_part_2]
    conditions_text_part_1_lines_number = sum([len(line) for line in conditions_text_part_1_in_lines])
    conditions_text_part_2_lines_number = sum([len(line) for line in conditions_text_part_2_in_lines])
    conditions_distance_between_lines = ((page_lower_margin+text_lower_margin+3.5*box_distance_between_lines+box_text_vertical_offset)-2*(conditions_text_upper_margin+conditions_text_lower_margin)-conditions_boxes_gap-(len(conditions_text_part_1_in_lines)+len(conditions_text_part_2_in_lines)-2)*conditions_paragraph_extra_gap)/(conditions_text_part_1_lines_number+conditions_text_part_2_lines_number)

# **Print lines and boxes**

    canvas.saveState()
    canvas.setStrokeColor(colors.grey)

    # *Boxes*
    background_directory = path.dirname(path.abspath(__file__)) + sep + 'Logos' + sep + 'background.jpg'
    canvas.drawImage(background_directory, page_left_margin, height-page_upper_margin-title_box_height, height=title_box_height, width=width-page_left_margin-page_right_margin)
    canvas.rect(page_left_margin, page_lower_margin, width-page_left_margin-page_right_margin, height-page_upper_margin-page_lower_margin)
    canvas.saveState()
    canvas.rect(page_left_margin, height-page_upper_margin-title_box_height, width-page_left_margin-page_right_margin, title_box_height, stroke=1, fill=0)
    canvas.restoreState()

    for line_height in lines_height:
        canvas.line(page_left_margin+text_left_margin, line_height, width-page_right_margin-text_right_margin, line_height)

    # *Interest boxes*
    for index, interest_box_group in enumerate(interest_boxes_positions):
        current_height = interest_boxes_height[index]
        for interest_box in interest_box_group:
            x = page_left_margin+text_left_margin+interest_box*box_text_line_width-interest_boxes_dimension
            y = current_height
            canvas.rect(page_left_margin+text_left_margin+interest_box*box_text_line_width-interest_boxes_dimension, current_height, interest_boxes_dimension, interest_boxes_dimension, stroke=1, fill=0)

    # *Check boxes*
    for index, check_box_group in enumerate(check_boxes_positions):
        current_height = check_boxes_height[index]
        for check_box in check_box_group:
            x = page_left_margin+text_left_margin+check_box*box_text_line_width-check_boxes_dimension
            y = current_height
            canvas.rect(page_left_margin+text_left_margin+check_box*box_text_line_width-check_boxes_dimension, current_height, check_boxes_dimension, check_boxes_dimension, stroke=1, fill=0)
        
    # *Count boxes*
    canvas.saveState()
    canvas.setStrokeColor(colors.black)
    horizontal_position = page_left_margin + text_left_margin
    for count_box_group in count_box_array:
        for index in range(count_box_group):
            canvas.rect(horizontal_position, count_box_height, count_box_dimension, count_box_dimension, stroke=1, fill=0)
            horizontal_position += count_box_dimension
        horizontal_position += count_box_gap
    canvas.restoreState()

    # *Lower box date and signature lines*
    lower_box_date_and_signature_lines = page_lower_margin+text_lower_margin+5*box_distance_between_lines
    canvas.line(page_left_margin+text_left_margin, lower_box_date_and_signature_lines, page_left_margin+text_left_margin+0.145*box_text_line_width, lower_box_date_and_signature_lines)
    canvas.line(page_left_margin+text_left_margin+0.155*box_text_line_width+stringWidth(_("from"),font,text_size), lower_box_date_and_signature_lines, page_left_margin+text_left_margin+0.345*box_text_line_width, lower_box_date_and_signature_lines)
    canvas.line(page_left_margin+text_left_margin+0.355*box_text_line_width+stringWidth(_("in"),font,text_size), lower_box_date_and_signature_lines, page_left_margin+text_left_margin+0.45*box_text_line_width, lower_box_date_and_signature_lines)
    canvas.line(page_left_margin+text_left_margin+0.5*box_text_line_width, lower_box_date_and_signature_lines, width-page_right_margin-text_right_margin, lower_box_date_and_signature_lines)

    # *Conditions boxes*
    canvas.saveState()
    canvas.setStrokeColor(colors.lightgrey)
    canvas.setFillColor(colors.lightgrey)
    conditions_box_part_1_height = (conditions_text_part_1_lines_number-1)*conditions_distance_between_lines+conditions_text_size+(len(conditions_text_part_1)-1)*conditions_paragraph_extra_gap+conditions_text_lower_margin+conditions_text_upper_margin
    conditions_box_part_2_height = (conditions_text_part_2_lines_number-1)*conditions_distance_between_lines+conditions_text_size+(len(conditions_text_part_2)-1)*conditions_paragraph_extra_gap+conditions_text_lower_margin+conditions_text_upper_margin
    canvas.rect(page_left_margin+text_left_margin, page_lower_margin+text_lower_margin+conditions_box_part_2_height+conditions_boxes_gap, box_text_line_width, conditions_box_part_1_height, stroke=1, fill=1)
    canvas.rect(page_left_margin+text_left_margin, page_lower_margin+text_lower_margin, box_text_line_width, conditions_box_part_2_height, stroke=1, fill=1)
    canvas.restoreState()

    canvas.restoreState()

# **Print logos and titles**

    logo_directory = path.dirname(path.abspath(__file__)) + sep + 'Logos' + sep + 'logo.jpg'
    logo = Image.open(logo_directory)
    logo_width, logo_height = logo.size
    new_logo_width = (title_box_height-1.6*box_logo_gap) * logo_width / logo_height
    logo_horizontal_position = page_left_margin + box_logo_gap
    logo_vertical_position = height - page_upper_margin - title_box_height + 0.8*box_logo_gap


    canvas.drawImage(
        logo_directory,
        logo_horizontal_position,
        logo_vertical_position,
        height = title_box_height - 1.6*box_logo_gap,
        width = new_logo_width
    )

    text_image_directory = path.dirname(path.abspath(__file__)) + sep + 'Logos' + sep + 'text.jpg'
    text_image = Image.open(text_image_directory)
    text_image_width, text_image_height = text_image.size
    new_text_image_width = title_box_height * text_image_width / text_image_height
    text_image_horizontal_position = width - page_right_margin - box_logo_gap - new_text_image_width
    text_image_vertical_position = height - page_upper_margin - title_box_height

    canvas.drawImage(
        text_image_directory,
        text_image_horizontal_position,
        text_image_vertical_position,
        height = title_box_height,
        width = new_text_image_width
    )


# **Print form text**
    canvas.saveState()

    #Upper texts
    for index, text_group in enumerate(form_texts):
        current_height = form_texts_height[index]
        for form_text in text_group:
            canvas.setFont(form_text[2], text_size)
            canvas.drawString(page_left_margin+text_left_margin+form_text[1]*box_text_line_width, current_height, form_text[0])

    #Conditions text
    canvas.setFont(font, conditions_text_size)
    current_height=page_lower_margin+text_lower_margin+conditions_box_part_2_height+conditions_boxes_gap+conditions_text_lower_margin
    for paragraph in conditions_text_part_1_in_lines:
        for index in range(len(paragraph)):
            canvas.drawString(page_left_margin+text_left_margin+conditions_text_left_margin, current_height, paragraph[-1-index])
            current_height += conditions_distance_between_lines
        current_height += conditions_paragraph_extra_gap
    current_height=page_lower_margin+text_lower_margin+conditions_text_lower_margin
    for paragraph in conditions_text_part_2_in_lines:
        for index in range(len(paragraph)):
            canvas.drawString(page_left_margin+text_left_margin+conditions_text_left_margin, current_height, paragraph[-1-index])
            current_height += conditions_distance_between_lines
        current_height += conditions_paragraph_extra_gap

# **Print filling text**
    if not data["arguments"]["empty"]:

    #Upper texts
        index=0
        for filling_text_group in filling_texts:
            current_height=filling_texts_height[index]
            index+=1
            for filling_text in filling_text_group:
                if filling_text[0]!=None:
                    canvas.setFont(filling_text[2], text_size)
                    canvas.drawString(page_left_margin+text_left_margin+filling_text[1]*box_text_line_width, current_height, filling_text[0])

    #Lower texts
        IBAN_index=0
        if data["doc"].payment_form=="Bank Transfer":
            canvas.setFont(font, count_text_size)
            horizontal_position = page_left_margin + text_left_margin + count_box_dimension/2
            for count_box_group in count_box_array:
                for index in range(count_box_group):
                    canvas.drawCentredString(horizontal_position, count_box_height+(count_box_dimension-count_text_size)/2+count_text_size*0.17, data["doc"].iban[IBAN_index])
                    IBAN_index+=1
                    horizontal_position += count_box_dimension
                horizontal_position += count_box_gap

    canvas.restoreState()


    return canvas