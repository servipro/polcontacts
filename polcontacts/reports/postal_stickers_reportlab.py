# -*- coding: utf-8 -*-
# Copyright (c) 2017, SERVIPRO SL and contributors
# For license information, please see license.txt

from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from common_functions import fonts_import, split_text_in_lines, text_truncate, n_e


def drawpdf(canvas, data):
    
    # **Internal parameters**
    fonts_import()
    font = 'OpenSans-Regular'
    bold_font = 'OpenSans-Bold'

    # **page dimensions**
    width, height = A4

    # **Variables initialized**
    canvas.setFont(font, data["stickers_configuration"].text_size)

    current_column = max(min(data["arguments"]["column"], data["stickers_configuration"].columns_number) - 1, 0)
    current_row = max(min(data["arguments"]["row"], data["stickers_configuration"].rows_number) - 1, 0)

    gap_between_columns = (width - data["stickers_configuration"].page_left_margin * cm - data["stickers_configuration"].page_right_margin * cm - data["stickers_configuration"].columns_number * data["stickers_configuration"].column_width * cm) / (data["stickers_configuration"].columns_number - 1)
    gap_between_rows = (height - data["stickers_configuration"].page_upper_margin * cm - data["stickers_configuration"].page_lower_margin * cm - data["stickers_configuration"].rows_number * data["stickers_configuration"].rows_height * cm) / (data["stickers_configuration"].rows_number - 1)

    # **Starting position calculation**
    horizontal_position = data["stickers_configuration"].page_left_margin * cm + current_column * (data["stickers_configuration"].column_width * cm + gap_between_columns)
    vertical_position = height - data["stickers_configuration"].page_upper_margin * cm - data["stickers_configuration"].rows_height * cm - current_row * (data["stickers_configuration"].rows_height * cm + gap_between_rows)

    # **Lines height calculation
    gap_between_text_lines = min(0.5 * data["stickers_configuration"].text_size, (data["stickers_configuration"].rows_height * cm-4*data["stickers_configuration"].text_size - data["stickers_configuration"].sticker_upper_margin * cm - data["stickers_configuration"].sticker_lower_margin * cm)/3)
    name_text_height_three_lines = data["stickers_configuration"].rows_height * cm / 2 + gap_between_text_lines + 0.17 * data["stickers_configuration"].text_size + 0.5*data["stickers_configuration"].text_size
    address_text_height_three_lines = data["stickers_configuration"].rows_height * cm / 2 + 0.17 * data["stickers_configuration"].text_size - 0.5*data["stickers_configuration"].text_size
    city_text_height_three_lines = data["stickers_configuration"].rows_height * cm / 2 - gap_between_text_lines + 0.17 * data["stickers_configuration"].text_size - 1.5*data["stickers_configuration"].text_size
    name_text_height_four_lines = data["stickers_configuration"].rows_height * cm / 2 + 1.5 * gap_between_text_lines + 0.17 * data["stickers_configuration"].text_size + data["stickers_configuration"].text_size
    address_text_height_four_lines_1 = data["stickers_configuration"].rows_height * cm / 2 + 0.5 * gap_between_text_lines + 0.17 * data["stickers_configuration"].text_size
    address_text_height_four_lines_2 = data["stickers_configuration"].rows_height * cm / 2 - 0.5 * gap_between_text_lines + 0.17 * data["stickers_configuration"].text_size - data["stickers_configuration"].text_size
    city_text_height_four_lines = data["stickers_configuration"].rows_height * cm / 2 - 1.5 * gap_between_text_lines + 0.17 * data["stickers_configuration"].text_size - 2*data["stickers_configuration"].text_size

    # **Printing loop**
    for member in data["members_data"]:

        # **Lines text calculation
        name_text = text_truncate(member.contact_name+" "+member.surname1+" "+(member.surname2 or ""),data["stickers_configuration"].column_width * cm-data["stickers_configuration"].sticker_left_margin*cm-data["stickers_configuration"].sticker_right_margin*cm,font,data["stickers_configuration"].text_size)
        address_text_in_lines = split_text_in_lines(n_e(member.address),data["stickers_configuration"].column_width * cm-data["stickers_configuration"].sticker_left_margin*cm-data["stickers_configuration"].sticker_right_margin*cm,font,data["stickers_configuration"].text_size)
        if len(address_text_in_lines)>1:
            address_text_second_line = ""
            for line in address_text_in_lines[1:]:
                address_text_second_line+=line+" "
            address_text_in_lines = (address_text_in_lines[0],text_truncate(address_text_second_line.rstrip(),data["stickers_configuration"].column_width * cm-data["stickers_configuration"].sticker_left_margin*cm-data["stickers_configuration"].sticker_right_margin*cm,font,data["stickers_configuration"].text_size))
        city_text = text_truncate(n_e(member.postal_code) + ", " + n_e(member.city),data["stickers_configuration"].column_width * cm-data["stickers_configuration"].sticker_left_margin*cm-data["stickers_configuration"].sticker_right_margin*cm,font,data["stickers_configuration"].text_size)

        # **Dibuixem marge si es configura**
        if data["stickers_configuration"].print_border:
            canvas.rect(horizontal_position, vertical_position, data["stickers_configuration"].column_width * cm, data["stickers_configuration"].rows_height * cm, fill=0)

        # **Dibuixem el text**
        if len(address_text_in_lines)<2:
            canvas.drawString(horizontal_position+data["stickers_configuration"].sticker_left_margin*cm, vertical_position + name_text_height_three_lines, name_text)
            canvas.setFont(font, data["stickers_configuration"].text_size)
            canvas.drawString(horizontal_position+data["stickers_configuration"].sticker_left_margin*cm, vertical_position + address_text_height_three_lines, address_text_in_lines[0])
            canvas.drawString(horizontal_position+data["stickers_configuration"].sticker_left_margin*cm, vertical_position + city_text_height_three_lines, city_text)
        else:
            canvas.setFont(bold_font, data["stickers_configuration"].text_size)
            canvas.drawString(horizontal_position+data["stickers_configuration"].sticker_left_margin*cm, vertical_position + name_text_height_four_lines, name_text)
            canvas.setFont(font, data["stickers_configuration"].text_size)
            canvas.drawString(horizontal_position+data["stickers_configuration"].sticker_left_margin*cm, vertical_position + address_text_height_four_lines_1, address_text_in_lines[0])
            canvas.drawString(horizontal_position+data["stickers_configuration"].sticker_left_margin*cm, vertical_position + address_text_height_four_lines_2, address_text_in_lines[1])
            canvas.drawString(horizontal_position+data["stickers_configuration"].sticker_left_margin*cm, vertical_position + city_text_height_four_lines, city_text)

        # **Move to next position**
        if data["arguments"]["direction"] == "Horizontal":
            current_column += 1
            if current_column >= data["stickers_configuration"].columns_number:
                current_row += 1
                current_column = 0
                if current_row >= data["stickers_configuration"].rows_number:
                    canvas.showPage()
                    canvas.setFont(font, data["stickers_configuration"].text_size)
                    current_row = 0
        else:
            current_row += 1
            if current_row >= data["stickers_configuration"].rows_number:
                current_column += 1
                current_row = 0
                if current_column >= data["stickers_configuration"].columns_number:
                    canvas.showPage()
                    canvas.setFont(font, data["stickers_configuration"].text_size)
                    current_column = 0

        # **New position calculation**
        horizontal_position = data["stickers_configuration"].page_left_margin * cm + current_column * (data["stickers_configuration"].column_width * cm + gap_between_columns)
        vertical_position = height - data["stickers_configuration"].page_upper_margin * cm - data["stickers_configuration"].rows_height * cm - current_row * (data["stickers_configuration"].rows_height * cm + gap_between_rows)