# coding=utf-8

import frappe
import redis
from frappe.utils.file_manager import save_url
from pol_contacts.doctype.receivable.receivable import receivable_exists
from frappe import _, ValidationError
from datetime import datetime
from frappe.translate import get_user_lang
import tempfile

from polcontacts.pol_contacts.doctype.contact_fee.contact_fee import calculate_partial_fee
import json

from polcontacts.reports import postal_stickers_reportlab
from reportlab.pdfgen import canvas

def get_children_from_cache(territory):
    conn = redis.from_url(frappe.local.conf.redis_cache)
    prefix = frappe.conf.redis_territory + "_"

    ##If the cache is not initialized, we create it
    if not conn.exists(prefix + "cache"):
        generate_territory_cache()

    try:
        return conn.get(prefix + territory).decode("utf-8").split(";")
    except AttributeError:
        generate_territory_cache()
        return get_all_children(territory)

def get_user_territories(user):
    territory = frappe.db.get_value("User", user, ["territory"])
    child_territories = get_children_from_cache(territory)
    return child_territories

def has_permission_entity(doc, user):
    child_territories = get_user_territories(user)

    if doc.territory in child_territories:
        return True

    return False

@frappe.whitelist()
def get_parent_regions(item):
    result = get_all_parents(item)
    if result != []:
        if not u"Federació Barcelona Ciutat" in result:
            result.remove(result[0])

        if "Municipi" in result[0]:
            result.remove(result[0])
        result.remove('Nacional')
    return result[:2]

def get_all_parents(item):
    result = []
    if frappe.db.exists("Territory", item):
        territory = frappe.db.get_value("Territory", item, ["name", "parent_territory"], as_dict=True)
        while territory and territory.parent_territory and territory.parent_territory != "":
            result.append(territory.name)
            territory = frappe.db.get_value("Territory", territory.parent_territory, ["name", "parent_territory"],
                                            as_dict=True)

        result.append(territory.name)
    return result

def get_all_children(item):
    result = [item]
    items = frappe.get_list("Territory", fields=["name", "is_group"], filters={"parent_territory": item},
                            user="Administrator")
    for obj in items:
        if not obj.is_group:
            result.append(obj.name)
        else:
            for elem in get_all_children(obj.name):
                result.append(elem)
    return result

@frappe.whitelist()
def delete_territory_cache():
    conn = redis.from_url(frappe.local.conf.redis_cache)
    prefix = frappe.conf.redis_territory + "_"
    for key in conn.scan_iter(prefix + "*"):
        conn.delete(key)

@frappe.whitelist()
def generate_territory_cache():
    conn = redis.from_url(frappe.local.conf.redis_cache)
    prefix = frappe.conf.redis_territory + "_"
    ##Set a key in order to indicate that the cache has been initialized
    conn.set(prefix + "cache", 1)

    top = frappe.get_list("Territory", fields=["name"], user="Administrator")
    for item in top:
        conn.set(prefix + item.name, ";".join(get_all_children(item.name)))

        # Se permite ver entre todas las entidades padres e hijas del territorio del usuario

def entity_unique_query_conditions(user):
    if user == "Administrator":
        return ""
    else:
        territory = frappe.db.get_value("User", user, ["territory"])
        child_territories = get_all_parents(territory)
        child_territories += get_children_from_cache(territory)

        return u"""(name  in ({territories}))""".format(
            territories='"' + '", "'.join(child_territories) + '"')

def entity_query_conditions(user):
    if user == "Administrator":
        return ""
    else:
        territory = frappe.db.get_value("User", user, ["territory"])
        child_territories = get_all_parents(territory)
        child_territories += get_children_from_cache(territory)

        return u"""(territory  in ({territories}))""".format(
            territories='"' + '", "'.join(child_territories) + '"')

def contact_membership_query_conditions(user):
    if user == "Administrator":
        return ""
    else:
        territory = frappe.db.get_value("User", user, ["territory"])
        return u"""territories LIKE '%{territory}%'""".format(
            territory=territory.replace("'", "\\'"))

def has_permission_contact_membership(doc, user):
    territory = frappe.db.get_value("User", user, ["territory"])
    return territory in doc.territories

def polcontact_query_conditions(user):
    if user == "Administrator":
        return ""
    else:
        return territory_query_conditions(user)

def territory_query_conditions(user):
    if user == "Administrator":
        return ""
    else:
        territory = frappe.db.get_value("User", user, ["territory"])
        child_territories = get_children_from_cache(territory)
        return u"""(territory  in ({territories}))""".format(
            territories='"' + '", "'.join(child_territories) + '"')

period_divisor = {"Monthly": 12, "Quarterly": 4, "Biannual": 2, "Annual": 1}

def gen_receivable(contact, period, fee, contact_bank_bic=None, special_info=None):
    doc = frappe.new_doc("Receivable")

    # Base details
    doc.update({
        "contact": contact.name,
        "territory": contact.territory,
        "type": contact.payment_form,
        "receivable_period": period["period"],
        "biannual_period": period["biannual_period"],
        "year_period": period["year_period"],
        "month_period": period["month_period"],
        "quarter_period": period["quarter_period"],
        "date": frappe.utils.nowdate(),
        "amount": 0.0,
        "contact_name": contact.full_name,
        "receivable_type": fee.quote_type,
        "table_entries": []
    })

    # Payment Form
    if contact.payment_form == "Bank Transfer":
        if contact_bank_bic == None:
            contact_bank_bic = frappe.get_doc("Bank", contact.bank).bic
        doc.status_invoice = "Wallet"
        doc.bic = contact_bank_bic
        doc.bank = contact.bank
        doc.iban = contact.iban
    elif contact.payment_form == "Cash":
        doc.status_cash = "Pending"

    amount = fee.get_fee(period["year_period"]).amount
    if fee.quote_type == "Ordinary":
        amount = amount / period_divisor[period["period"]]

    membership_date = contact.ordinary_fee_start

    if fee.quote_type == "Special" and special_info.membership_date is not None:
        membership_date = special_info.membership_date

    if membership_date != None:
        amount = calculate_partial_fee(amount, period, membership_date)
        if amount == None:
            return None

    doc.amount = amount
    doc.fee = fee.name
    total = doc.amount

    # Ordinary Receivable
    if fee.quote_type == "Ordinary" and contact.extra_ordinary > 0:
        doc.extra = contact.extra_ordinary / period_divisor[period["period"]]
        total += doc.extra

    # Special Receivable
    elif fee.quote_type == "Special":
        if special_info.quote_origin != None:
            doc.quote_origin = special_info.quote_origin
        if special_info.quote_origin != None:
            doc.quote_destination = special_info.quote_destination

        if special_info.extra_special > 0:
            doc.extra = special_info.extra_special
            total += doc.extra

    if int(total) == 0:
        return None

    doc.receivable_total = total
    doc.db_insert()

    return doc

def gen_fixed_receivable(contact, period, fee, contact_bank_bic=None, special_info=None, fixed_amount=0.0):
    doc = gen_receivable(contact, period, fee, contact_bank_bic, special_info)
    if doc is None:
        return None

    doc.amount = fixed_amount
    doc.extra = 0.0
    doc.receivable_total = doc.amount

    doc.db_update()
    return doc

@frappe.whitelist()
def generate_receivables(period, date, quote_type, lang=None):
    if lang == None and frappe.session.user:
        lang = get_user_lang('frappe.session.user')

    i = 1
    frappe.db.begin()

    filters = {
        'contact_status': 'Current',
        'payment_form': ["!=", "Undefined"]
    }

    if quote_type == "Ordinary":
        filters["ordinary_period"] = period["period"]

    contacts = frappe.get_all("PolContact",
                              filters=filters,
                              fields=["dni", "name", "contact_name", "full_name", "iban", "payment_form",
                                      "ordinary_period", "extra_ordinary", "territory",
                                      "bank", "ordinary_fee", "ordinary_fee_start"])

    banks_array = frappe.get_list("Bank", fields=["bic", "name"])
    banks_dict = {}
    for bank in banks_array:
        banks_dict[bank.name] = bank

    fees_array = frappe.get_list("Contact Fee", fields=["name"])
    fees_dict = {}
    for fee in fees_array:
        fees_dict[fee.name] = frappe.get_doc("Contact Fee", fee.name)

    receivables = []

    for contact in contacts:
        # publish task_update
        if i % 100 == 0:
            frappe.publish_realtime("remittance_progress", {"progress": [i, len(contacts)]}, user=frappe.session.user)
        i += 1

        if contact.payment_form == "Bank Transfer" and not contact.bank in banks_dict:
            frappe.logger(module="polcontact").debug(contact)
            frappe.msgprint(
                _("Bank {0} not found for contact {1}, Receivable not generated", lang=lang).format(contact.bank,
                                                                                                    contact.dni))
            continue

        bank_bic = banks_dict[contact.bank].bic if contact.bank else None

        if quote_type == "Ordinary" and contact.ordinary_fee:
            if not receivable_exists(contact.name, period, quote_type, contact.ordinary_fee):
                receivable = gen_receivable(contact, period, fees_dict[contact.ordinary_fee], bank_bic)
                if receivable and contact.payment_form == "Bank Transfer":
                    receivables.append(receivable.name)

        elif quote_type == "Special":
            special_entries = frappe.get_all("Contact Special Fee Table", filters={"parent": contact.name}, fields="*")
            for special_entry in special_entries:
                if special_entry.special_period == period["period"]:
                    if not receivable_exists(contact.name, period, quote_type, special_entry.special_fee):
                        receivable = gen_receivable(contact, period, fees_dict[special_entry.special_fee], bank_bic,
                                                    special_entry)
                        if receivable and contact.payment_form == "Bank Transfer":
                            receivables.append(receivable.name)

    frappe.msgprint(_("Receivables generated correctly", lang=lang))

    frappe.db.commit()
    frappe.clear_cache()
    frappe.publish_realtime("doctype_clean", {"doctype": "Receivable"})
    frappe.publish_realtime("list_update", {"doctype": "Receivable"}, after_commit=True)
    return receivables


@frappe.whitelist()
def generate_extra_receivables(period, date, quote_type, fee_name, amount, fixed_amount, lang=None):
    if lang == None and frappe.session.user:
        lang = get_user_lang('frappe.session.user')

    i = 1
    frappe.db.begin()

    filters = {
        'contact_status': 'Current',
        "payment_form": ["!=", "Undefined"]
    }

    contacts = frappe.get_all("PolContact",
                              filters=filters,
                              fields=["dni", "name", "contact_name", "full_name", "iban", "payment_form",
                                      "ordinary_period", "extra_ordinary", "territory","bank", "ordinary_fee",
                                      "ordinary_fee_start"])

    banks_array = frappe.get_list("Bank", fields=["bic", "name"])
    banks_dict = {}
    for bank in banks_array:
        banks_dict[bank.name] = bank

    fees_array = frappe.get_list("Contact Fee", fields=["name"])
    fees_dict = {}
    for fee in fees_array:
        fees_dict[fee.name] = frappe.get_doc("Contact Fee", fee.name)

    receivables = []

    for contact in contacts:
        # publish task_update
        if i % 100 == 0:
            frappe.publish_realtime("remittance_progress", {"progress": [i, len(contacts)]}, user=frappe.session.user)
        i += 1

        if contact.payment_form == "Bank Transfer" and not contact.bank in banks_dict:
            frappe.logger(module="polcontact").debug(contact)
            frappe.msgprint(
                _("Bank {0} not found for contact {1}, Receivable not generated", lang=lang).format(contact.bank,
                                                                                                    contact.dni))
            continue

        bank_bic = banks_dict[contact.bank].bic if contact.bank else None

        receivable = False

        if quote_type == "Ordinary" and contact.ordinary_fee==fee_name:
            if not receivable_exists(contact.name, period, quote_type, contact.ordinary_fee):
                if amount == "Full Amount" :
                    receivable = gen_receivable(contact, period, fees_dict[contact.ordinary_fee], bank_bic)
                else:
                    receivable = gen_fixed_receivable(contact, period, fees_dict[contact.ordinary_fee], bank_bic, None, fixed_amount)
                if receivable and contact.payment_form == "Bank Transfer":
                    receivables.append(receivable.name)

        elif quote_type == "Special":
            special_entries = frappe.get_all("Contact Special Fee Table", filters={"parent": contact.name}, fields="*")
            for special_entry in special_entries:
                if special_entry.special_fee==fee_name:
                    if not receivable_exists(contact.name, period, quote_type, special_entry.special_fee):
                        if amount == "Full Amount":
                            receivable = gen_receivable(contact, period, fees_dict[special_entry.special_fee], bank_bic, special_entry)
                        else:
                            receivable = gen_fixed_receivable(contact, period, fees_dict[special_entry.special_fee], bank_bic, special_entry, fixed_amount)
                        if receivable and contact.payment_form == "Bank Transfer":
                            receivables.append(receivable.name)

    frappe.msgprint(_("Receivables generated correctly", lang=lang))

    frappe.db.commit()
    frappe.clear_cache()
    frappe.publish_realtime("doctype_clean", {"doctype": "Receivable"})
    frappe.publish_realtime("list_update", {"doctype": "Receivable"}, after_commit=True)
    return receivables

@frappe.whitelist()
def generate_remittance(description, filter_by_bank, bank_account_name, date_remittance,quote_type,remittance_type):
    bank_account = frappe.get_doc("Bank Account", bank_account_name)

    filters = {'type': 'Bank Transfer', "status_invoice": "Wallet"}

    if filter_by_bank == "1":
        iban_bank = bank_account.iban[4:8]
        ##the number could be in another position, so we need to check further instead of using substring
        filters["iban"] = ["like", "%" + iban_bank + "%"]

    if quote_type != "":
        filters["receivable_type"] = quote_type

    amount = 0
    i = 0

    receivables = frappe.get_all("Receivable", filters=filters, fields=["name"])
    if len(receivables) == 0:
        frappe.msgprint(_("No remittance generated as there are no Receivables with payment form: Bank Transfer"))

        return {
            "result": "not_ok"
        }

    doc = frappe.new_doc("Remittance")
    doc.update({"description": description,
                "account": bank_account.name,
                "date": date_remittance,
                "remittance_amount": 0,
                "unpaid_amount": 0,
                "stopped_amount": 0,
                "remittance_type": remittance_type
                })
    doc.insert(ignore_permissions=True)

    for receivable_name in receivables:
        if i % 100 == 0:
            frappe.publish_realtime("remittance_progress", {"progress": [i, len(receivables)]},
                                    user=frappe.session.user)
        i += 1
        receivable = frappe.get_doc("Receivable", receivable_name)
        if filter_by_bank == "1" and receivable.iban[4:8] != iban_bank:
            continue
        receivable.flags.ignore_update = True
        receivable.remittance = doc.name
        receivable.status_invoice = "Remittance"
        receivable.remittance_account = bank_account.name
        receivable.remittance_description = description
        receivable.remittance_date = date_remittance

        if receivable.receivable_period and receivable.receivable_period != "Annual" and receivable.receivable_type == "Ordinary":
            receivable.add_management_fee()

        amount += receivable.get_amount()
        receivable.flags.ignore_links = True
        receivable.receivable_total = receivable.get_amount()
        receivable.db_update()
    doc.update_totals()
    frappe.publish_realtime("doctype_clean", {"doctype": "Receivable"})
    frappe.publish_realtime("doctype_clean", {"doctype": "Remittance"})
    frappe.publish_realtime("list_update", {"doctype": "Remittance"}, after_commit=True)
    frappe.publish_realtime("list_update", {"doctype": "Receivable"}, after_commit=True)

    frappe.publish_realtime("remittance_progress", {"doc": doc.name}, user=frappe.session.user)

    return {
        "result": "ok",
        "name": doc.name
    }


@frappe.whitelist()
def generate_remittance_from_list(description, bank_account_name, date_remittance, receivables, lang=None, ignore_management_fee=False):
    if lang == None and frappe.session.user:
        lang = get_user_lang('frappe.session.user')

    frappe.db.begin()
    bank_account = frappe.get_doc("Bank Account", bank_account_name)

    amount = 0
    i = 0

    if len(receivables) == 0:
        frappe.publish_realtime("remittance_progress", {"error": _(
            "No remittance generated as there are no Receivables with payment form: Bank Transfer<br><br>Cash receivables were generated correctly",
            lang=lang)}, user=frappe.session.user)

        return {
            "result": "not_ok"
        }

    doc = frappe.new_doc("Remittance")
    doc.update({"description": description,
                "account": bank_account.name,
                "date": date_remittance,
                "remittance_amount": 0,
                "unpaid_amount": 0,
                "stopped_amount": 0,
                "remittance_type": "Recurring"
                })
    doc.insert(ignore_permissions=True)

    for receivable_name in receivables:
        if i % 100 == 0:
            frappe.publish_realtime("remittance_progress", {"progress": [i, len(receivables)]},
                                    user=frappe.session.user)
        i += 1
        receivable = frappe.get_doc("Receivable", receivable_name)

        receivable.flags.ignore_update = True
        receivable.remittance = doc.name
        receivable.status_invoice = "Remittance"
        receivable.remittance_account = bank_account.name
        receivable.remittance_description = description
        receivable.remittance_date = date_remittance

        if receivable.receivable_period != "Annual" and receivable.receivable_type == "Ordinary" and not ignore_management_fee:
            receivable.add_management_fee()

        amount += receivable.get_amount()
        receivable.flags.ignore_links = True
        receivable.receivable_total = receivable.get_amount()
        receivable.db_update()
    doc.update_totals()
    frappe.publish_realtime("doctype_clean", {"doctype": "Receivable"})
    frappe.publish_realtime("doctype_clean", {"doctype": "Remittance"})
    frappe.publish_realtime("list_update", {"doctype": "Remittance"}, after_commit=True)
    frappe.publish_realtime("list_update", {"doctype": "Receivable"}, after_commit=True)

    frappe.db.commit()

    frappe.publish_realtime("remittance_progress", {"doc": doc.name}, user=frappe.session.user)

@frappe.whitelist()
def generate_extra_remittance_job(quote_type, month_period, year_period, date_remittance, bank_account, fee, amount, fixed_amount):
    lang = get_user_lang(frappe.session.user)

    if quote_type != frappe.db.get_value("Bank Account", bank_account, ["bank_account_type"]):
        q_type = _(quote_type)
        msg = _(u"Selected bank account is not {0}").format(unicode(q_type))
        frappe.throw(msg)

    frappe.enqueue('polcontacts.utils.generate_extra_remittance', queue='long', timeout=600000,
                   quote_type=quote_type,
                   month_period=month_period,
                   year_period=year_period,
                   date_remittance=date_remittance,
                   bank_account=bank_account,
                   fee=fee,
                   amount=amount,
                   fixed_amount=fixed_amount,
                   lang=lang,
                   )

    return {
        "result": "ok"
    }

#from .profile import profileit

@frappe.whitelist()
#@profileit("generate_extra_remittance")
def generate_extra_remittance(quote_type, month_period, year_period, date_remittance, bank_account, fee, amount, fixed_amount, lang=None):

    if lang == None and frappe.session.user:
        lang = get_user_lang('frappe.session.user')

    quote_period = {
        "period": "Monthly",
        "biannual_period": None,
        "quarter_period": None,
        "year_period": year_period,
        "month_period": month_period
    }

    receivables = generate_extra_receivables(
        period=quote_period,
        date=date_remittance,
        quote_type=quote_type,
        fee_name= fee,
        amount= amount,
        fixed_amount=fixed_amount,
        lang=lang
    )

    quote_name = _(quote_period["month_period"], lang=lang) + " " + _(quote_period["year_period"], lang=lang)

    if quote_type == "Ordinary":
        if quote_period["period"] == "Quarterly":
            quote_name = _(quote_period["quarter_period"], lang=lang) + " " + _(quote_period["year_period"], lang=lang)
        elif quote_period["period"] == "Biannual":
            quote_name = _(quote_period["biannual_period"], lang=lang) + " " + _(quote_period["year_period"], lang=lang)
        elif quote_period["period"] == "Annual":
            quote_name = _("Annual", lang=lang) + " " + _(quote_period["year_period"], lang=lang)

    return generate_remittance_from_list(
        description=_(quote_type, lang=lang) + " " + fee + " " + quote_name,
        bank_account_name=bank_account,
        date_remittance=date_remittance,
        receivables=receivables,
        lang=lang,
        ignore_management_fee=True
    )

@frappe.whitelist()
def generate_periodic_remittance_job(quote_type, period, biannual_period, quarter_period, year_period, date_remittance,
                                     bank_account, month_period=None):
    lang = get_user_lang(frappe.session.user)

    if period == "Monthly" and quote_type == "Ordinary":
        msg = _(u"Ordinary remittance can not be monthly")
        frappe.throw(msg)

    if quote_type != frappe.db.get_value("Bank Account", bank_account, ["bank_account_type"]):
        q_type = _(quote_type)
        msg = _(u"Selected bank account is not {0}").format(unicode(q_type))
        frappe.throw(msg)

    frappe.enqueue('polcontacts.utils.generate_periodic_remittance', queue='long', timeout=60000,
                   quote_type=quote_type,
                   period=period,
                   biannual_period=biannual_period,
                   quarter_period=quarter_period,
                   year_period=year_period,
                   date_remittance=date_remittance,
                   bank_account=bank_account,
                   month_period=month_period,
                   lang=lang
                   )

    return {
        "result": "ok"
    }


@frappe.whitelist()
def generate_periodic_remittance(quote_type, period, biannual_period, quarter_period, year_period, date_remittance,
                                 bank_account, month_period=None, lang=None):
    if lang == None and frappe.session.user:
        lang = get_user_lang('frappe.session.user')

    if period == "Monthly":
        quarter_period = None
        biannual_period = None
    elif period == "Annual":
        quarter_period = None
        biannual_period = None
        month_period = None
    elif period == "Biannual":
        quarter_period = None
        month_period = None
    elif period == "Quarterly":
        biannual_period = None
        month_period = None

    quote_period = {
        "period": period,
        "biannual_period": biannual_period,
        "quarter_period": quarter_period,
        "year_period": year_period,
        "month_period": month_period
    }

    receivables = generate_receivables(
        period=quote_period,
        date=date_remittance,
        quote_type=quote_type,
        lang=lang
    )

    if quote_type == "Ordinary":
        if quote_period["period"] == "Quarterly":
            quote_name = _(quote_period["quarter_period"], lang=lang) + " " + _(quote_period["year_period"], lang=lang)
        elif quote_period["period"] == "Biannual":
            quote_name = _(quote_period["biannual_period"], lang=lang) + " " + _(quote_period["year_period"], lang=lang)
        elif quote_period["period"] == "Annual":
            quote_name = _("Annual", lang=lang) + " " + _(quote_period["year_period"], lang=lang)
    else:
        quote_name = _(quote_period["month_period"], lang=lang) + " " + _(quote_period["year_period"], lang=lang)

    return generate_remittance_from_list(
        description=_(quote_type, lang=lang) + " " + quote_name,
        bank_account_name=bank_account,
        date_remittance=date_remittance,
        receivables=receivables,
        lang=lang
    )

@frappe.whitelist()
def generateXMLRemittance(name):
    remittance = frappe.get_doc("Remittance", name)

    frappe.local.response.filename = remittance.description + ".xml"
    frappe.local.response.filecontent = remittance.generate_XML()
    frappe.local.response.type = "download"

@frappe.whitelist()
def get_approx_remittance():
    filters = {'contact_status': 'Current',
               "payment_form": ["!=", "Undefined"]}

    contacts = frappe.get_all("PolContact",
                              filters=filters,
                              fields=["ordinary_period", "extra_ordinary", "ordinary_fee"])

    fees_array = frappe.get_list("Contact Fee", fields=["name"])
    fees_dict = {}
    for fee in fees_array:
        fees_dict[fee.name] = frappe.get_doc("Contact Fee", fee.name)

    amounts = {
        "Ordinary": {
            "Annual": 0.0,
            "Biannual": 0.0,
            "Quarterly": 0.0
        },
        "Special": 0.0
    }

    for contact in contacts:
        if contact.ordinary_fee and contact.ordinary_period:
            fee = fees_dict[contact.ordinary_fee].get_fee(datetime.now().year)
            if fee is None:
                continue

            amounts["Ordinary"][contact.ordinary_period] += (fee.amount / period_divisor[contact.ordinary_period])

            if contact.extra_ordinary > 0:
                amounts["Ordinary"][contact.ordinary_period] += contact.extra_ordinary / period_divisor[
                    contact.ordinary_period]

                # special_entries = frappe.get_all("Contact Special Fee Table", filters={"parent": contact.name}, fields="*")
                # for special_entry in special_entries:
                #     if special_entry.special_period == period["period"]:
                #         if not receivable_exists(contact.name, period, quote_type, special_entry.special_fee):
                #             receivable = gen_receivable(contact, period, fees_dict[special_entry.special_fee], bank_bic,
                #                                         special_entry)
                #             if receivable and contact.payment_form == "Bank Transfer":
                #                 receivables.append(receivable.name)
                #
                # if contact.special_fee:
                #     fee = fees_dict[contact.special_fee].get_fee(datetime.now().year)
                #     if fee is None:
                #         continue
                #
                #     amounts["Special"] += fee.amount
                #     if contact.extra_special>0:
                #         amounts["Special"] += contact.extra_special

    return {
        "Ordinary": {
            "Annual": frappe.utils.data.fmt_money(amounts["Ordinary"]["Annual"]),
            "Biannual": frappe.utils.data.fmt_money(amounts["Ordinary"]["Biannual"]),
            "Quarterly": frappe.utils.data.fmt_money(amounts["Ordinary"]["Quarterly"])
        }
    }

@frappe.whitelist()
def count(doctype, filters):
    count = frappe.get_list(doctype, filters=filters, fields=["name"], as_list=True, debug=False, distinct=True)
    return len(count)

    #Cannot use count
    # filters_array = json.loads(filters)
    # count = frappe.db.count(doctype, filters=filters_array, debug=True)
    # return count

@frappe.whitelist()
def get_comm_list(doctype, filters, limit_page_start, limit_page_length):
    list = frappe.get_list(doctype, filters=filters, fields=["dni, full_name"], limit_page_start=limit_page_start,
                           limit_page_length=limit_page_length)
    return list

@frappe.whitelist()
def doc_exists(doctype, name):
    return frappe.db.exists(doctype, name) != None

@frappe.whitelist()
def set_unpaid(docs):
    documents = json.loads(docs)
    frappe.enqueue('polcontacts.utils.set_unpaid_background', queue='long', timeout=60000,
                   documents=documents
                   )

def set_unpaid_background(documents):
     current_doc = ""
     try:
        non_touched = []
        errors = []

        i = 0

        affected_remittances = []
        for doc in documents:
            current_doc = doc
            if frappe.db.exists("Receivable", doc):
                so = frappe.get_doc("Receivable", doc)
                so.flags.ignore_update = True
                
                if so.type == u"Bank Transfer" and so.status_invoice == u"Remittance":
                    repayment_fee = frappe.get_value("Bank Account", so.remittance_account, "repayment_fee")
                    so.repayment_fee += repayment_fee
                    so.receivable_total += repayment_fee
                    
                    if frappe.get_value("PolContact", so.contact, "contact_status") == "Current":
                        so.status_invoice = "Unpaid"
                    else:
                        so.status_invoice = "Stopped"
                    so.db_update()
                    if so.remittance not in affected_remittances:
                        affected_remittances.append(so.remittance)
                else:
                    non_touched.append(doc)

                if i % 10 == 0:
                    frappe.publish_realtime("unpaid_progress", {"progress": [i, len(documents)]}, user=frappe.session.user)
                i += 1

        frappe.publish_realtime("unpaid_progress", {"progress": [100, 100]}, user=frappe.session.user)

        for remittance_id in affected_remittances:
            remittance = frappe.get_doc("Remittance", remittance_id)
            remittance.update_totals()

        frappe.publish_realtime("non_touched_receivables", {"non_touched": non_touched}, user=frappe.session.user)
     except Exception as e:
        frappe.publish_realtime("error", {"msg": "Document: " + doc + "<br>Message: " +str(e)}, user=frappe.session.user)

@frappe.whitelist()
def print_stickers(filters, arguments):
    try:
        data=dict()
        filters = json.loads(filters)
        data["arguments"] = json.loads(arguments)
        data["stickers_configuration"] = frappe.get_doc("Postal Stickers Configuration", "Postal Stickers Configuration")
        #data["members_data"] = [frappe.get_doc("PolContact", "47856405Y"),]

        data["members_data"] = frappe.get_list("PolContact", filters=filters, fields=["*"])

        with tempfile.NamedTemporaryFile() as temp:
            c = canvas.Canvas(temp)
            postal_stickers_reportlab.drawpdf(c, data)
            frappe.local.response.filename = u"{name}.pdf".format(name=_("Stickers"))
            frappe.local.response.filecontent = c.getpdfdata()
            frappe.local.response.type = "download"

    except ValidationError as e:
        return frappe.respond_as_web_page(_("Report Error"),
                                   e.message,
                                   http_status_code=404, indicator_color='red')

def assign_member_numbers():
    contacts = frappe.get_list("PolContact", filters=[["founding_partner","=","0"], ["member_number","=","0"]], order_by="membership_date")
    i=0
    for name in contacts:
        contact = frappe.get_doc("PolContact", name)
        contact.assign_member_number()
        contact.db_update()
        i+=1
        print(str(i)+"/"+str(len(contacts)))


def informe_butlletes():
    from os import walk

    mypath = "/home/frappe/frappe-bench/sites/pdecat/private/files/"
    f = []

    for (dirpath, dirnames, filenames) in walk(mypath):
        f.extend(filenames)
        break

    butlletes_fitchers = []
    for file in f:
        if file[:9]=="butlleta_":
            butlletes_fitchers.append(file)

    contacts = frappe.get_list("PolContact")
    dnis = list(map(lambda x: x["name"], contacts))

    for butlleta in butlletes_fitchers:
        if butlleta[9:-4] not in dnis:
            print(butlleta)

def generate_files_butlleta():
    contacts = frappe.get_list("PolContact", filters=[["subscription_request_file", "like", "/private/files/butlleta_%"]],
                    fields=["name", "subscription_request_file"])

    for contact in contacts:
        frappe.utils.file_manager.save_url(contact["subscription_request_file"], "butlleta_"+contact["name"], "PolContact", contact["name"], "Home/Attachments", True)

def assignar_butlleta():
    import os.path
    contacts = frappe.get_list("PolContact", filters=[["subscription_request_file", "=", ""]])

    i=0
    for name_dict in contacts:
        name=name_dict["name"]

        if os.path.exists("/home/frappe/frappe-bench/sites/pdecat/private/files/butlleta_"+name+".pdf"):
            contact = frappe.get_doc("PolContact", name)
            contact.subscription_request_file = "/private/files/butlleta_"+name+".pdf"
            contact.have_request = True
            contact.db_update()
            i+=1
            print(str(i)+"/"+str(len(contacts)))

from frappe.handler import uploadfile as upload_original
@frappe.whitelist()
def uploadfile():
    frappe.form_dict.is_private = 1
    return upload_original()
