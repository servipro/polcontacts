frappe.pages['import_unpaid'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('Import Unpaid'),
		single_column: true
	});

	$(frappe.render_template('import_unpaid')).appendTo(page.body);
	page.content = $(page.body).find('.content_area');

	$("#end_xml").click(function(){

		frappe.confirm(
			__('All the receivables in the list will be marked as unpaid, do you want to continue?'),
			function(){
				frappe.call({
					"method": "polcontacts.utils.set_unpaid",
					args: {
						"docs": page.docs
					}
				});
				window.close();
			},
			function(){
				window.close();
			}
		)


	});

	$("#sepa_xml").change(function(){
		input = $("#sepa_xml").prop('files');
		if(input.length==0){
			show_alert(__('Select a file'), 5);
		}
		else{
			fr = new FileReader();
			fr.onload = received_xml;
			//fr.readAsText(file);
			fr.readAsText(input[0]);
		}
	});

	frappe.realtime.on("unpaid_progress", function(data) {
		if(data.progress) {
			frappe.hide_msgprint(true);
			page.has_progress = true;
			console.log(data.progress[0] + "/" + data.progress[1]);
			frappe.show_progress(__("Marking Unpaid"), data.progress[0], data.progress[1]);
			if(data.progress[0]==data.progress[1]){
				setTimeout(frappe.hide_progress, 1000);
			}
		}
	});

	frappe.realtime.on("non_touched_receivables", function(data) {
		debugger;
		if(data.hasOwnProperty('non_touched') && data.non_touched.length>0){
			llista = "";
			for(var i=0; i<data.non_touched.length; i++){
				llista+="<li>"+data.non_touched[i]+"</li>"
			}

			frappe.msgprint("<ul>" + llista + "</ul> <p>Aquests documents no estan en estat de remesa.</p>", 'Documents no modificats');
		}

		show_alert('Finalitzat Correctament', 5);
	});



        frappe.realtime.on("error", function(data) {
                if(data.hasOwnProperty('msg')){
			frappe.hide_progress();
                        frappe.msgprint(data.msg, 'Error');
                }
        });

	var received_xml = function(){
		$("#second_panel").hide();

		page.docs = [];

		parser = new DOMParser();
		xmlDoc = parser.parseFromString(fr.result,"text/xml");

		info = xmlDoc.getElementsByTagName("TxInfAndSts");

		$("#tabla_xml_body").html("");

		var total = 0;

		for(var i=0; i<info.length; ++i){
			var name_mandate = $(info[i]).find('Nm').html();
			var doc_mandate = $(info[i]).find('MndtId').html();
			var doc_amount = $(info[i]).find('InstdAmt').html();
			total += parseFloat(doc_amount);

			page.docs.push(doc_mandate);

			$("#tabla_xml_body").append(
				"<tr> <td>"+name_mandate+"</td> <td>"+doc_mandate+"</td> <td>"+doc_amount+" €</td> </tr>"
			)
		}

		$("#tabla_xml_body").append(
			"<tr class='bg-info' style='font-weight: bold'> <td>Total</td> <td></td> <td>"+total.toFixed(2)+" €</td> </tr>"
		)

		$("#second_panel").show();
	};

	frappe.pages['import_unpaid'].page = page;
};
