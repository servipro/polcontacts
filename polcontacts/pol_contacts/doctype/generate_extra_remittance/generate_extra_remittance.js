// Copyright (c) 2017, Pau Rosello and contributors
// For license information, please see license.txt

// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.realtime.on("remittance_progress", function(data) {
				if(data.progress) {
					frappe.hide_msgprint(true);
					console.log(data.progress[0] + "/" + data.progress[1]);
					frappe.show_progress(__("Generating Remittance"), data.progress[0], data.progress[1]);
				}
				if(data.doc) {
					show_alert(__('Remittance generated correctly'));
				    frappe.set_route("Form", "Remittance", data.doc);
				}
				if(data.error) {
					msgprint(data.error)
				}
});

frappe.ui.form.on('Generate Extra Remittance', {
	refresh: function(frm) {
		this.frm = frm;

		frm.set_value("year_period", String(new Date().getFullYear()));

		frm.page.set_primary_action(__("Generate Remittance"), function() {
            if (typeof frm.doc.date_remittance==='undefined' || frm.doc.date_remittance===''){
            	msgprint(__('Date Remmitance cannot be empty'));
                return
            }
            if (typeof frm.doc.bank_account==='undefined' || frm.doc.bank_account===''){
            	msgprint(__('Bank cannot be empty'));
                return
            }

			frappe.call({
					method: "polcontacts.utils.generate_extra_remittance_job",
					args: {
						"quote_type": frm.doc.quote_type,
						"month_period": frm.doc.month_period,
						"year_period": frm.doc.year_period,
						"date_remittance": frm.doc.date_remittance,
						"bank_account": frm.doc.bank_account,
						"fee": frm.doc.contact_fee,
						"amount": frm.doc.amount,
						"fixed_amount": frm.doc.fixed_amount
					},
					callback: function(){
						show_alert(__('Generant remesa, esperi'));
						frappe.show_progress(__("Generating Remittance"), 0, 100);
					}
			});

		});

		frm.set_query("bank_account", function() {
			return {
				filters: {
					bank_account_type: frm.doc.quote_type
				}
			}
		});

		frm.set_query("contact_fee", function() {
			return {
				filters: {
					quote_type: frm.doc.quote_type
				}
			}
		});

	}
});





frappe.ui.form.on('Generate Extra Remittance', 'quote_type' ,function(frm) {
	frm.set_value("bank_account", "");
	frm.set_value("contact_fee", "");
});

