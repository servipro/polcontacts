// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Bank Account', {
	setup: function(frm) {
		frm.set_query('city', {'is_group': 0});
	}
});

frappe.ui.form.on('Bank Account', 'iban' ,function(frm) {
    var iban = frm.doc.iban.replace(/ /g,"");
    if(polcontacts.validation.iban(iban)){
        frappe.call({
            method:"frappe.client.get_list",
            args:{
                doctype:"Bank",
                filters: [
                    ["entity", "=", iban.substring(4, 8)]
                ],
                fields: ["name","bic"]
            },
            callback: function(r) {
                frm.set_value("bank",r.message[0].name);
                frm.set_value("bic",r.message[0].bic);
                frm.set_value("recipient_bank",r.message[0].name);
                frm.set_value("recipient_bank_office",iban.substring(8, 12));
            }
        });
	}
	else{
	    frm.set_value("bank","");
	    frm.set_value("bic","");
        frm.set_value("recipient_bank","");
        frm.set_value("recipient_bank_office","");
	}
});

frappe.ui.form.on("Bank Account", "validate", function(frm) {
	if (!polcontacts.validation.iban(frm.doc.iban)) {
		msgprint(__("The introduced IBAN is not correct"));
		$("div[data-fieldname='iban']").addClass("has-error");
		validated = false;
		return false;
	}
	if (!polcontacts.utils.has_value(frm.doc.presenter_cif) || !polcontacts.validation.cif(frm.doc.presenter_cif)) {
		msgprint(__("The Presenter CIF is not correct"));
		$("div[data-fieldname='presenter_cif']").addClass("has-error");
		validated = false;
		return false;
	}
	if (!polcontacts.utils.has_value(frm.doc.presenter_sepa) || !polcontacts.validation.sepa(frm.doc.presenter_sepa)) {
		msgprint(__("The Presenter SEPA is not correct"));
		$("div[data-fieldname='presenter_sepa']").addClass("has-error");
		validated = false;
		return false;
	}
	if (frm.doc.presenter_sufix.length!=3) {
		msgprint(__("The Presenter Sufix is not correct"));
		$("div[data-fieldname='presenter_sufix']").addClass("has-error");
		validated = false;
		return false;
	}
	if (frm.doc.process.length!=2) {
		msgprint(__("The Process is not correct"));
		$("div[data-fieldname='process']").addClass("has-error");
		validated = false;
		return false;
	}

	return true;
});


cur_frm.add_fetch('city','parent_territory','region');
cur_frm.add_fetch('city','postal_code','postal_code');