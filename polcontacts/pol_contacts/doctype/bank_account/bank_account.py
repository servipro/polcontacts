# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class BankAccount(Document):
	def before_save(self):
		self.iban = self.iban.replace(" ", "")
		self.iban = self.iban.upper()
		self.presenter_cif = self.presenter_cif.upper()
		self.presenter_sepa = self.presenter_sepa.upper()
