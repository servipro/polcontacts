// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Territory', {
	setup: function(frm) {
		frm.set_query('parent_territory', {'is_group': 1});
	}
});