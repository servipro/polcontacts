/**
 * Created by pau on 17/10/2016.
 */


    frappe.treeview_settings["Territory"] = {
        breadcrumbs: "Territory",
        title: __("Territory"),
        get_tree_root: true,
        disable_add_node: true,
        get_tree_nodes: "polcontacts.pol_contacts.doctype.territory.territory.get_children"
    };


