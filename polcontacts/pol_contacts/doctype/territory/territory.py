# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from polcontacts.utils import generate_territory_cache, delete_territory_cache

@frappe.whitelist()
def get_children():
	doctype = frappe.local.form_dict.get('doctype')
	parent_field = 'parent_' + doctype.lower().replace(' ', '_')
	parent = frappe.form_dict.get("parent") or frappe.db.get_value("User", frappe.session.user, "territory")

	territories = frappe.db.sql("""select name as value,
		is_group as expandable
		from `tab{ctype}`
		where docstatus < 2
		and ifnull(`{parent_field}`,'') = %s
		order by name""".format(ctype=frappe.db.escape(doctype), parent_field=frappe.db.escape(parent_field)),
		parent, as_dict=1)

	if(frappe.form_dict.get("parent") == None):
		territories.insert(0,{"expandable":1L, "value":parent})

	return territories


class Territory(Document):

    def on_update(self):
        delete_territory_cache()
        generate_territory_cache()