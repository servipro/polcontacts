// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt



frappe.ui.form.on('Generate Remittance', {
	quote_type: function(frm){
		frm.set_value("quote_period", "Monthly");
	},
	refresh: function(frm) {
		this.frm = frm;

		frappe.realtime.on("remittance_progress", function(data) {
				if(data.progress) {
					frappe.hide_msgprint(true);
					console.log(data.progress[0] + "/" + data.progress[1]);
					frappe.show_progress(__("Generating Remittance"), data.progress[0], data.progress[1]);
				}
				if(data.doc) {
					show_alert(__('Remittance generated correctly'));
				    frappe.set_route("Form", "Remittance", data.doc);
				}
				if(data.error) {
					msgprint(data.error)
				}
		});

		var date = new Date();
		frm.set_value("month_period", date.toLocaleString("en-us", { month: "long" }));

		frm.set_value("year_period", String(new Date().getFullYear()));

		frm.page.set_primary_action(__("Generate Remittance"), function() {
			delete frappe.cur_progress;

            if (typeof frm.doc.date_remittance==='undefined' || frm.doc.date_remittance===''){
            	msgprint(__('Date Remmitance cannot be empty'));
                return
            }
            if (typeof frm.doc.bank_account==='undefined' || frm.doc.bank_account===''){
            	msgprint(__('Bank cannot be empty'));
                return
            }
            if (frm.doc.quote_period==='Monthly' && (typeof frm.doc.month_period==='undefined' || frm.doc.month_period==='')){
            	msgprint(__('Month cannot be empty'));
                return
            }

			if(frm.doc.remittance_type=="Recurring") {
				frappe.call({
					method: "polcontacts.utils.generate_periodic_remittance_job",
					args: {
						"quote_type": frm.doc.quote_type,
						"period": frm.doc.quote_period,
						"biannual_period": frm.doc.biannual_period,
						"quarter_period": frm.doc.quarter_period,
						"year_period": frm.doc.year_period,
						"month_period": frm.doc.month_period,
						"date_remittance": frm.doc.date_remittance,
						"bank_account": frm.doc.bank_account
					}
				});
			}
			else{
			    if (typeof frm.doc.description==='undefined' || frm.doc.description===''){
                    msgprint(__('Description cannot be empty'));
                    return
                }
				frappe.call({
					method:"polcontacts.utils.generate_remittance",
					args: {
						"bank_account_name": frm.doc.bank_account,
						"description": frm.doc.description,
						"date_remittance": frm.doc.date_remittance,
						"quote_type": frm.doc.quote_type,
						"filter_by_bank": 0,
						"remittance_type": "Occasional"
					}
				});
			}
		});

		frm.add_custom_button(__("Get Approx Remittance"), function(){
				frappe.call({
					method: "polcontacts.utils.get_approx_remittance",
					callback: function (r) {
						msgprint("<ul>"
									+ "<li><b>Quota Anual</b> "+ r.message["Ordinary"]["Annual"]+"€</li>"
									+ "<li><b>Quota Semestral</b> "+ r.message["Ordinary"]["Biannual"]+"€</li>"
									+ "<li><b>Quota Trimestral</b> "+ r.message["Ordinary"]["Quarterly"]+"€</li>"
									// + "<li><b>Aportació</b> "+ r.message["Special"] +"€</li>"
							+ "</ul>", __('Approx Remittance'))
					}
				});
		});

		frm.set_query("bank_account", function() {
			return {
				filters: {
					bank_account_type: frm.doc.quote_type
				}
			}
		});
	}
});





frappe.ui.form.on('Generate Remittance', 'quote_type' ,function(frm) {
	frm.set_value("bank_account", "");
});