// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('PolContact', {
	setup: function(frm) {
		frm.set_query('territory', {'is_group': 0});
		frm.set_query('city', {'is_group': 0});

    },
    onload: function(frm) {
        frm.fields_dict["subscription_request_file"].df.is_private = 1;
        frm.fields_dict["dni_copy"].df.is_private = 1;
        frm.fields_dict["talent_pool_document"].df.is_private = 1;
	},
	refresh: function(frm){
	    frm.clear_custom_buttons();

        if(cur_frm.doc.__onload) {
            var unpaid = cur_frm.doc.__onload.dashboard_info.total_unpaid;
            if (unpaid > 0) {
                frm.page.set_indicator(__("Receivables Pending"), "orange");
            }
        }

        economic_fields = ["payment_form", "iban", "bank", "different_owner", "account_owner", "ordinary_fee",
            "ordinary_period", "extra_ordinary", "ordinary_comment", "ordinary_doc"]

        if(frm.doc.__islocal != 1 && roles.indexOf("SIGEAS Economic")==-1) {
            $.each( economic_fields, function( key, value ) {
              cur_frm.set_df_property(value, "read_only", true);

            });
        }

        if(roles.indexOf("SIGEAS Nacional")!=-1 && frm.doc.contact_status == "Unsubscribed") {
            if (frm.doc.__islocal != 1) {
                frm.add_custom_button(__("Recuperar Alta"), function () {
                    frappe.prompt([],
                        function (values) {
                            frappe.call({
                                "method": "polcontacts.pol_contacts.doctype.polcontact.polcontact.set_current",
                                args: {
                                    "name": frm.doc.name
                                },
                                callback: function (data) {
                                    show_alert(__('Contact Updated'))
                                }
                            })
                        },
                        __("Recuperar Alta"),
                        'Save'
                    );
                })
            }
        }


        if(roles.indexOf("SIGEAS Read Only")==-1) {
            if (frm.doc.__islocal != 1) {
                frm.add_custom_button(__("Print Application Form"), function () {

                    frappe.prompt([
                            {
                                "default": 0,
                                "fieldname": "empty",
                                "fieldtype": "Check",
                                "label": __("Print Empty")
                            }],
                        function (values) {
                            polcontacts.pdf.pdf_doc_function(frm.doc.doctype, frm.doc.name, "print_application_form", values, __("Application Form"))
                        },
                        __('Application Form'),
                        __('Print')
                    );
                })
            }
            if (frm.doc.contact_status == "Current") {
                frm.add_custom_button(__("Deregister"), function () {
                    frappe.prompt([
                            {
                                "fieldname": "comment",
                                "fieldtype": "Long Text",
                                "label": "Comment",
                                "reqd": 1
                            }
                        ],
                        function (values) {
                            frappe.call({
                                "method": "polcontacts.pol_contacts.doctype.contact_membership_request.contact_membership_request.deregister",
                                args: {
                                    "comment": values.comment,
                                    "id": frm.doc.name
                                },
                                callback: function (data) {
                                    frappe.set_route("Form", "Contact Membership Request", data.message);
                                }
                            })
                        },
                        __("Deregister"),
                        'Save'
                    );

                });
                frm.add_custom_button(__("New Modification Request"), function () {
                    frappe.prompt([
                            {
                                "fieldname": "description",
                                "fieldtype": "Long Text",
                                "label": "Description",
                                "reqd": 1
                            }
                        ],
                        function (values) {
                            frappe.call({
                                "method": "polcontacts.pol_contacts.doctype.contact_modification_request.contact_modification_request.new_modification",
                                args: {
                                    "description": values.description,
                                    "contact": frm.doc.name
                                },
                                callback: function (data) {
                                    frappe.set_route("Form", "Contact Modification Request", data.message);
                                }
                            })
                        },
                        __("New Modification Request"),
                        'Save'
                    );

                });

                $("button:contains(" + __('New Modification Request') + ")").addClass("btn-primary").removeClass("btn-default");
                $("button:contains(" + __('Deregister') + ")").addClass("btn-info").removeClass("btn-default");
            }
            else if (frm.doc.contact_status == "Unsubscribed") {
                button = frm.add_custom_button(__("Register"), function () {
                    frappe.prompt([
                            {
                                "fieldname": "comment",
                                "fieldtype": "Long Text",
                                "label": "Comment",
                                "reqd": 1
                            }
                        ],
                        function (values) {
                            frappe.call({
                                "method": "polcontacts.pol_contacts.doctype.contact_membership_request.contact_membership_request.register_contact",
                                args: {
                                    "comment": values.comment,
                                    "id": frm.doc.name
                                },
                                callback: function (data) {
                                    frappe.set_route("Form", "Contact Membership Request", data.message);
                                }
                            })
                        },
                        __("Register"),
                        'Save'
                    );

                });

                button.addClass("btn-primary").removeClass("btn-default");
            }
        }

		frm.set_query("ordinary_fee", function() {
			return {
				filters: {
					quote_type: "Ordinary"
				}
			}
		})

        frm.set_query("special_fee","special_fee_table", function () {
            return {
                filters: {
                    quote_type: "Special"
                }
            }
        })

        frm.set_query("studies","studies_table", function (doc, cdt, cdn) {
            return {
                filters: {
                    studies_level: locals[cdt][cdn].level
                }
            }
        })

        frappe.ui.form.on("Contact Table Studies", "level", function(frm, cdt, cdn) {
            locals[cdt][cdn]["studies"]=""
            refresh_field("studies_table");
        });

        frm.set_query("position","political_responsibility", function (doc, cdt, cdn) {
            return {
                filters: {
                    type: locals[cdt][cdn].type
                }
            }
        })

        frappe.ui.form.on("Political Responsibility", "type", function(frm, cdt, cdn) {
            locals[cdt][cdn]["position"]=""
            refresh_field("political_responsibility");
        });

		if (!frm.doc.__islocal){
            if(roles.indexOf("SIGEAS Delete Contact")!=-1){
                frm.add_custom_button(__("Delete User"), function() {
                    frappe.confirm(__("This user will be completely removed, are you sure?"),
                        function(values){
                            frappe.call({
                                "method": "polcontacts.pol_contacts.doctype.polcontact.polcontact.delete",
                                args: {
                                    "name": frm.doc.name
                                },
                                callback: function (data) {
                                    show_alert(__('Contact deleted permanently'))
                                    frappe.set_route("List","PolContact");
                                }
                            })
                        }
                    );
                })
                $("button:contains("+__('Delete User')+")").addClass( "btn-danger" ).removeClass("btn-default")
            }
            if(roles.indexOf("System Manager")!=-1){
                var doc = frm.doc;

                frm.add_custom_button(__("Change DNI"), function() {

                    frappe.prompt([
                            {'fieldname': 'new_dni', 'fieldtype': 'Data', 'label': __('New DNI'), 'reqd': 1}
                        ],
                        function(data){
                            frappe.call({
                                "method": "change_dni",
                                "doc": doc,
                                "args": {
                                    "new_dni": data.new_dni
                                },
                                "callback": function (data) {
                                    setTimeout(function(){
                                        show_alert('Succesfully Changed', 5);
                                        frappe.set_route("Form","PolContact", data.message)
                                    }, 1000);
                                }
                            })
                        },
                        'New DNI',
                        'Save'
                    )
                });

                $("button:contains("+__('Change DNI')+")").addClass( "btn-danger" ).removeClass("btn-default")
            }

		}
	}
});

var autocomplete_adress = function(dir){
    frappe.call({
            method:"google_api_integration.places.autocomplete_adress",
            args: {
                "address": dir
            },
            callback: function (data) {
                var city = "";
                var region = "";
                var country = "";
                var postal_code = "";

                if(!jQuery.isEmptyObject(data) && data.message.length==1){
                    $.each(data.message[0].address_components, function(i, component){
                        if( component.types.indexOf("locality")!=-1){
                            city= component.short_name;
                        }
                        if( component.types.indexOf("administrative_area_level_2")!=-1){
                            region= component.long_name;
                        }
                        if( component.types.indexOf("country")!=-1){
                            country= component.long_name;
                        }
                        if( component.types.indexOf("postal_code")!=-1){
                            postal_code= component.short_name;
                        }
                    })
                    cur_frm.set_value("region",region);
                    cur_frm.set_value("country",country);
                }
            }
    });
}

frappe.ui.form.on('PolContact', 'postal_code' ,function(frm) {
    if(polcontacts.utils.has_value(frm.doc.city) && polcontacts.utils.has_value(frm.doc.postal_code)){
        autocomplete_adress(frm.doc.city + " " + frm.doc.postal_code)
    }
})

frappe.ui.form.on('PolContact', 'iban' ,function(frm) {
    var iban = frm.doc.iban.replace(/ /g,"");
    if(polcontacts.validation.iban(iban)){
        frappe.call({
            method:"frappe.client.get_list",
            args:{
                doctype:"Bank",
                filters: [
                    ["entity", "=", iban.substring(4, 8)]
                ],
                fields: ["name","bic"]
            },
            callback: function(r) {
                frm.set_value("bank",r.message[0].name);
            }
        });
	}
	else{
	    frm.set_value("bank","");
	}
});

//IBAN validation
// Función que devuelve los números correspondientes a cada letra
frappe.ui.form.on("PolContact", "validate", function(frm) {
	if (!polcontacts.utils.has_value(frm.doc.dni) || (!polcontacts.validation.dni(frm.doc.dni) && frm.doc.country=="Espanya")) {
            msgprint(__("The introduced DNI is not correct"));
            $("div[data-fieldname='dni']").addClass("has-error");
            $("html, body").animate({ scrollTop: $("div[data-fieldname='dni']").offset().top - 200 }, 1000);
            validated = false;
            return false;
	}

	if (frm.doc.country=="Espanya" && frm.doc.category=="Activist" && frm.doc.payment_form=="Bank Transfer" && (!polcontacts.utils.has_value(frm.doc.iban) || !polcontacts.validation.iban(frm.doc.iban.replace(/ /g,"")))) {
		msgprint(__("The introduced IBAN is not correct"));
		$("div[data-fieldname='iban']").addClass("has-error");
        $("html, body").animate({ scrollTop: $("div[data-fieldname='iban']").offset().top - 200 }, 1000);
		validated = false;
		return false;
	}
	if (!polcontacts.utils.has_value(frm.doc.mobile_phone) && !polcontacts.utils.has_value(frm.doc.phone) && !polcontacts.utils.has_value(frm.doc.email)) {
		msgprint(__("You need to provide Phone, Mobile Phone or Email"));
        $("html, body").animate({ scrollTop: $("div[data-fieldname='phone']").offset().top - 200 }, 1000);
		validated = false;
		return false;
	}
	var subscribed=0;
    $.each(frm.doc.interests, function(i, interest){
        if(interest.level=="Subscription"){
            subscribed++
        }
    });
    if(subscribed>3){
        msgprint(__("A contact can only be subscribed to 2 fields of interest"));
		validated = false;
		return false;
    }

	if (polcontacts.utils.has_value(frm.doc.interests)) {
        var interests = frm.doc.interests.map(function(item) { return item.field_of_interest});
        if(interests.length != jQuery.unique(interests).length) {
            msgprint(__("Can not have duplicated Interest Fields"));
            $("div[data-fieldname='interests']").addClass("has-error");
            $("html, body").animate({scrollTop: $("div[data-fieldname='interests']").offset().top - 200}, 1000);
            validated = false;
            return false;
        }
	}
	return true;
});

cur_frm.add_fetch('city','parent_territory','region');
cur_frm.add_fetch('city','postal_code','postal_code');

frappe.ui.form.on_change("PolContact", "territory", function(frm) {
        if(polcontacts.utils.has_value(frm.doc.territory)){
            frappe.call({
                  "method": "polcontacts.utils.get_parent_regions",
                  args: {
                      "item": frm.doc.territory
                  },
                  callback: function (data) {
                        if(data.message === undefined){
                            frm.set_value("region1","");
                            frm.set_value("region2","");
                            return;
                        }
                        if(data.message.length>0){
                            frm.set_value("region1",data.message[0]);
                            frm.set_value("region2","");
                        }
                        if(data.message.length>1){
                            frm.set_value("region2",data.message[1]);
                        }
                  }
            })
        }

})


frappe.ui.form.on_change("PolContact", "city", function(frm) {
    if(polcontacts.utils.has_value(frm.doc.city) && polcontacts.utils.has_value(frm.doc.postal_code)){
        autocomplete_adress(frm.doc.city + " " + frm.doc.postal_code)
    }

    if(typeof frm.doc.territory === 'undefined' || frm.doc.territory==""){
            frappe.call({
                  "method": "polcontacts.utils.doc_exists",
                  args: {
                      "doctype": "Territory",
                      "name": frm.doc.city
                  },
                  callback: function (data) {
                        if(data.message){
                            frm.set_value("territory",frm.doc.city);
                        }
                  }
            })

    }
    if(cur_frm.doc.city==""){
            frm.set_value("region","");
            frm.set_value("postal_code","");
    }
});

frappe.ui.form.on_change("PolContact", "payment_form", function(frm) {
    if(cur_frm.doc.payment_form=="Bank Transfer"){
        cur_frm.toggle_reqd("iban", true);
    }
    else{
        cur_frm.toggle_reqd("iban", false);
    }
});

frappe.ui.form.on("PolContact", {
    refresh: function(frm) {
        if (!frm.doc.__islocal) {
            polcontacts.utils.set_contact_indicators(frm);
        }
    }
});

//No permitir repetidos en entidades y solo las del municipio y nacional en que se encuentre el contacto
frappe.ui.form.on("Contact Table Entity", "entities_add", function(frm){
        set_query_entity(frm)
});

frappe.ui.form.on("Contact Fee",{
    onload: function(frm) {
        frm.fields_dict["special_doc"].df.is_private = 1;
	}
})
var set_query_entity = function(frm){
    var notin=[]
//    $.each(frm.doc.entities, function(i, item){
//        notin.push(item.entity_name)
//   });
	frm.set_query("entity", "entities", function(doc, cdt, cdn) {
            return {
                filters: {
                     "territory": ["in", [doc.territory,"Nacional"]],
                     "entity_name": ["not in", notin]
                }
            };
    });
}

frappe.ui.form.on("Polling Place", "polling_place_add", function(frm, dt, dn){
    frappe.model.set_value(dt, dn, "city", frm.doc.territory);
});

frappe.ui.form.on("Electoral List", "electoral_list_add", function(frm, dt, dn){
    frappe.model.set_value(dt, dn, "city", frm.doc.territory);
});

frappe.ui.form.on("Political Responsibility", "political_responsibility_add", function(frm, dt, dn){
    frappe.model.set_value(dt, dn, "city", frm.doc.territory);
});


