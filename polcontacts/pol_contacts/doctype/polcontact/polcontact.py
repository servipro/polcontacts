# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

from datetime import date, datetime

import validators, tempfile

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.model.naming import make_autoname
from frappe.model.rename_doc import rename_doc

from polcontacts.pol_contacts.doctype.contact_membership_request.contact_membership_request import register_contact
from polcontacts.reports import application_form_reportlab, postal_stickers_reportlab
from polcontacts.utils import get_all_parents, get_parent_regions
from polcontacts.validation import valid_account_number, validDNI

from reportlab.pdfgen import canvas

@frappe.whitelist()
def delete(name):
    if "SIGEAS Delete Contact" in frappe.get_roles():
        frappe.get_doc("PolContact", name).remove_complete()
        return "ok"
    else:
        frappe.throw(_("""You do no have permission to delete users"""))

@frappe.whitelist()
def set_current(name):
    if "SIGEAS Nacional" in frappe.get_roles():
        contact = frappe.get_doc("PolContact", name)
        contact.contact_status = "Current"
        contact.db_update()
        return "ok"
    else:
        frappe.throw(_("""You do no have permission to update status"""))

class PolContact(Document):

    def change_dni(self, new_dni):
        old_dni = self.dni
        self.dni = new_dni
        self.db_update()
        rename_doc("PolContact", old_dni, new_dni, force=True)
        return new_dni

    def print_application_form(self, arguments):
        data=dict()
        data["arguments"] = arguments
        data["doc"] = self
        with tempfile.NamedTemporaryFile() as temp:
            c = canvas.Canvas(temp)
            application_form_reportlab.drawpdf(c, data)
            return c.getpdfdata()

    def print_postal_stickers(self, arguments):
        data=dict()
        data["arguments"] = arguments
        data["stickers_configuration"] = frappe.get_doc("Postal Stickers Configuration", "Postal Stickers Configuration")
        data["members_data"] = [self,]
        with tempfile.NamedTemporaryFile() as temp:
            c = canvas.Canvas(temp)
            postal_stickers_reportlab.drawpdf(c, data)
            return c.getpdfdata()

    def remove_complete(self):
        self.stop_receivables(_("Contact deleted"))
        frappe.db.sql("delete from `tabPolContact` where name=%s", self.name)
        frappe.publish_realtime("list_update", {"doctype": "PolContact"}, after_commit=True)

    def validate(self):
        if self.birthdate:
            birthdate = self.birthdate

            if type(birthdate) is unicode:
                birthdate = datetime.strptime(self.birthdate, '%Y-%m-%d')

            if self.category != "Sympathizer" and calculate_age(birthdate) < 18:
                frappe.throw(_("Can not insert Underage"))

        if self.email!=None and self.email!="" and not "@" in self.email:
            frappe.throw(_("Invalid Email"))

        if not validDNI(self.dni) and self.country=="Espanya":
            frappe.throw(_("Invalid DNI"))

        if self.neighborhood:
            neighborhood = frappe.get_doc("Neighborhood", self.neighborhood)
            if self.territory != neighborhood.territory:
                frappe.throw(_("""The selected neighborhood does not belong to the territory {0}""").format(self.territory))

        # if self.phone != None and self.phone != "":
        # 	if (self.phone[0]!="8" and self.phone[0]!="9") or len(self.phone)!=9:
        # 		frappe.throw(_("""The phone seems to be incorrect. It must start with a 9 or 8 and have 9 digits"""))
        #
        # if self.mobile_phone != None and self.mobile_phone != "":
        # 	if (self.mobile_phone[0]!="6" and self.mobile_phone[0]!="7") or len(self.mobile_phone)!=9:
        # 		frappe.throw(_("""The mobile phone seems to be incorrect. It must start with a 6 or a 7 and have 9 digits"""))

        if self.category!="Sympathizer":
            if self.category=="Activist" and (not self.ordinary_fee or self.ordinary_fee==None):
                frappe.throw(_("""If the contacts is Activist, ordinary fee must be specified"""))

            fees_found = []
            for special_fee in self.special_fee_table:
                if special_fee.special_fee in fees_found:
                    frappe.throw(_("""There can not be two special fees with the same fee"""))
                else:
                    fees_found.append(special_fee.special_fee)

            if self.payment_form=="Bank Transfer" and (self.country=="" or self.country==None or self.country=="Espanya"):
                iban = self.iban.replace(" ", "").upper()
                if len(iban)!=24 or not validators.iban(iban):
                    frappe.throw(_("IBAN not valid"))
                elif not valid_account_number(iban):
                    frappe.throw(_("Account Number not valid"))
                else:
                    bank = frappe.get_all("Bank", fields=["name"], filters={"entity":iban[4:8]})
                    if len(bank)==0:
                        frappe.throw(_("Bank (Entity) {0} not present, please put in contact with administrator".format(iban[4:8])))
        else:
            self.ordinary_fee = ""

    def autoname(self):
        self.dni = self.dni.upper()
        self.name = self.dni

    def update_regions(self):
        regions = get_parent_regions(self.territory)
        if len(regions) > 0:
            self.region1 = regions[0]
            self.region2 = ""
        if len(regions) > 1:
            self.region2 = regions[1]

    def email_lower(self):
        if self.email:
            self.email = self.email.lower()

    def fix_name(self):
        self.contact_name = self.contact_name.title()
        self.surname1 = self.surname1.title()
        if self.surname2 != None:
            self.surname2 = self.surname2.title()

        self.full_name = ' '.join(filter(None, [self.contact_name, self.surname1, self.surname2]))

    def before_save(self):
        self.owner = "Administrator"

        self.update_regions()
        self.fix_name()
        self.email_lower()
        self.assign_member_number()

        if self.category != "Activist":
            self.ordinary_fee=None
            self.extra_ordinary=0

        if self.payment_form == "Cash":
            self.iban=""
        elif self.iban and (self.country=="" or self.country==None or self.country=="Espanya"):
            self.iban = self.iban.replace(" ", "").upper()
            banks = frappe.get_all("Bank", fields=["name"], filters={"entity": self.iban[4:8]})
            if len(banks)==0:
                frappe.throw(_("Could not find the bank for the provided IBAN"), exc=frappe.ValidationError)
            self.bank = banks[0].name

        for interest in self.interests:
            interest.remove_not_selected()

    def stop_receivables(self, msg):
        receivables = frappe.get_all('Receivable',
                                             filters=[	["contact", "=", self.name],
                                                        ["status_invoice", "not in", "Remittance,Stopped"],
                                                        ["status_cash", "not in", "Paid,Stopped"]
                                                     ],
                                             fields=['name']
                                         )

        for receivable_name in receivables:
            receivable = frappe.get_doc("Receivable", receivable_name)
            receivable.update_status("Stopped")
            if msg!=None:
                if receivable.comment==None:
                    receivable.comment=msg
                else:
                    receivable.comment=receivable.comment+'\n'+msg
            receivable.save()

    def on_update(self):
        if self.contact_status=="Unsubscribe Process" or self.contact_status=="Unsubscribed":
            self.stop_receivables(_("Contact is not in Current State"))
        else:
            receivables_different_iban = frappe.get_all('Receivable',
                                             filters=[
                                                        ["contact", "=", self.name],
                                                        ["iban", "!=", self.iban],
                                                        ["type", "=", "Bank Transfer"],
                                                        ["status_invoice", "!=", "Remittance"]
                                                      ],
                                             fields=['name']
                                         )

            for receivable in receivables_different_iban:
                receivable_doc = frappe.get_doc("Receivable", receivable)
                receivable_doc.iban = self.iban
                receivable_doc.save()
                frappe.msgprint(_("The receivable {0} IBAN has been updated").format(receivable.name))
            #
            # receivables_different_iban = frappe.get_all('Receivable',
            #                                             filters=[
            #                                                         ["contact", "=", self.name]
            #                                             ],
            #                                             fields=['name', 'type', 'receivable_period', 'fee', '']
            #                                             )
            #
            # for receivable in receivables_different_iban:
            #     receivable_doc = frappe.get_doc("Receivable", receivable)
            #     receivable_doc.iban = self.iban
            #     frappe.msgprint(_("The receivable {0} IBAN has been modified".format(receivable)))

    def after_insert(self):
        if not self.avoid_request:
            register_contact(_("New Signup"),self.dni)

    def onload(self):
        info = {}
        info["total_ordinary"], info["total_special"] = self.get_unpaid_total()
        self.set_onload('dashboard_info', info)

    def assign_member_number(self):
        if self.contact_status=="Current":
            member_number = None

            #founding members
            if self.founding_partner:
                member_number = "0"
            #other members
            elif self.member_number=="0" or not self.member_number:
                member_number = make_autoname("MEMBER_NUMBER.######")

            if member_number:
                self.member_number = member_number.replace("MEMBER_NUMBER", "")

    def get_unpaid_total(self):
        return get_unpaid(self.name)

def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

def get_unpaid(name):
    total_ordinary = 0
    total_special = 0

    receivables = frappe.get_all("Receivable", fields=["receivable_total", "type", "status_cash", "status_invoice", "receivable_type"],
                                 filters={"contact": name})

    for doc in receivables:
        total = 0

        if doc.type == "Bank Transfer" and doc.status_invoice != "Remittance":
            total = doc.receivable_total
        if doc.type == "Cash" and doc.status_cash != "Paid":
            total = doc.receivable_total

        if doc.receivable_type == "Ordinary":
            total_ordinary+=total
        else:
            total_special+=total

    return total_ordinary, total_special
