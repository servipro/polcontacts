frappe.listview_settings['PolContact'] = {
    colwidths: {"subject": 2},
    hide_name_column: true,
	onload: function(me){
	    me.page.add_menu_item(__("Print Stickers"), function(){
	        frappe.prompt([
                {
                   "fieldname": "column",
                   "fieldtype": "Int",
                   "label": __("Column Start Position"),
                   "default": "1",
                   "reqd": 1,
                },{
                   "fieldname": "direction",
                   "fieldtype": "Select",
                   "label": __("Print Direction"),
                   "options": "Vertical\nHorizontal",
                   "default": "Vertical",
                },{
                   "fieldname": "cbr1",
                   "fieldtype": "Column Break",
                },{
                   "fieldname": "row",
                   "fieldtype": "Int",
                   "label": __("Row Start Position"),
                   "default": "1",
                   "reqd": 1,
                }],
                function(values){
                    var filters = me.filter_list.get_filters()

                    var w = window.open(
					    frappe.urllib.get_full_url("/api/method/polcontacts.utils.print_stickers?"
                        +"filters="+encodeURIComponent(JSON.stringify(filters))
                        +"&arguments="+encodeURIComponent(JSON.stringify(values)))
				    );

                    if(!w) {
                        msgprint(__("Please enable pop-ups")); return;
                    }
                },
                __('Postal Stickers'),
                __('Print')
            );


	    })
	}
};