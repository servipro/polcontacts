// Copyright (c) 2017, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Postal Stickers Configuration', {
	reset_configuration: function(frm){
	    frm.set_value("page_upper_margin", 1.35)
	    frm.set_value("page_lower_margin", 1.35)
	    frm.set_value("page_left_margin", 0)
	    frm.set_value("page_right_margin", 0)
	    frm.set_value("columns_number", 3)
	    frm.set_value("column_width", 7.0)
	    frm.set_value("rows_number", 9)
	    frm.set_value("rows_height", 3.0)
	    frm.set_value("sticker_upper_margin", 0.3)
	    frm.set_value("sticker_lower_margin", 0.3)
	    frm.set_value("sticker_left_margin", 0.3)
	    frm.set_value("sticker_right_margin", 0.3)
	    frm.set_value("text_size", 12)
	    frm.set_value("print_border", 0)
        cur_frm.save()
	}
});
