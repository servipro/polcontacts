frappe.listview_settings['Contact Membership Request'] = {
    colwidths: {"subject": 2},
    hide_name_column: true,
    filters:[["status","=", __("Ongoing")]]
}