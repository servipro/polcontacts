# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from polcontacts.utils import get_all_parents
from frappe import _

@frappe.whitelist()
def register_contact(comment, id):
    contact = frappe.get_doc("PolContact", id)

    doc = frappe.new_doc("Contact Membership Request")
    doc.contact = contact.dni
    doc.contact_name = contact.contact_name
    doc.surnames = ' '.join(filter(None, [contact.surname1, contact.surname2]))
    doc.action = "Sign Up"
    doc.status = "Ongoing"
    doc.application_date = contact.application_date
    doc.territory = contact.territory
    territories = get_all_parents(contact.territory)
    doc.territories = ",".join(territories)
    doc.updates = []

    for territory in territories:
        doc.append("updates", {
            "action": "Pending",
            "territory": territory
        })

    doc.insert(ignore_permissions=True)
    contact.contact_status = 'Sign Up Process'
    contact.save(ignore_permissions=True)
    contact.notify_update()

    return doc.name

@frappe.whitelist()
def deregister(comment, id):
    contact = frappe.get_doc("PolContact", id)
    doc = frappe.new_doc("Contact Membership Request")
    doc.contact = id
    doc.action = "Unsubscribe"
    doc.status = "Ongoing"
    doc.comment = comment
    doc.application_date = frappe.utils.nowdate()
    doc.territory = contact.territory
    territories = get_all_parents(contact.territory)
    doc.territories = ",".join(territories)
    doc.updates = []

    for territory in territories:
        doc.append("updates", {
            "action":"Pending",
            "territory": territory
        })

    doc.insert(ignore_permissions=True)
    contact.contact_status = 'Unsubscribe Process'
    contact.save(ignore_permissions=True)
    contact.notify_update()

    return doc.name

##Cache territorio
@frappe.whitelist()
def add_update(action, comment, id):
    doc = frappe.get_doc("Contact Membership Request", id)
    territory = frappe.db.get_value("User", frappe.session.user, ["territory"])


    if territory in doc.territories:
        for update in doc.updates:
            if update.action=="Pending" and update.territory == territory:
                update.action = action
                update.comment = comment
                update.user = frappe.session.user
                update.date = frappe.utils.nowdate()
            elif update.territory == territory:
                doc.append("updates", {
                    "action": action,
                    "territory": territory,
                    "comment": comment,
                    "user": frappe.session.user,
                    "date": frappe.utils.nowdate()
                })
                break
        doc.save(ignore_permissions=True)
    else:
        frappe.throw(_("You are not allowed to update the request on this territory"))

@frappe.whitelist()
def review(action, comment, id):
    doc = frappe.get_doc("Contact Membership Request", id)
    if "SIGEAS Nacional" in frappe.get_roles():
        doc.decision=action
        doc.reviewer_comment=comment
        doc.reviewer = frappe.session.user
        doc.status = "Ended"
        doc.pending_decision = 0

        contact = frappe.get_doc("PolContact", doc.contact)
        if doc.action=="Sign Up" and doc.decision=="Approved":
            contact.contact_status = "Current"
            contact.membership_date = frappe.utils.nowdate()
            contact.ordinary_fee_start = contact.membership_date
            contact.assign_member_number()
        elif doc.action=="Unsubscribe" and doc.decision=="Approved":
            contact.contact_status = "Unsubscribed"
            contact.desubscription_date = frappe.utils.nowdate()
        elif doc.action == "Sign Up" and doc.decision == "Denied":
            contact.contact_status = "Subscription Denied"
        elif doc.action == "Unsubscribe" and doc.decision == "Denied":
            contact.contact_status = "Current"
        contact.save(ignore_permissions=True)
        doc.save(ignore_permissions=True)
        frappe.publish_realtime("doc_update", {"doctype": "PolContact","name": doc.contact})
    else:
        frappe.throw(_("You are not allowed to update the request on this territory"))

class ContactMembershipRequest(Document):
    def before_save(self):
        if self.status!="Ended":
            pending_decision = True
            for item in self.updates:
                if item.action=="Pending":
                    pending_decision=False
            self.pending_decision = pending_decision