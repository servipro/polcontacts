// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt


frappe.ui.form.on('Contact Membership Request', {
	refresh: function(frm) {
        frm.page.clear_actions_menu()
		frm.page.clear_actions()

	    if(frm.doc.status=="Ongoing"){
		    frm.add_custom_button(__("Add Comment"), function() {
			frappe.prompt([
                {
                   "fieldname": "action",
                   "fieldtype": "Select",
                   "label": "Action",
                   "options": " \nApproved\nDenied",
                   "reqd": 1,
                },
                {
                   "fieldname": "comment",
                   "fieldtype": "Long Text",
                   "label": "Action",
                   "reqd": 1
                }
                ],
                function(values){
                    frappe.call({
                        "method": "polcontacts.pol_contacts.doctype.contact_membership_request.contact_membership_request.add_update",
                        args: {
                            "action": values.action,
                            "comment": values.comment,
                            "id": frm.doc.name
                        },
                        callback: function (data) {
                            cur_frm.reload_doc()
                        }
                    })
                },
                'New Comment',
                'Save'
            );

		})
                if(roles.indexOf("SIGEAS Nacional")!=-1){
                    var button = frm.add_custom_button(__("Review Request"), function() {
                        frappe.prompt([
                            {
                               "fieldname": "action",
                               "fieldtype": "Select",
                               "label": "Action",
                               "options": " \nApproved\nDenied",
                               "reqd": 1,
                            },
                            {
                               "fieldname": "comment",
                               "fieldtype": "Long Text",
                               "label": "Action",
                               "reqd": 1
                            }
                        ],
                        function(values){
                            frappe.call({
                                "method": "polcontacts.pol_contacts.doctype.contact_membership_request.contact_membership_request.review",
                                args: {
                                    "action": values.action,
                                    "comment": values.comment,
                                    "id": frm.doc.name
                                },
                                callback: function (data) {
                                    cur_frm.reload_doc()
                                }
                            })
                        },
                        __('Review Request'),
                        __('Save')
                        );
                    })
                    button.addClass( "btn-primary" ).removeClass("btn-default")
                }
		}
	}
});
