# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

from datetime import datetime

import frappe
import validators
from frappe.model.document import Document
from frappe.desk.form.load import run_onload
from frappe import _
from polcontacts.validation import valid_account_number

period_divisor = {"Monthly":12, "Quarterly":4, "Biannual":2, "Annual":1}


@frappe.whitelist()
def update_status(status, name):
    so = frappe.get_doc("Receivable", name)
    return so.update_status(status)

class Receivable(Document):

    def validate(self):
        if not self.get_amount()>0:
            frappe.throw(_("Receivable must have amount greater than 0"))
        if self.type == "Bank Transfer" and self.iban.startswith("ES"):
            iban = self.iban.replace(" ", "").upper()
            if len(iban) != 24 or not validators.iban(iban):
                frappe.throw(_("IBAN not valid"))
            elif not valid_account_number(iban):
                frappe.throw(_("Account Number not valid"))
            else:
                bank = frappe.get_all("Bank", fields=["name"], filters={"entity": iban[4:8]})
                if len(bank) == 0:
                    frappe.throw(
                        _("Bank (Entity) {0} not present, please put in contact with administrator".format(iban[4:8])))

        return True

    def get_amount(self):
        return self.amount + self.extra + self.repayment_fee + self.management_fee

    def before_save(self):
        if self.type == "Bank Transfer":
            self.iban = self.iban.replace(" ", "").upper()
            bank = frappe.get_all("Bank", fields=["name"], filters={"entity": self.iban[4:8]})
            if len(bank) == 0:
                frappe.throw(_("Bank (Entity) {0} not present, please put in contact with administrator".format(iban[4:8])))
            else:
                bank = frappe.get_doc("Bank", bank[0].name)
                self.bank = bank.name
                self.bic = bank.bic

        self.receivable_total = self.get_amount()

    def before_insert(self):
        if self.type=="Bank Transfer":
            self.status_invoice="Wallet"
        elif self.type=="Cash":
            self.status_cash="Pending"

        if self.receivable_period:
            if not self.year_period:
                frappe.throw(_("You must select a year"))

            if self.receivable_period == "Quarterly" and not self.quarter_period:
                frappe.throw(_("You must select a quarter"))
            elif self.receivable_period == "Biannual" and not self.biannual_period:
                frappe.throw(_("You must select a semester"))
            elif self.receivable_period == "Monthly" and not self.month_period:
                frappe.throw(_("You must select a month"))

        if receivable_exists(self.contact, {
            "period": self.receivable_period,
            "year_period": self.year_period,
            "biannual_period": self.biannual_period,
            "quarter_period": self.quarter_period,
            "month_period": self.month_period
        }, self.receivable_type, self.fee):
            frappe.throw(_("This receivable with period and fee is already created"))

        contact = frappe.get_doc("PolContact", self.contact)
        self.territory = contact.territory

    def add_repayment_fee(self):
        bank_account = frappe.get_doc("Bank Account", self.remittance_account)
        self.repayment_fee += bank_account.repayment_fee

    def add_management_fee(self):
        bank_account = frappe.get_doc("Bank Account", self.remittance_account)
        if self.management_fee==0:
            self.management_fee = bank_account.management_fee

    def update_status(self, new_status):
        if self.type=="Bank Transfer":
            old_status = self.status_invoice
            self.status_invoice=new_status

            if new_status=="Wallet" and self.remittance and self.remittance != "":
                old_remittance = self.remittance
                self.remittance = ""
                self.remittance_account = ""
                self.remittance_description = ""
                self.remittance_date = ""
                self.save()

                if not self.flags.ignore_update:
                    remittance = frappe.get_doc("Remittance", old_remittance)
                    remittance.update_totals()

            elif new_status == "Unpaid":
                self.add_repayment_fee()
                self.save()
            
            else:
                self.save()

        elif self.type=="Cash":
            old_status = self.status_cash
            if new_status=="Paid":
                self.payment_date = datetime.now()
            else:
                self.payment_date = ""
            self.status_cash=new_status
            self.save()

        frappe.logger().debug("User " + frappe.session.user + " is changing receivable " + self.name + " from " + old_status + " to " + new_status)
        return "ok"

    def on_update(self):
        if not self.flags.ignore_update:
            if self.type == "Bank Transfer" and self.remittance and self.remittance != "":
                remittance = frappe.get_doc("Remittance", self.remittance)
                remittance.update_totals()

    def on_trash(self):
        if self.type == "Bank Transfer" and self.remittance and self.remittance != "":
            remittance = frappe.get_doc("Remittance", self.remittance)
            remittance.update_totals()
            if remittance.docstatus == 1:
                frappe.throw(_("Can not delete a receivable that belongs to a submitted remittance"))

    def after_delete(self):
        frappe.logger().info("User " + frappe.session.user + " deleted receivable " + self.name)
        if not self.flags.ignore_update:
            if self.type == "Bank Transfer" and self.remittance and self.remittance != "":
                remittance = frappe.get_doc("Remittance", self.remittance)
                remittance.update_totals()

def receivable_period_name(receivable_type, period, quarter_period, biannual_period, year_period, month_period):
    if period == "Quarterly":
        quote_name = _(quarter_period) + " " + _(year_period)
    elif period == "Biannual":
        quote_name = _(quarter_period) + " " + _(year_period)
    elif period == "Annual":
        quote_name = _(period) + " " + _(year_period)
    elif period == "Monthly":
        quote_name = _(month_period) + " " + _(year_period)
    else:
        quote_name = ""

    return quote_name

def receivable_exists(contact, period, receivable_type, fee):
    if period["period"] == "Monthly":
        return frappe.db.exists("Receivable", {
            "contact": contact,
            "receivable_type": receivable_type,
            "year_period": period["year_period"],
            "month_period": period["month_period"],
            "fee": fee
        })
    elif period["period"] == "Annual":
        return frappe.db.exists("Receivable", {
            "contact": contact,
            "receivable_type": receivable_type,
            "year_period": period["year_period"],
            "fee": fee
        })
    elif period["period"] == "Biannual":
        return frappe.db.exists("Receivable", {
            "contact": contact,
            "receivable_type": receivable_type,
            "year_period": period["year_period"],
            "biannual_period": period["biannual_period"],
            "fee": fee
        })
    elif period["period"] == "Quarterly":
        return frappe.db.exists("Receivable", {
            "contact": contact,
            "receivable_type": receivable_type,
            "year_period": period["year_period"],
            "quarter_period": period["quarter_period"],
            "fee": fee
        })

def get_description(receivable):
    #Calculate receivable description
    extra_info=_(receivable.fee)
    if receivable.receivable_type=="Special" and receivable.quote_origin:
        extra_info=receivable.quote_origin
    
    if receivable.amount > 0:
        if receivable.fee:
            description = _(receivable.receivable_type) + " " + extra_info + " " + receivable_period_name(receivable.receivable_type,
                                                                                                                 receivable.receivable_period,
                                                                                                                 receivable.quarter_period,
                                                                                                                 receivable.biannual_period,
                                                                                                                 receivable.year_period,
                                                                                                                 receivable.month_period) + " : " + str(receivable.amount) + " Eur"
        else:
            description = _(receivable.receivable_type) + " " + receivable_period_name( receivable.receivable_type,
                                                                                        receivable.receivable_period,
                                                                                        receivable.quarter_period,
                                                                                        receivable.biannual_period,
                                                                                        receivable.year_period,
                                                                                        receivable.month_period) + " : " + str(receivable.amount) + " Eur"
    else:
        description = extra_info

    if receivable.extra > 0:
        description += " " + _(receivable.receivable_type) + u" Voluntària " + receivable_period_name(receivable.receivable_type,
                                                                                                       receivable.receivable_period,
                                                                                                       receivable.quarter_period,
                                                                                                       receivable.biannual_period,
                                                                                                       receivable.year_period,
                                                                                                       receivable.month_period) + " : " + str(receivable.extra) + " Eur"
    if receivable.management_fee > 0:
        description += " " + _("Management Fee") +" : "+ str(receivable.management_fee) + " Eur"

    if receivable.repayment_fee > 0:
        description += " " + _("Repayment Fee") +" : "+ str(receivable.repayment_fee) + " Eur"

    return description
