frappe.listview_settings['Receivable'] = {
    colwidths: {"indicator":2, "subject": 3},
    add_columns: ["date", "receivable_total"],
    add_fields: ["date", "receivable_total", "receivable_type", "type", "status_invoice", "status_cash"],
    hide_name_column: false,
    column_render: {
		receivable_total: function(doc) {
         debugger;
		   var type = doc.receivable_type=="Ordinary"?"Q":"A";
			var html = repl("%(type)s - %(receivable_total)s€", {receivable_total: doc.receivable_total, type:type});
			return html;
		}
	},
    refresh: function(listview) {
        listview.page.clear_actions_menu()
        if(frappe.user_roles.includes('SIGEAS Economic') && frappe.user_roles.includes('SIGEAS Communications')){
            listview.page.add_action_item(__("Send Unpaid Notice Email"), function() {
                  frappe.prompt([
                     {
                        "fieldname": "date_repayment",
                        "fieldtype": "Data",
                        "label": __("Date Repayment"),
                        "reqd": 1
                     },{
                        "fieldname": "notification_level",
                        "fieldtype": "Select",
                        "label": __("Notification Level"),
                        "options": "1\n2\n3\n4\n5",
                        "reqd": 1,
                     },{
                        "fieldname": "receivable_type",
                        "fieldtype": "Select",
                        "label": __("Receivable Type"),
                        "options": "All\nOrdinary\nSpecial",
                        "reqd": 1,
                     },{
                        "fieldname": "remittance_name",
                        "fieldtype": "Link",
                        "label": __("Remittance"),
                        "options": "Remittance"
                     }],
                     function(values){
                        frappe.call({
                              method:"polcontacts.communications.send_unpaid",
                              args: {
                                 "date_repayment": values.date_repayment,
                                 "notification_level": values.notification_level,
                                 "receivable_type": values.receivable_type,
                                 "remittance_name": values.remittance_name
                              },
                              callback:function(r){
                                 msgprint(__("Unpaid Emails Queued"), __("Unpaid Emails Queued"))
                              }
                        });
                     });
            });
            listview.page.add_action_item(__("Send Unpaid Notice SMS"), function() {
                  frappe.prompt([
                     {
                        "fieldname": "date_repayment",
                        "fieldtype": "Data",
                        "label": __("Date Repayment"),
                        "reqd": 1
                     },{
                        "fieldname": "notification_level",
                        "fieldtype": "Select",
                        "label": __("Notification Level"),
                        "options": "1\n2\n3\n4\n5",
                        "reqd": 1,
                     },{
                        "fieldname": "receivable_type",
                        "fieldtype": "Select",
                        "label": __("Receivable Type"),
                        "options": "All\nOrdinary\nSpecial",
                        "reqd": 1,
                     },{
                        "fieldname": "remittance_name",
                        "fieldtype": "Link",
                        "label": __("Remittance"),
                        "options": "Remittance"
                     },{
                        "fieldname": "only_if_not_email",
                        "fieldtype": "Check",
                        "label": __("Send if no email was sent"),
                        "default": true
                     }],
                     function(values){
                        frappe.call({
                              method:"polcontacts.communications.send_unpaid_sms",
                              args: {
                                 "date_repayment": values.date_repayment,
                                 "notification_level": values.notification_level,
                                 "receivable_type": values.receivable_type,
                                 "remittance_name": values.remittance_name,
                                 "only_if_not_email": values.only_if_not_email
                              },
                              callback:function(r){
                                 msgprint(__("Unpaid SMS Queued"), __("Unpaid SMS Queued"))
                              }
                        });
                     });
            })
      }
	},
	get_indicator: function(doc) {
	    if(doc.type==="Bank Transfer") {
	        if(doc.status_invoice==="Wallet"){
	        	return [__(doc.status_invoice), "grey", "status_invoice,=," + doc.status_invoice];
	        }
	        else if(doc.status_invoice==="Remittance"){
	        	return [__(doc.status_invoice), "green", "status_invoice,=," + doc.status_invoice];
	        }
	        else if(doc.status_invoice==="Unpaid"){
	        	return [__(doc.status_invoice), "red", "status_invoice,=," + doc.status_invoice];
	        }
	        else if(doc.status_invoice==="Stopped"){
	        	return [__(doc.status_invoice), "orange", "status_invoice,=," + doc.status_invoice];
	        }
		}
		else if(doc.type==="Cash"){
		    if(doc.status_cash==="Pending"){
	        	return [__(doc.status_cash), "grey", "status_cash,=," + doc.status_cash];
	        }
	        else if(doc.status_cash==="Paid"){
	        	return [__(doc.status_cash), "green", "status_cash,=," + doc.status_cash];
	        }
	        else if(doc.status_cash==="Stopped"){
	        	return [__(doc.status_cash), "orange", "status_cash,=," + doc.status_cash];
	        }
		}
	}
}
