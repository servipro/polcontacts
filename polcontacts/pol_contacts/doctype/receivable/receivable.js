// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

var update_status = function(frm, status){
    frappe.call({
        method:"polcontacts.pol_contacts.doctype.receivable.receivable.update_status",
        args:{
            name: frm.doc.name,
            status: status
        },
        callback: function(r) {
            frm.refresh();
        }
    });
};

frappe.ui.form.on('Receivable', {
    refresh: function(frm) {
        frm.set_query("deposit_bank", function() {
			return {
				filters: {
					bank_account_type: frm.doc.receivable_type
				}
			}
        });
        
        frm.set_query("fee", function() {
			return {
				filters: {
					quote_type: frm.doc.receivable_type
				}
			}
		});

        if(frm.doc.type=="Bank Transfer"){
            if(frm.doc.status_invoice == "Unpaid") {
                frm.add_custom_button(
                    __("Mark as paid"),
                    function () {
                        update_status(frm, "Remittance")
                    },
                    __("Actions")
                );
            }
            if(frm.doc.status_invoice != "Stopped") {
                frm.add_custom_button(
                    __("Mark as stopped"),
                    function () {
                        update_status(frm, "Stopped")
                    },
                    __("Actions")
                );
            }
            if(frm.doc.status_invoice == "Remittance"){
                frm.add_custom_button(
                    __("Mark as unpaid"),
                    function(){
                        update_status(frm, "Unpaid")
                    },
                    __("Actions")
                );
                $("button:contains("+__('Actions')+")").addClass( "btn-primary" ).removeClass("btn-default")
            }
            else if(frm.doc.status_invoice != "Wallet"){
                frm.add_custom_button(
                    __("Move to wallet"),
                    function(){
                        update_status(frm, "Wallet")
                    },
                    __("Actions")
                );
                $("button:contains("+__('Actions')+")").addClass( "btn-primary" ).removeClass("btn-default")
            }

        }
        else{
            var is_paid = frm.doc.type=="Cash" && frm.doc.status_cash == "Paid";
            frm.set_df_property("iban", "read_only", is_paid? 1:0);

            if(frm.doc.status_cash != "Stopped") {
                frm.add_custom_button(
                    __("Mark as stopped"),
                    function () {
                        update_status(frm, "Stopped")
                    },
                    __("Actions")
                );
            }

            if(frm.doc.status_cash != "Paid") {
                frm.add_custom_button(
                    __("Mark as paid"),
                    function () {
                        update_status(frm, "Paid")
                    },
                    __("Actions")
                );
            }
            if(frm.doc.status_cash != "Pending") {

                frm.add_custom_button(
                    __("Mark as pending"),
                    function () {
                        update_status(frm, "Pending")
                    },
                    __("Actions")
                );
            }
            $("button:contains("+__('Actions')+")").addClass( "btn-primary" ).removeClass("btn-default")
        }

        frappe.realtime.on("doctype_clean", function(data) {
            if(frappe.get_route()[0]==="Form" && cur_frm.doc.doctype===data.doctype) {
                console.log("doctype_clean " + data.doctype);
                cur_frm.reload_doc();
            }
        });

    },
    onload: function (frm) {
        var is_remittance = frm.doc.type=="Bank Transfer" && frm.doc.status_invoice == "Remittance";
    }
});

frappe.ui.form.on('Receivable', 'iban' ,function(frm) {
    var iban = frm.doc.iban.replace(/ /g,"");
    if(polcontacts.validation.iban(iban)){
        frappe.call({
            method:"frappe.client.get_list",
            args:{
                doctype:"Bank",
                filters: [
                    ["entity", "=", iban.substring(4, 8)]
                ],
                fields: ["name","bic"]
            },
            callback: function(r) {
                frm.set_value("bank",r.message[0].name);
                frm.set_value("bic",r.message[0].bic);
            }
        });
    }
    else{
        frm.set_value("bank","");
    }
});

frappe.ui.form.on('Receivable', 'contact' ,function(frm) {
    frappe.call({
        method:"frappe.client.get_list",
        args:{
            doctype:"PolContact",
            filters: [
                ["name", "=", frm.doc.contact]
            ],
            fields: ["full_name", "iban", "payment_form"]
        },
        callback: function(r) {
            frm.set_value("contact_name",r.message[0].full_name);
            frm.set_value("type",r.message[0].payment_form);
            frm.set_value("iban",r.message[0].iban);
        }
    });
});