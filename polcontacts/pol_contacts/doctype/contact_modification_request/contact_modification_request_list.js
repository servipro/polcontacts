frappe.listview_settings['Contact Modification Request'] = {
    colwidths: {"subject": 2},
    hide_name_column: true,
    filters:[["status","=", __("Pending")]]
};