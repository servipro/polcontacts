# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

@frappe.whitelist()
def new_modification(description, contact):
    doc = frappe.new_doc("Contact Modification Request")
    doc.contact = contact
    doc.description = description
    doc.insert()

    return doc.name


class ContactModificationRequest(Document):
	pass
