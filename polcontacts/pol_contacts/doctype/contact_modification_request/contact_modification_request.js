// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Contact Modification Request', {
	onload: function(frm) {
            frm.fields_dict["document"].df.is_private = 1;
	}
});
