# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

import copy
import frappe
from frappe.model.document import Document
from polcontacts.communications import send_unpaid
from polcontacts.pol_contacts.doctype.receivable.receivable import get_description
from polcontacts.utils import generate_remittance_from_list
from sepadd import SepaDD
from unidecode import unidecode
from frappe import _

@frappe.whitelist()
def send_unpaid_email(name, date_repayment):
    return send_unpaid(name, date_repayment)

@frappe.whitelist()
def cancel(name):
    remittance = frappe.get_doc("Remittance", name)
    return remittance.cancel_remittance()

@frappe.whitelist()
def unpaid_remittance(name, date):
    original_remittance = frappe.get_doc("Remittance", name)
    receivables = frappe.get_all('Receivable',
                                 filters={'remittance': original_remittance.name,
                                          'status_invoice': "Unpaid",
                                          'type': "Bank Transfer"
                                          },
                                 fields=['name', 'contact']
                                 )

    not_current_contacts = frappe.get_all('PolContact',
                                 filters=[["contact_status", "!=", "Current"]],
                                 fields=['name']
                                 )
    not_current_contacts_array = [x.name for x in not_current_contacts]

    for receivable in copy.copy(receivables):
        if receivable.contact in not_current_contacts_array:
            receivables.remove(receivable)

    frappe.db.commit()

    new_remittance =  generate_remittance_from_list(
        description=original_remittance.description + " I",
        bank_account_name=original_remittance.account,
        date_remittance=date,
        receivables=receivables
    )

    original_remittance.update_totals()

    return new_remittance

@frappe.whitelist()
def validate_receivables_remittance(name):
    remittance = frappe.get_doc("Remittance", name)

    receivables = remittance.get_valid_receivables()

    for receivable in receivables:
        contact = frappe.get_all("PolContact",
                                 filters={"name":receivable.contact},
                                 fields=["iban", "ordinary_period"]
                                 )
        if len(contact)==0:
            frappe.throw(_("The contact does not exist".format(receivable.contact)))
        else:
            contact=contact[0]
        #Check IBAN
        if contact.iban!=receivable.iban:
            frappe.throw(_("The receivable {0} has a different IBAN than it's contact").format(receivable.name))

        if (receivable.receivable_period is None or receivable.receivable_period=="") or (receivable.receivable_period=="Monthly" and "Extra" in receivable.month_period):
            return "ok"

        #If ordinary check ordinary period
        if receivable.receivable_type == "Ordinary":
            if contact.ordinary_period != receivable.receivable_period:
                frappe.throw(_("The ordinary receivable {0} has a different period than the contact {1}").format(receivable.name, receivable.contact))
        else:
            #Check special receivables
            special_fees = frappe.get_all("Contact Special Fee Table",
                                     filters={"parent":receivable.contact},
                                     fields=["special_fee", "special_period"])

            fees_dict = {}

            for fee in special_fees:
                fees_dict[fee.special_fee] = fee.special_period

            if receivable.fee not in fees_dict:
                    frappe.throw(_("The special receivable {0} contains a fee not configured in contact {1}").format(receivable.name, receivable.contact))
            elif receivable.receivable_period != fees_dict[receivable.fee]:
                    frappe.throw(_("The special receivable {0} has a different period than the contact {1}").format(receivable.name, receivable.contact))

    return "ok"

class Remittance(Document):
    def on_update(self):
        frappe.db.sql("update tabReceivable set remittance_description=%s WHERE remittance=%s", (self.description, self.name))

    def cancel_remittance(self):
        if self.remittance_type == "Recurring":
            frappe.db.sql("Delete from tabReceivable WHERE remittance=%s", self.name)
            frappe.delete_doc("Remittance", self.name, ignore_permissions=True)
        elif self.remittance_type == "Occasional":
            frappe.db.sql("update tabReceivable set remittance=NULL, status_invoice='Wallet', remittance_account=NULL, remittance_description='',remittance_date='' WHERE remittance=%s", self.name)
            frappe.delete_doc("Remittance", self.name, ignore_permissions=True)

        frappe.publish_realtime("doctype_clean", {"doctype": "Receivable"})
        frappe.publish_realtime("list_update", {"doctype": "Remittance"}, after_commit=True)
        frappe.publish_realtime("list_update", {"doctype": "Receivable"}, after_commit=True)
        return "ok"

    def update_totals(self):
        if self.docstatus==2:
            return

        query_sum = """
            SELECT SUM(receivable_total) from `tabReceivable` where status_invoice=%s and remittance=%s
        """
        self.remittance_amount = frappe.db.sql(query_sum, ["Remittance", self.name])[0][0] or 0.0
        self.unpaid_amount = frappe.db.sql(query_sum, ["Unpaid", self.name])[0][0] or 0.0
        self.stopped_amount = frappe.db.sql(query_sum, ["Stopped", self.name])[0][0] or 0.0


        self.extra = frappe.db.sql("""SELECT SUM(extra) from `tabReceivable` where remittance=%s""", self.name)[0][0] or 0.0
        self.management_fee = frappe.db.sql("""SELECT SUM(management_fee) from `tabReceivable` where remittance=%s""", self.name)[0][0] or 0.0
        self.repayment_fee = frappe.db.sql("""SELECT SUM(repayment_fee) from `tabReceivable` where remittance=%s""", self.name)[0][0] or 0.0

        self.remittance_total = self.remittance_amount + self.unpaid_amount + self.stopped_amount

        self.save()

    def get_valid_receivables(self):
        filters = {'remittance': self.name, "status_invoice": "Remittance"}
        return  frappe.get_all("Receivable", filters=filters, fields=["*"])


    def generate_XML(self, validation=True):
        if validation:
            validate_receivables_remittance(self.name)

        bank_account = frappe.get_doc("Bank Account", self.account)
        creditor_bank = frappe.get_doc("Bank", bank_account.bank)

        config = {
            "name": bank_account.presenter_name[:22],
            "IBAN": bank_account.iban,
            "BIC": creditor_bank.bic,
            "batch": True,
            "creditor_id": bank_account.presenter_sepa,  # supplied by your bank or financial authority
            "currency": "EUR"  # ISO 4217
        }
        sepa = SepaDD(config)


        i=1
        receivables = self.get_valid_receivables()
        errors = []
        for receivable in receivables:
            if(i%100 == 0):
                frappe.publish_realtime("remittance_xml", {"progress": [i, len(receivables)]}, user=frappe.session.user)
            
            i+=1

            contact = frappe.get_doc("PolContact", receivable.contact)
            try:
                description = unidecode(get_description(receivable)[:140])
                if self.custom_receivable_description:
                    description = unidecode(self.custom_receivable_description[:140])
                payment = {
                    "name": unidecode(contact.name + " " + contact.full_name).replace("\n", ""),
                    "IBAN": receivable.iban.replace(" ",""),
                    "BIC": receivable.bic,
                    "amount": int(receivable.receivable_total*100),  # in cents
                    "type": "RCUR",  # FRST,RCUR,OOFF,FNAL
                    "collection_date": self.date,
                    "mandate_id": receivable.name,
                    "mandate_date": receivable.date,
                    "description": description
                }
                sepa.add_payment(payment)

            except:
                import sys
                errors.append(receivable.name + "," + str(receivable.receivable_total) + "," + contact.full_name)
                error = sys.exc_info()
                frappe.logger().debug("Error in contact :" + contact.name)
                frappe.logger().debug(error)

        # Render the SEPA document

        frappe.publish_realtime("remittance_xml", {"progress": [1,1]}, user=frappe.session.user)

        if errors == []:
            return sepa.export()
        else:
            errors.insert(0,"Error en els rebuts:")
            return ("\n").join(errors)
