// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt
frappe.ui.form.on('Remittance', {
    refresh: function(frm) {
        frm.page.clear_actions_menu();

        frm.add_custom_button(
            __("Delete Remittance"),
            function(){
                this.doc_name = frm.doc.name;

                frappe.confirm(
                    __('Are you sure want delete this remittance'),
                    function(){
                        frappe.call({
                            method:"polcontacts.pol_contacts.doctype.remittance.remittance.cancel",
                            args:{
                                name: cur_frm.doc.name
                            },
                            callback: function(r) {
                                frappe.set_route("List", "Remittance");
                                show_alert(__('Deleted correctly'))
                            }
                        });
                    },
                    function(){
                        window.close();
                    }
                );


            }
        );
        $("button:contains("+__('Delete Remittance')+")").addClass( "btn-danger" ).removeClass("btn-default")

        frm.add_custom_button(__("Generate XML"), function() {
           frappe.call({
               method:"polcontacts.pol_contacts.doctype.remittance.remittance.validate_receivables_remittance",
               args:{
                   name: cur_frm.doc.name,
               },
               callback: function(r) {
                   if(r.message==="ok"){
                        var anchor = document.createElement('a');
                        anchor.href = frappe.urllib.get_full_url("/api/method/polcontacts.utils.generateXMLRemittance?"+"name="+encodeURIComponent(frm.doc.name))+"&validation=false";
                        anchor.target = '_blank';
                        anchor.download = "SEPA.xml";
                        anchor.click();
                   }
               }
           });
        });

        frm.add_custom_button(__('View Receipts'), function() {
            frappe.set_route('query-report', 'Remittance Receivables',
                {remittance:frm.doc.name});
        });

        frm.add_custom_button(__('Send Mail Unpaid'), function() {

            frappe.prompt([
                 {
                    "fieldname": "date_repayment",
                    "fieldtype": "Data",
                    "label": __("Date Repayment"),
                    "reqd": 1
                 },{
                    "fieldname": "notification_level",
                    "fieldtype": "Select",
                    "label": __("Notification Level"),
                    "options": "1\n2\n3\n4\n5",
                    "reqd": 1,
                 },{
                    "fieldname": "receivable_type",
                    "fieldtype": "Select",
                    "label": __("Receivable Type"),
                    "options": "All\nOrdinary\nSpecial",
                    "reqd": 1,
                 }],
                 function(values){
                    frappe.call({
                         method:"polcontacts.communications.send_unpaid",
                         args: {
                             "date_repayment": values.date_repayment,
                             "notification_level": values.notification_level,
                             "receivable_type": values.receivable_type,
                             "remittance_name": frm.doc.name
                         },
                         callback:function(r){
                            msgprint(__("Unpaid Emails Queued"), __("Unpaid Emails Queued"))
                         }
                    });
                 });



        }, __("Unpaid"));

        frm.add_custom_button(__('Generate Unpaid Remittance'), function() {
            frappe.prompt([
                    {'fieldname': 'date', 'fieldtype': 'Date', 'label': __('Unpaid Remittance Date'), 'reqd': 1}
                ],
                function(values){

                    var me = this;
                    frappe.realtime.on("remittance_progress", function(data) {
                        if(data.progress) {
                            frappe.hide_msgprint(true);
                            me.has_progress = true;
                            console.log(data.progress[0] + "/" + data.progress[1]);
                            frappe.show_progress(__("Generating Remittance"), data.progress[0], data.progress[1]);
                        }
                        if(data.doc) {
                            show_alert(__('Remittance generated correctly'));
                            frappe.set_route("Form", "Remittance", data.doc);
                        }
                    });

                    frappe.call({
                        method:"polcontacts.pol_contacts.doctype.remittance.remittance.unpaid_remittance",
                        args:{
                            name: cur_frm.doc.name,
                            date: values.date
                        },
                        callback: function(r) {
                            if (me.has_progress) {
							    frappe.show_progress(__("Generating Remittance"), 1, 1);
							    setTimeout(frappe.hide_progress, 1000);
						    }
						    show_alert(__('Generating Remittance'))
                        }
                    });
                },
                __('Generate Remittance from Unpaid'),
                __('Generate')
            );

        }, __("Unpaid"));




        frappe.realtime.on("doctype_clean", function(data) {
            if(frappe.get_route()[0]==="Form" && cur_frm.doc.doctype===data.doctype) {
                console.log("doctype_clean " + data.doctype);
                cur_frm.reload_doc();
            }
        });

        var me = this;
        frappe.realtime.on("remittance_xml", function(data) {
            if(data.progress) {
                frappe.hide_msgprint(true);
                me.has_progress = true;
                console.log(data.progress[0] + "/" + data.progress[1]);
                frappe.show_progress(__("Generating XML"), data.progress[0], data.progress[1]);
                if(data.progress[0] == data.progress[1]){
                    frappe.show_progress(__("Generating XML"), 1, 1);
                    setTimeout(frappe.hide_progress, 1000);
                }
            }
        });

    }
});