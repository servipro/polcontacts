frappe.listview_settings['Remittance'] = {
    colwidths: {"subject": 4, "indicator":1},
    hide_name_column: false,
    onload: function(listview) {
        // listview.page.clear_actions_menu()
        // listview.page.add_action_item(__("Generate Recurring Remittance"), function() {
         //    frappe.prompt([
         //            {
         //               "fieldname": "quote_type",
         //               "fieldtype": "Select",
         //               "label": "Bank Account Type",
         //               "options": "Ordinary\nSpecial",
         //               "reqd": 1,
         //            },
         //            {
         //               "fieldname": "quote_period",
         //               "fieldtype": "Select",
         //               "options": "Quarterly\nBiannual\nAnnual",
         //               "label": __("Filter By Period"),
         //               "reqd": 1
         //            },
         //            {
         //               "fieldname": "date_remittance",
         //               "fieldtype": "Date",
         //               "label": __("Date Remittance"),
         //               "reqd": 1
         //            }
         //        ],
         //        function(values){
         //            if(values.filters=="Filter by Bank Account" && !polcontacts.utils.has_value(values.bank_account)){
         //                frappe.throw(__("You need to select a bank if filters are applied"));
         //            }
         //            else{
        //
         //                var me = this;
        //
         //                frappe.realtime.on("remittance_progress", function(data) {
         //                    if(data.progress) {
         //                        frappe.hide_msgprint(true);
         //                        me.has_progress = true;
         //                        console.log(data.progress[0] + "/" + data.progress[1]);
         //                        frappe.show_progress(__("Generating Remittance"), data.progress[0], data.progress[1]);
         //                    }
         //                });
        //
        //
         //                frappe.call({
         //                    method:"polcontacts.utils.generate_periodic_remittance",
         //                    args: {
         //                        "quote_type": values.quote_type,
         //                        "quote_period": values.quote_period,
         //                        "date_remittance": values.date_remittance
         //                    },
         //                    callback:function(r){
         //                        show_alert(__('Remittance generated correctly'));
        //
         //                        if(me.has_progress) {
         //                            frappe.show_progress(__("Generating Remittance"), 1, 1);
         //                            setTimeout(frappe.hide_progress, 1000);
         //                        }
         //                        cur_list.refresh()
         //                    }
         //                });
         //            }
         //        },
         //        __('Generate'),
         //        __('Save')
         //    );
		// });
        // listview.page.add_action_item(__("Generate Ocasional Remittance"), function() {
         //    frappe.prompt([
         //            {
         //               "fieldname": "bank_account",
         //               "fieldtype": "Link",
         //               "label": __("Destination Bank Account"),
         //               "options": "Bank Account",
         //               "reqd": 1
         //            },
         //            {
         //               "fieldname": "date_remittance",
         //               "fieldtype": "Date",
         //               "label": __("Date Remittance"),
         //               "reqd": 1
         //            },
         //            {
         //               "fieldname": "description",
         //               "fieldtype": "Data",
         //               "label": __("Description"),
         //               "reqd": 1,
         //            },
         //            {
         //               "fieldname": "filter_by_bank",
         //               "fieldtype": "Check",
         //               "label": __("Filter by Bank Account"),
         //               "default": 0
         //            },
         //            {
         //               "fieldname": "date_receivable",
         //               "fieldtype": "Date",
         //               "label": __("Filter Date Receivable"),
         //               "reqd": 0
         //            },
         //            {
         //               "fieldname": "quote_period",
         //               "fieldtype": "Select",
         //               "options": "\nMonthly\nQuarterly\nBiannual\nAnnual",
         //               "label": __("Filter By Period"),
         //               "reqd": 0
         //            },
         //        ],
         //        function(values){
         //            if(values.filters=="Filter by Bank Account" && !polcontacts.utils.has_value(values.bank_account)){
         //                frappe.throw(__("You need to select a bank if filters are applied"));
         //            }
         //            else{
        //
         //                var me = this;
        //
         //                frappe.realtime.on("remittance_progress", function(data) {
         //                    if(data.progress) {
         //                        me.has_progress = true;
         //                        console.log(data.progress[0] + "/" + data.progress[1])
         //                        frappe.show_progress(__("Generating Remittance"), data.progress[0], data.progress[1]);
         //                    }
         //                });
        //
        //
         //                frappe.call({
         //                    method:"polcontacts.utils.generate_remittance",
         //                    args: {
         //                        "bank_account": values.bank_account,
         //                        "filter_by_bank": values.filter_by_bank,
         //                        "description": values.description,
         //                        "date_remittance": values.date_remittance,
         //                        "date_receivable": values.date_receivable,
         //                        "quote_period": values.quote_period
         //                    },
         //                    callback:function(r){
         //                        if(me.has_progress) {
         //                            frappe.show_progress(__("Generating Remittance"), 1, 1);
         //                            setTimeout(frappe.hide_progress, 1000);
         //                        }
        //
         //                        cur_list.refresh()
         //                    }
         //                });
         //            }
         //        },
         //        __('Generate'),
         //        __('Save')
         //    );
		// });
	}
}
