// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Bank', {
	refresh: function(frm) {

	}
});


frappe.ui.form.on("Bank", "validate", function(frm) {
	if (frm.doc.bic.length!=11) {
		msgprint(__("The introduced BIC is not correct"));
		$("div[data-fieldname='bic']").addClass("has-error");
		validated = false;
		return false;
	}
});