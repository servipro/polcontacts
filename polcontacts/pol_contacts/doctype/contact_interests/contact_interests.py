# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

subinterest_fields = {
    "Economic":[
        "economy",
        "industry",
        "innovation",
        "business",
        "turism",
        "tic",
        "energy"
    ],
    "Social Diversity and Citizenship":[
        "social_safety",
        "ocupation",
        "pensions",
        "immigration",
        "health",
        "elder_people"
    ],
    "International Relations":[
        "european_politics",
        "international_cooperation"
    ],
    "Geographic and Environment":[

    ],
    "Institutional Relations":[
        "legal",
        "law",
        "public_administration",
        "equality"
    ],
    "Culture and Language":[

    ],
    "Sports":[

    ],
    "Agriculture Stockbreeding and Fishing":[
        "diet",
        "rural_development",
        "agriculture",
        "cattle",
        "fishing"
    ],
    "Security":[
        "police",
        "civil_proteccion",
        "emergencies",
        "defense"
    ],
    "Health":[],
    "Education":[],
    "Digital Politics":[],
    "Universities, research and knowledge transfer":[],
    "Older People":[],
    "Feminism and Equality":[],
    "Law and Justice":[]
}

class ContactInterests(Document):
    def remove_not_selected(self):
        for subinterest in subinterest_fields.keys():
            if self.field_of_interest!= subinterest:
                for field in subinterest_fields[subinterest]:
                    setattr(self,field,False)