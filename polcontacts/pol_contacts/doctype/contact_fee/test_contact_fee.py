# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and Contributors
# See license.txt
from __future__ import unicode_literals

import frappe
import unittest
import datetime
# test_records = frappe.get_test_records('Contact Fee')
from polcontacts.pol_contacts.doctype.contact_fee.contact_fee import calculate_partial_fee


class TestContactFee(unittest.TestCase):

	def test_biannual_second(self):
		quote_period = {
			"period": "Biannual",
			"biannual_period": "Second Semester",
			"quarter_period": None,
			"year_period": "2017",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,27)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,4,15)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 40)

		membership_date = datetime.datetime(2017,9,1)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 40)

	def test_biannual_first(self):
		quote_period = {
			"period": "Biannual",
			"biannual_period": "First Semester",
			"quarter_period": None,
			"year_period": "2017",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,27)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,4,15)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 40)

		membership_date = datetime.datetime(2017,9,1)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

		membership_date = datetime.datetime(2017,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

	def test_quarterly_fourth(self):
		quote_period = {
			"period": "Quarterly",
			"biannual_period": None,
			"quarter_period": "Fourth Quarter",
			"year_period": "2017",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,27)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,4,15)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,9,1)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

	def test_quarterly_third(self):
		quote_period = {
			"period": "Quarterly",
			"biannual_period": None,
			"quarter_period": "Third Quarter",
			"year_period": "2017",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,27)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,4,15)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,9,1)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

		membership_date = datetime.datetime(2017, 12, 14)
		self.assertEqual(calculate_partial_fee(80, quote_period, membership_date), None)


	def test_quarterly_second(self):
		quote_period = {
			"period": "Quarterly",
			"biannual_period": None,
			"quarter_period": "Second Quarter",
			"year_period": "2017",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,27)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,4,15)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,9,1)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

		membership_date = datetime.datetime(2017,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

	def test_quarterly(self):
		quote_period = {
			"period": "Quarterly",
			"biannual_period": None,
			"quarter_period": "First Quarter",
			"year_period": "2017",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,27)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,4,15)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

		membership_date = datetime.datetime(2017,9,1)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

		membership_date = datetime.datetime(2017,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

	def test_annual(self):
		quote_period = {
			"period": "Annual",
			"biannual_period": None,
			"quarter_period": None,
			"year_period": "2017",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,27)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

		membership_date = datetime.datetime(2017,4,15)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 60)

		membership_date = datetime.datetime(2017,9,1)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 40)

		membership_date = datetime.datetime(2017,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 20)

		membership_date = datetime.datetime(2018, 12, 12)
		self.assertEqual(calculate_partial_fee(80, quote_period, membership_date), None)

		membership_date = datetime.datetime(2016, 12, 12)
		self.assertEqual(calculate_partial_fee(80, quote_period, membership_date), 80)

	def test_different_year(self):
		quote_period = {
			"period": "Quarterly",
			"biannual_period": None,
			"quarter_period": "Second Semester",
			"year_period": "2018",
			"month_period": None
		}
		membership_date = datetime.datetime(2017,2,25)

		self.assertEqual(calculate_partial_fee(40, quote_period,membership_date), 40)

		membership_date = datetime.datetime(2018, 12, 12)
		self.assertEqual(calculate_partial_fee(80, quote_period, membership_date), 80)

		membership_date = datetime.datetime(2019,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), None)

		membership_date = datetime.datetime(2016,12,12)
		self.assertEqual(calculate_partial_fee(80, quote_period,membership_date), 80)

if __name__ == "__main__":
	frappe.connect()
	unittest.main()