frappe.listview_settings['Contact Fee'] = {
	onload: function(me){
	    me.page.add_menu_item(__("Copy Fee Year"), function(){
	        frappe.prompt([
                {
                   "fieldname": "origin",
                   "fieldtype": "Select",
                   "label": __("Year Origin"),
                   "options": "2017\n2018\n2019\n2020\n2021\n2022\n2023\n2024",
                   "default": "1",
                   "reqd": 1
                },{
                   "fieldname": "destination",
                   "fieldtype": "Select",
                   "label": __("Year Destination"),
                   "options": "2017\n2018\n2019\n2020\n2021\n2022\n2023\n2024",
                   "default": "1",
                   "reqd": 1
                }],
                function(values){
                    frappe.call({
                        "method": "polcontacts.pol_contacts.doctype.contact_fee.contact_fee.copy_fee_year",
                        freeze: true,
                        args: {
                            "origin": values.origin,
                            "destination": values.destination
                        },
                        callback: function (r) {
                            if(r.message="ok"){
                                msgprint(__('Contact Fees Updated'))
                            }
                            else{
                                show_alert(__('Error'))
                            }
                        }
                    })
                },
                __('Copy Fee Year'),
                __('Save')
            );


	    })
	}
};