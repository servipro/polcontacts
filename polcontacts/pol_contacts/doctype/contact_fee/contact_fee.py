# -*- coding: utf-8 -*-
# Copyright (c) 2015, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals

import datetime
import frappe
from frappe.model.document import Document
from frappe import _
from unidecode import unidecode


class ContactFee(Document):
    def validate(self):
        found = False
        current_year = datetime.datetime.now().year

        for i, item in enumerate(sorted(self.years, key=lambda item: item.year), start=1):
            item.idx = i
            if current_year == item.year:
                found = True

        if not found:
            frappe.throw(_("Current year must be configured"))



    def get_fee(self,year):
        for fee in self.years:
            if fee.year == int(year):
                return fee
        frappe.emit_js("frappe.msgprint(__('{}'))".format(_("Quote {} has no fee for year {}").format(unidecode(self.name), year)))
        frappe.throw(u"{} has no fee for year {}".format(unidecode(self.name), year))

def monthToNum(shortMonth):
    return{
        'January' : 1,
        'February' : 2,
        'March' : 3,
        'April' : 4,
        'May' : 5,
        'June' : 6,
        'July' : 7,
        'August' : 8,
        'September' : 9,
        'October' : 10,
        'November' : 11,
        'December' : 12,
        'Extra1': 13,
        'Extra2': 14,
        'Extra3': 15,
        'Extra4': 16
    }[shortMonth]

def calculate_partial_fee(fee, period, membership_date):
    # Inscripcion posterior a membership
    if membership_date.year > int(period["year_period"]):
        return None
    if membership_date.year < int(period["year_period"]):
        return fee

    month_check = 12

    if period["period"] == "Monthly":
        month_check = int(monthToNum(period["month_period"]))

    if period["period"] == "Biannual":
        if period["biannual_period"] == "First Semester":
            month_check=6
        else:
            month_check = 12

    if period["period"] == "Quarterly":
        if period["quarter_period"] == "First Quarter":
            month_check=3
        elif period["quarter_period"] == "Second Quarter":
            month_check=6
        elif period["quarter_period"] == "Third Quarter":
            month_check=9
        else:
            month_check=12

    if membership_date.year == int(period["year_period"]) and month_check < membership_date.month:
        return None

    membership_month = membership_date.month

    if period["period"] == "Annual":
        if membership_month < 4:
            return fee
        if membership_month < 7:
            return fee*0.75
        if membership_month < 10:
            return fee*0.50
        else:
            return fee*0.25

    if period["period"] == "Biannual":
        if membership_month < 4:
            return fee
        if membership_month < 7:
            return fee*0.5
        if membership_month < 10:
            return fee
        else:
            return fee*0.5
    else:
        return fee

@frappe.whitelist()
def copy_fee_year(origin, destination):
    contact_fees = frappe.get_list("Contact Fee")
    for item in contact_fees:
        contact_fee = frappe.get_doc("Contact Fee", item.name)
        found = any(x.year == int(destination) for x in contact_fee.years)
        origin_fee = filter(lambda x: x.year == int(origin), contact_fee.years)

        if not found and len(origin_fee)==1:
            contact_fee.append("years",{
                "year": destination,
                "amount": origin_fee[0].amount
            })

        contact_fee.save()

    return "ok"