# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from polcontacts.utils import get_all_children, get_children_from_cache
from frappe import _

def execute(filters=None):
	results = []

	territoris = list(frappe.db.sql("SELECT "
							   "region2,"
							   "region1,"
							   "territory,"
							   "SUM(IF(contact_status = 'Sign Up Process',1,0)) as 'sign_up_process',"
							   "SUM(IF(contact_status = 'Current',1,0)) as 'current', "
							   "SUM(IF(contact_status = 'Unsubscribe Process',1,0)) as 'unsubscribe_process', "
							   "SUM(IF(contact_status = 'Unsubscribed',1,0)) as 'unsubscribed', "
							   "SUM(IF(contact_status = 'Subscription Denied',1,0)) as 'subscription_denied', "
							   "SUM(IF(contact_status = 'Unsubscription Denied',1,0)) as 'unsubscription_denied' "
							   "from tabPolContact "
							   "GROUP BY territory;"))

	columns = [
		_("Region2") + ":Data:120",
		_("Region1") + ":Data:120",
		_("Territory Subscription") + ":Data:120",
		_("Sign Up Process") + ":Int:100",
		_("Current") + ":Int:100",
		_("Unsubscribe Process") + ":Int:100",
		_("Unsubscribed") + ":Int:100",
		_("Subscription Denied") + ":Int:100",
		_("Unsubscription Denied") + ":Int:100"
	]

	territory = frappe.db.get_value("User", frappe.session.user, ["territory"])
	children_territory = get_children_from_cache(territory)

	for territory in territoris[:]:
		if territory[0] not in children_territory:
			territoris.remove(territory)

	return columns, territoris
