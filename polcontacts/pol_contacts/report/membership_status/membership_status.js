// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.query_reports["Membership Status"] = {
	"filters": [
		{
			"fieldname":"territory",
			"label": __("Territory"),
			"fieldtype": "Link",
			"options": "Territory"
		},
		{
			"fieldname":"contact_status",
			"label": __("Status"),
			"fieldtype": "Select",
			"options": "\nSign Up Process\nUnsubscribe Process\nUnsubscribed\nSubscription Denied\nUnsubscription Denied"
		}
	],
	get_chart_data: function(columns, result) {
		var signup_process = 0;
		var unsubscribed = 0;
		var unsubscribe_process = 0;
		var unsubscribe_denied = 0;
		var subscribe_denied = 0;

		$.each(result, function(i, contact) {
			if(contact[3]==__("Sign Up Process")){
				signup_process+=1
			}
			else if(contact[3]==__("Unsubscribe Process")){
				unsubscribe_process+=1
			}
			else if(contact[3]==__("Unsubscribed")){
				unsubscribed+=1
			}
			else if(contact[3]==__("Subscription Denied")){
				subscribe_denied+=1
			}
			else if(contact[3]==__("Unsubscription Denied")){
				unsubscribe_denied+=1
			}
		})


		return {
			"data": {
				"x": 'x',
				"columns": [
					['x', __("Sign Up Process"), __("Unsubscribe Process"), __("Unsubscribed"), __("Subscription Denied"), __("Unsubscription Denied")],
					[__("Amount"), signup_process, unsubscribed, unsubscribe_process, subscribe_denied, unsubscribe_denied]
				],
				color: function (color, d) {
					// d will be 'id' when called for legends

					if(d.x==0){
						return '#00b33c'
					}
					else if(d.x==1){
						return "#FFA105"
					}
					else if(d.x==2){
						return "#EA0707"
					}
					else if(d.x==3){
						return "#FFA105"
					}
					else if(d.x==4){
						return "#FFA105"
					}
				}
				// rows: [['Date', 'Mins to first response']].concat(result)
			},
			"bar": {
				"width": {
					"ratio": 0.5
				}
			},
			"chart_type": 'bar',


		}
	}
};
