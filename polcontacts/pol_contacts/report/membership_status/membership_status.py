# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from polcontacts.utils import get_all_children, get_children_from_cache
from frappe import _

def execute(filters=None):
	results = []

	if not "contact_status" in filters:
		filters["contact_status"] = ["!=", "Current"]

	try:
		if filters["territory"] != "Nacional":
			filters["territory"] = ["in", get_all_children(filters["territory"])]
		else:
			filters.pop('territory', None)
	except KeyError:
		pass

	contacts = frappe.get_list("PolContact",
								 filters=filters,
								 fields=["dni","contact_name", "surname1", "surname2", "contact_status", "territory", "sex", "membership_date", "desubscription_date", "application_date"])

	for item in contacts:
		results.append(
				[item.dni, item.contact_name, item.surname1, item.surname2, _(item.contact_status), item.territory, item.sex, item.membership_date, item.desubscription_date,item.application_date])

	columns = [
		_("DNI") + ":Link/PolContact:100",
		_("Name") + ":Data:100",
		_("Surname 1") + ":Data:120",
		_("Surname 2") + ":Data:120",
		_("Status") + ":Data:100",
		_("Territory") + ":Link/Territory:120",
		_("Sex") + ":Select:75",
		_("Membership Date") + ":Date:120",
		_("Desubscription Date") + ":Date:120",
		_("Application Date") + ":Date:120"
	]

	return columns, results