# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from polcontacts.utils import get_all_children

def execute(filters=None):
	results = []
	filters["type"] = "Cash"
	try:
		if filters["territory"]!="Nacional":
			filters["territory"] = ["in", get_all_children(filters["territory"])]
		else:
			filters.pop('territory', None)

	except KeyError:
		pass

	receivables = frappe.get_list("Receivable",
				   filters=filters,
				   fields=["name", "status_cash", "amount","contact_name", "contact"])

	for item in receivables:
		results.append([item.name, _(item.status_cash), item.amount, item.contact, item.contact_name])

	columns = [
		_("Receivable") + ":Link/Receivable:120",
		_("Status Cash") + ":Data:100",
		_("Amount") + ":Currency",
		_("DNI") + ":Link/PolContact:120",
		_("Contact Name") + ":Data:200"
	]
  
	return columns, results