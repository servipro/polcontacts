# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from polcontacts.utils import get_all_children


def execute(filters=None):
	results = []
	filters["type"] = "Bank Transfer"

	try:
		if filters["territory"]!="Nacional":
			filters["territory"] = ["in", get_all_children(filters["territory"])]
		else:
			filters.pop('territory', None)
	except KeyError:
		pass

	receivables = frappe.get_list("Receivable",
				   filters=filters,
				   fields=["name", "remittance", "status_invoice", "contact_name", "contact", "amount", "extra", "management_fee", "repayment_fee","receivable_total", "notifications_sent", "sms_sent"])


	for item in receivables:
		results.append([item.remittance, item.name, _(item.status_invoice), item.contact, item.contact_name, item.amount, item.extra, item.management_fee, item.repayment_fee, item.receivable_total, item.notifications_sent, item.sms_sent])

	columns = [
		_("Remittance") + ":Link/Remittance:120",
		_("Receivable") + ":Link/Receivable:120",
		_("Status Invoice") + ":Data:100",
		_("DNI") + ":Link/PolContact:120",
		_("Contact Name") + ":Data:200",
		_("Amount") + ":Currency:60",
		_("Extra") + ":Currency:60",
		_("Management Fee") + ":Currency:60",
		_("Repayment Fee") + ":Currency:60",
		_("Total") + ":Currency:60",
		_("Emails") + ":Int:60",
		_("SMS") + ":Int:60"
	]
  
	return columns, results