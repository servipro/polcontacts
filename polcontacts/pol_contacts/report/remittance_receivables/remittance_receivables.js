// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.query_reports["Remittance Receivables"] = {
	"filters": [
		{
			"fieldname":"remittance",
			"label": __("Remittance"),
			"fieldtype": "Link",
			"options": "Remittance"
		},
		{
			"fieldname":"status_invoice",
			"label": __("Status"),
			"fieldtype": "Select",
			"options": " \nRemittance\nUnpaid\nStopped"
		},
        {
			"fieldname":"contact",
			"label": __("PolContact"),
			"fieldtype": "Link",
			"options": "PolContact"
		},
		{
			"fieldname":"territory",
			"label": __("Territory"),
			"fieldtype": "Link",
			"options": "Territory"
		},
	],
	get_chart_data: function(columns, result) {
		var remittance = 0;
		var unpaid = 0;
		var stopped = 0;

		debugger;

		$.each(result, function(i, receivable) {
			if(receivable[2]==__("Remittance")){
				remittance+=receivable[9]
			}
			else if(receivable[2]==__("Unpaid")){
				unpaid+=receivable[9]
			}
			else if(receivable[2]==__("Stopped")){
				stopped+=receivable[9]
			}
		})


		return {
			"data": {
				"x": 'x',
				"columns": [
					['x', __("Paid"), __("Unpaid"), __("Stopped")],
					[__("Amount"), remittance, unpaid, stopped]
				],
				color: function (color, d) {
					// d will be 'id' when called for legends

					if(d.x==0){
						return '#00b33c'
					}
					else if(d.x==1){
						return "#E82C0E"
					}
					else if(d.x==2){
						return "#FFA105"
					}
				}
				// rows: [['Date', 'Mins to first response']].concat(result)
			},
			"bar": {
				"width": {
					"ratio": 0.5
				}
			},
			"chart_type": 'bar',


		}
	}
};
