// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.query_reports["Unpaid Receivables"] = {
	"filters": [
		{
			"fieldname":"type",
			"label": __("Payment Form"),
			"fieldtype": "Select",
			"options": " \nBank Transfer\nCash"
		},
		{
			"fieldname":"receivable_type",
			"label": __("Receivable Type"),
			"fieldtype": "Select",
			"options": " \nOrdinary\nSpecial"
		},
        {
			"fieldname":"contact",
			"label": __("PolContact"),
			"fieldtype": "Link",
			"options": "PolContact"
		},
		{
			"fieldname":"contact_status",
			"label": __("Contact Status"),
			"fieldtype": "Select",
			"options": " \nSign Up Process\nCurrent\nUnsubscribe Process\nUnsubscribed\nSubscription Denied\nUnsubscription Denied"
		},
		{
			"fieldname":"territory",
			"label": __("Territory"),
			"fieldtype": "Link",
			"options": "Territory"
		}
	]
}
