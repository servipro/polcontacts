# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from polcontacts.utils import get_all_children


def execute(filters=None):
    results = []
    filters["status_cash"] = ["!=", "Paid"]
    filters["status_invoice"] = ["not in", ["Wallet", "Remittance"]]

    if "contact_status" in filters:
        contact_status = filters["contact_status"]
        del filters["contact_status"]
    else:
        contact_status = None

    try:
        if filters["territory"] != "Nacional":
            filters["territory"] = ["in", get_all_children(filters["territory"])]
        else:
            filters.pop('territory', None)

    except KeyError:
        pass

    receivables = frappe.get_list("Receivable",
                                  filters=filters,
                                  fields=["type", "name", "contact", "contact_name", "status_cash", "status_invoice", "receivable_type", "receivable_total", "iban", "bank", "date"],
                                  order_by="-date")

    contacts_cache = {}
    for item in receivables:
        if item.contact in contacts_cache:
            contact = contacts_cache[item.contact]
        else:
            if not frappe.db.exists("PolContact", item.contact):
                    continue
            contact = frappe.get_doc("PolContact", item.contact)
            contacts_cache[item.contact] = contact

        status = item.status_invoice
        if item.type =="Cash":
            status = item.status_cash

        if contact_status == None or contact_status == " " or contact_status == contact.contact_status:
            if item.type == "Bank Transfer":
                results.append([_(item.type), _(item.receivable_type), item.date, item.contact_name, _(contact.contact_status), contact.name, _(status), item.receivable_total, contact.phone, contact.mobile_phone, contact.region1, contact.territory, "..." + contact.iban[-4:] + " " + contact.bank])
            else:
                results.append([_(item.type), _(item.receivable_type), item.date, item.contact_name, _(contact.contact_status), contact.name, _(status), item.receivable_total, contact.phone, contact.mobile_phone, contact.region1, contact.territory, ""])

    columns = [
        _("Payment Form") + ":Data:120",
        _("Receivable Type") + ":Data:120",
        _("Creation Date") + ":Date:100",
        _("Contact Name") + ":Data:150",
        _("Contact Status") + ":Data:100",
		_("DNI") + ":Data:100",
        _("Status") + ":Data:",
        "Total Rebut" + ":Currency",
        _("Phone") + ":Data:",
        _("Mobile Phone") + ":Data:",
        _("Region1") + ":Data:120",
        _("Territory") + ":Data:120",
        _("IBAN") + ":Data:150"
    ]

    return columns, results
