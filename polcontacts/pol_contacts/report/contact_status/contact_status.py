# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from polcontacts.utils import get_all_children, get_children_from_cache
from frappe import _

def execute(filters=None):
	results = []

	if "unpaid" in filters:
		filter_unpaid = filters["unpaid"]
		del filters["unpaid"]
	else:
		filter_unpaid = ""

	filters["contact_status"] = ["=", "Current"]

	try:
		if filters["territory"] != "Nacional":
			filters["territory"] = ["in", get_all_children(filters["territory"])]
		else:
			filters.pop('territory', None)
	except KeyError:
		pass

	contacts = frappe.get_list("PolContact",
								 filters=filters,
								 fields=["dni","region1", "region2", "territory", "surname1", "surname2", "contact_name", "origin", "have_request", "payment_form", "category"],
								 order_by="region1 asc, region2 asc, territory asc, surname1 asc, contact_name asc")

	unpaid_query = """
	    SELECT tabReceivable.contact, tabReceivable.receivable_total
	        FROM tabReceivable
	        WHERE
	            (tabReceivable.`type` = "Bank Transfer" AND
	            tabReceivable.status_invoice != "Remittance")
	            OR
	            (tabReceivable.`type` = "Cash" AND
	            tabReceivable.status_cash != "Paid")
	"""
	unpaids = dict(frappe.db.sql(unpaid_query))


	for item in contacts:
		unpaid="No"

		if item.dni in unpaids:
			unpaid = "Yes"
		if filter_unpaid=="" or filter_unpaid==unpaid:
			results.append(
					[item.dni, item.region1, item.region2, item.territory, item.surname1, item.surname2, item.contact_name, item.origin, _("Yes") if item.have_request==1 else _("No"), _(item.payment_form), item.category, _(unpaid)])

	columns = [
		_("DNI") + ":Link/PolContact:100",
		_("Region1") + ":Link/Territory:120",
		_("Region2") + ":Link/Territory:120",
		_("Territory") + ":Link/Territory:120",
		_("Surname 1") + ":Data:120",
		_("Surname 2") + ":Data:120",
		_("Name") + ":Data:100",
		_("Origin") + ":Data:50",
		_("Have Request") + ":Data:50",
		_("Payment Form") + ":Select:120",
		_("Category") + ":Select:120",
		_("Unpaid") + ":Data:50"
	]

	return columns, results
