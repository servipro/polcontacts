// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.query_reports["Contact Status"] = {
	"filters": [
		{
			"fieldname":"territory",
			"label": __("Territory"),
			"fieldtype": "Link",
			"options": "Territory"
		},
		{
			"fieldname":"category",
			"label": __("Category"),
			"fieldtype": "Select",
   			"options": "\nActivist\nSympathizer\nExternal"
		},
		{
			"fieldname":"unpaid",
			"label": __("Unpaid"),
			"fieldtype": "Select",
   			"options": "\nYes\nNo"
		}
	]
};
