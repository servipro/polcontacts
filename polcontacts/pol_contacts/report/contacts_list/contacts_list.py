# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	data = frappe.get_list("PolContact",
				fields=[
					"region1",
					"territory",
					"contact_name",
					"surname1",
					"surname2",
					"mobile_phone",
					"phone",
					"email",
					"origin",
					"have_request",
					"category"],
				order_by="region1, territory, surname1, surname2, contact_name",
				as_list=True
			)



	# data = frappe.db.sql("""
	#     SELECT  region1 as comarca,
	# 			territory,
	# 			contact_name,
	# 			surname1,
	# 			surname2,
	# 			mobile_phone,
	# 			phone,
	# 			email,
	# 			origin,
	# 			IF(have_request=0, "No", "Si"),
	# 			category
	# 	FROM tabPolContact
	# 	ORDER BY comarca, territory, surname1, surname2, contact_name
	# """)

	columns = [
		_("Region1") + ":Link/Territory:150",
		_("Territory") + ":Link/Territory:120",
		_("Name") + ":Data:100",
		_("Surname 1") + ":Data:120",
		_("Surname 2") + ":Data:120",
		_("Mobile Phone") + ":Data:80",
		_("Phone") + ":Data:80",
		_("Email") + ":Data:80",
		_("Origin") + ":Data:50",
		_("Have Request") + ":Check:50",
		_("Category") + ":Select:120"
	]

	return columns, data
