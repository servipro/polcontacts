# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from polcontacts.utils import get_all_children, get_children_from_cache
from frappe import _

def execute(filters=None):
	results = []

	if "receivable_type" in filters:
		filter_receivable_type = filters["receivable_type"]
		del filters["receivable_type"]
	else:
		filter_receivable_type = ""

	if "unpaid" in filters:
		filter_unpaid = filters["unpaid"]
		del filters["unpaid"]
	else:
		filter_unpaid = ""

	if "contact_status" == " ":
		del filters["contact_status"]

	try:
		if filters["territory"] != "Nacional":
			filters["territory"] = ["in", get_all_children(filters["territory"])]
		else:
			filters.pop('territory', None)
	except KeyError:
		pass

	contacts = frappe.get_list("PolContact",
								 filters=filters,
								 fields=["name","full_name","phone", "contact_status", "mobile_phone", "email", "have_request", "territory", "payment_form"],
								 order_by="full_name asc")

	filter_receivable_type_filter = ""
	if filter_receivable_type:
		filter_receivable_type_filter = """ 
				AND
	            tabReceivable.receivable_type='"""+filter_receivable_type+"""'
		"""
	
	unpaid_query = """
			SELECT tabReceivable.contact, sum(tabReceivable.receivable_total)
	        FROM tabReceivable
	        WHERE
	        	(
		            (tabReceivable.`type` = "Bank Transfer" AND
		            tabReceivable.status_invoice != "Remittance")
		            OR
		            (tabReceivable.`type` = "Cash" AND
		            tabReceivable.status_cash != "Paid")
	            )
	            """+filter_receivable_type_filter+"""
	        GROUP BY tabReceivable.contact
	"""
	unpaids = dict(frappe.db.sql(unpaid_query))


	for item in contacts:
		if item.name in unpaids:
			results.append([item.name,item.full_name, _(item.contact_status) ,_(item.payment_form), item.phone, item.mobile_phone, item.email, _("Yes") if item.have_request==1 else _("No"), unpaids[item.name], item.territory])

	columns = [
		_("DNI") + ":Link/PolContact:100",
		_("Name") + ":Data:200",
		_("Contact Status") + ":Data:200",
		_("Payment Form") + ":Data:120",
		_("Phone") + ":Data:120",
		_("Mobile Phone") + ":Data:120",
		_("Email") + ":Data:150",
		_("Have Request") + ":Data:120",
		_("Unpaid") + ":Currency:75",
		_("Territory") + ":link/Territory:100"
	]

	return columns, results
