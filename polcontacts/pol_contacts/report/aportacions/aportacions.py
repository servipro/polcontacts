# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from polcontacts.utils import get_all_children
from datetime import date


def execute(filters=None):
    results = []
    filters["receivable_type"] = "Special"
    filters["type"] = "Bank Transfer"

    try:
        if filters["territory"] != "Nacional":
            filters["territory"] = ["in", get_all_children(filters["territory"])]
        else:
            filters.pop('territory', None)
    except KeyError:
        pass

    if "year" in filters:
        year = int(filters["year"])
        filters.pop("year")
        start = date(year, 1, 1)
        end = date(year, 12, 31)
        filters["remittance_date"] = ["between", [start, end]]

    special_type=None
    try:
        special_type = filters["special_type"]
        filters.pop('special_type', None)
    except KeyError:
        pass

    quotes = frappe.get_list("Contact Fee", fields=["name", "quote_type", "special_type"])
    quote_cache = {}
    for quote in quotes:
        quote_cache[quote.name] = quote.special_type

    remittances = frappe.get_list("Remittance", fields=["name", "date"])
    remittances_cache = {}
    for remittance in remittances:
        remittances_cache[remittance.name] = remittance.date

    receivables = frappe.get_list("Receivable",
                                  filters=filters,
                                  fields=["name", "remittance", "status_invoice", "contact_name", "contact", "amount",
                                          "extra", "management_fee", "repayment_fee", "receivable_total",
                                          "notifications_sent", "sms_sent", "fee", "territory", "quote_origin", "remittance_description"])
    for item in receivables:
        try:
            if special_type !=None:
                if quote_cache[item.fee]==special_type:
                    results.append(
                        [item.remittance, item.remittance_description, remittances_cache[item.remittance], item.name, _(item.status_invoice), item.contact, item.contact_name,
                         item.territory, item.quote_origin or "", _(quote_cache[item.fee]), item.receivable_total])
            else:
                results.append(
                    [item.remittance, item.remittance_description, remittances_cache[item.remittance], item.name, _(item.status_invoice), item.contact, item.contact_name,
                     item.territory, item.quote_origin or "", _(quote_cache[item.fee]), item.receivable_total])
        except:
            pass

    columns = [
        _("Remittance") + ":Link/Remittance:120",
        _("Remittance Description") + ":Data:200",
        _("Remittance Date") + ":Date:120",
        _("Receivable") + ":Link/Receivable:120",
        _("Status Invoice") + ":Data:100",
        _("DNI") + ":Link/PolContact:120",
        _("Contact Name") + ":Data:200",
        _("Territory Subscription") + ":Data:200",
        _("Quote Origin") + ":Data:200",
        _("Special Type") + ":Data:100",
        _("Total") + ":Currency:60"
    ]

    return columns, results
