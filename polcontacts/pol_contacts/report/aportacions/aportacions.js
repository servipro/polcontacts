// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Aportacions"] = {
	"filters": [
		{
			"fieldname":"status_invoice",
			"label": __("Status"),
			"fieldtype": "Select",
			"options": " \nWallet\nRemittance\nUnpaid\nStopped"
		},
        {
			"fieldname":"contact",
			"label": __("PolContact"),
			"fieldtype": "Link",
			"options": "PolContact"
		},
		{
			"fieldname":"territory",
			"label": __("Territory"),
			"fieldtype": "Link",
			"options": "Territory"
		},
		{
			"fieldname":"special_type",
			"label": __("Special Type"),
			"fieldtype": "Select",
			"options": " \nPosition\nTerritory"
		},
		{
            "fieldname":"year",
            "label": __("Year"),
            "fieldtype": "Select",
            "options": "\n2017\n2018\n2019\n2020\n2021\n2022",
            "default": ""
        }
	]
};
