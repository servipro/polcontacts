# coding=utf-8
# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _


def execute(filters=None):
    data = frappe.db.sql("""
		SELECT 
			contact,
			receivables.contact_name,
			LEFT(postal_code,2) as postal_code,
			SUM(total_ordinary) as 'ordinary',
			SUM(total_special) as 'special'
		FROM (
			SELECT
				contact,
				contact_name,
				receivable_type,
				SUM(CASE WHEN receivable_type = 'Ordinary' THEN amount+extra ELSE 0 END) AS total_ordinary,
				SUM(CASE WHEN receivable_type = 'Special' THEN amount+extra ELSE 0 END) AS total_special
			FROM tabReceivable
			WHERE 
				(`type` = 'Cash' AND status_cash = "Paid" AND year(payment_date) = %(year)s) OR
				(`type` = 'Bank Transfer' AND status_invoice = "Remittance" AND year(remittance_date) = %(year)s)
			GROUP BY contact, receivable_type
		) AS receivables
		LEFT JOIN tabPolContact
			ON tabPolContact.name = receivables.contact
		GROUP BY contact
	""", {"year": filters["year"]})

    columns = [
        _("DNI") + ":Link/PolContact:120",
        _("Contact Name") + ":Data:200",
        "Codi Província" + ":Data:100",
        "Import Quotes" + ":Currency:100",
        "Import Aportacions" + ":Currency:100"
    ]

    return columns, data
