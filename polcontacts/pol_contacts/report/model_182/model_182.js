// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Model 182"] = {
    "filters": [
        {
            "fieldname":"year",
            "label": __("Year"),
            "fieldtype": "Select",
            "options": "2017\n2018\n2019\n2020\n2021\n2022",
            "default": "2017"
        }
    ]
};
