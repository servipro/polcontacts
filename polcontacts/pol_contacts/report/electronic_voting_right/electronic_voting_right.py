# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import itertools
from frappe import _
from polcontacts.utils import get_children_from_cache


def execute(filters=None):
    columns = [
        "DNI:Link/PolContact:100",
        "Surname 1:Data:100",
        "Surname 2:Data:100",
        "Name:Data:100",
        "Territory:Link/Territory:100",
        "Region1:Data:100",
		"Birthdate:Date:100",
		"Email:Data:100",
		"Have Request:Data:100"
    ]

    if filters["territory"]=="Nacional":
        territories_sql=""
        territories=""
    else:
        territories_sql="AND territory IN %(territories)s"
        territories = get_children_from_cache(filters["territory"])

    if "category" in filters:
        category = filters["category"]
        categories_sql="AND category = %(category)s"
    else:
        category = ""
        categories_sql = ""

    if "receivable_date" in filters:
        receivable_date = filters["receivable_date"]
        receivable_date_sql="AND date < %(receivable_date)s"
    else:
        receivable_date = ""
        receivable_date_sql = ""
        
    contact_query = """
        SELECT name, surname1, surname2, contact_name , territory, region1, birthdate, membership_date, contact_status, IF(have_request='1', \""""+_("Yes")+"""\",\""""+_("No")+"""\"),
            TIMESTAMPDIFF(MONTH, membership_date, CURDATE()) as months_old, email, birthdate, payment_form
        FROM tabPolContact
        WHERE
            contact_status='Current' AND
            payment_form IN ('Bank Transfer','Cash') AND
            TIMESTAMPDIFF(MONTH, membership_date, %(date)s) > 2
            """+ territories_sql + categories_sql +"""
        ORDER BY territory, surname1, surname2, contact_name
        """

    contacts = list(frappe.db.sql(contact_query,{"date": filters["date"], "territories": territories, "category":category}))


    unpaid_query = """
        SELECT tabReceivable.contact, tabReceivable.receivable_total
            FROM tabReceivable
            WHERE
                (tabReceivable.`receivable_type` = "Ordinary" AND (
                (tabReceivable.`type` = "Bank Transfer" AND
                tabReceivable.status_invoice != "Remittance") """ + receivable_date_sql + """ )
                OR
                (tabReceivable.`type` = "Cash" AND
                tabReceivable.status_cash != "Paid") """ + receivable_date_sql + """ ) 
    """

    unpaids = dict(frappe.db.sql(unpaid_query, {"receivable_date": receivable_date}))

    if "field_of_interest" in filters:
        field_of_interest_str = filters["field_of_interest"]

        subscribed_query = """
            SELECT parent
                FROM `tabContact Interests`
                WHERE `level`="Subscription" AND
                field_of_interest = %(field_of_interest)s
        """
        subscribed = list(itertools.chain.from_iterable(frappe.db.sql(subscribed_query, {"field_of_interest": field_of_interest_str}, as_list=True)))

    territory = frappe.db.get_value("User", frappe.session.user, ["territory"])
    children_territory = get_children_from_cache(territory)

    contact_result = []
    for contact in contacts[:]:
        if contact[0] in unpaids or contact[4] not in children_territory:
            contacts.remove(contact)
        elif "field_of_interest" in filters and not contact[0] in subscribed:
            contacts.remove(contact)
        else:
            contact_result.append([contact[0], contact[1], contact[2], contact[3], contact[4], contact[5], contact[12], contact[11],contact[9]])

    return columns, contact_result
