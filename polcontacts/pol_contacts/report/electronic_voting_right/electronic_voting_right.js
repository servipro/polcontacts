// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Electronic Voting Right"] = {
	"filters": [
	    {
			"fieldname":"date",
			"label": __("Voting Date"),
			"fieldtype": "Date",
			"reqd": 1
		},
	    {
			"fieldname":"territory",
			"label": __("Territory"),
			"fieldtype": "Link",
			"options": "Territory",
			"reqd": 1
		},
		{
		    "fieldname": "field_of_interest",
            "fieldtype": "Select",
            "label": "Field of Interest",
		    "options": "\nEconomic\nSocial Diversity and Citizenship\nInternational Relations\nGeographic and Environment\nInstitutional Relations\nCulture and Language\nSports\nAgriculture Stockbreeding and Fishing\nSecurity\nHealth\nEducation\nDigital Politics\nUniversities, research and knowledge transfer\nOlder People\nFeminism and Equality\nLaw and Justice",
		},
		{
			"fieldname":"category",
			"label": __("Category"),
			"fieldtype": "Select",
			"options": "\nActivist\nSympathizer\nExternal"
		},
		{
			"fieldname":"receivable_date",
			"label": __("Receivable Date"),
			"fieldtype": "Date"
		}
	]
}
