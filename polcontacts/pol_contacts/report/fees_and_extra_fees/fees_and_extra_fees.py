# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.database import Database
from polcontacts.utils import get_all_children


def execute(filters=None):
    if "group_by" in filters:
        group_by = filters["group_by"]
        filters.pop('group_by', None)

        if group_by == "Region1":
            return group_by_comarca(filters)
        elif group_by == "Region2":
            return group_by_vegueria(filters)
        elif group_by == "Quote Origin":
            return group_by_origin(filters)
    else:
        return get_full_data(filters)


def group_by_origin(filters):
    where, values = gen_where(filters)

    data = frappe.db.sql("""
				SELECT
				  rec.quote_origin,
				  SUM(rec.receivable_total)
				FROM tabReceivable as rec
				""" + where + """
				GROUP BY quote_origin
				ORDER BY quote_origin
			""", values)

    columns = [
        _("Origin") + ":Data:400",
        _("Total") + ":Currency:80"
    ]

    return columns, data


def group_by_vegueria(filters):
    where, values = gen_where(filters)

    data = frappe.db.sql("""
				SELECT
				  contact.region2 as vegueria,
				  SUM(rec.receivable_total)
				FROM tabReceivable as rec
				LEFT JOIN tabPolContact as contact on rec.contact = contact.name
				""" + where + """
				GROUP BY vegueria
				ORDER BY vegueria
			""", values)

    columns = [
        _("Region2") + ":Data:200",
        _("Total") + ":Currency:80"
    ]

    return columns, data


def group_by_comarca(filters):
    where, values = gen_where(filters)

    data = frappe.db.sql("""
				SELECT
				  contact.region2 as vegueria,
				  contact.region1 as comarca,
				  SUM(rec.receivable_total)
				FROM tabReceivable as rec
				LEFT JOIN tabPolContact as contact on rec.contact = contact.name
				""" + where + """
				GROUP BY comarca
				ORDER BY vegueria, comarca
			""", values)

    columns = [
        _("Region2") + ":Data:200",
        _("Region1") + ":Data:200",
        _("Total") + ":Currency:80"
    ]

    return columns, data


def get_full_data(filters):
    where, values = gen_where(filters)
    data = frappe.db.sql("""
			SELECT
			  contact.region2 as vegueria,
			  contact.region1 as comarca,
			  contact.territory as adscripcio,
			  contact.full_name as contact_name,
			  rec.receivable_type,
			  rec.quote_origin,
			  rec.date,
			  rec.receivable_total
			FROM tabReceivable as rec
			LEFT JOIN tabPolContact as contact on rec.contact = contact.name
			""" + where + """
			ORDER BY vegueria, comarca, adscripcio, contact_name, rec.name
		""", values)

    data = [list(i) for i in data]

    columns = [
        _("Region2") + ":Data:100",
        _("Region1") + ":Data:200",
        _("Territory") + ":Data:200",
        _("Contact Name") + ":Data:200",
        _("Receivable Type") + ":Data:100",
        _("Quote Origin") + ":Data:100",
        _("Date") + ":Date:90",
        _("Receivable Total") + ":Currency:80"
    ]

    for item in data:
        item[4] = _(item[4])

    return columns, data


def gen_where(filters):
    filters["type"] = "Bank Transfer"

    if "territory" in filters:
        if filters["territory"] != "Nacional":
            filters["rec.territory"] = ["in", get_all_children(filters["territory"])]
        filters.pop('territory', None)

    if "receivable_type" in filters:
        filters["rec.receivable_type"] = ["=", filters["receivable_type"]]
        filters.pop('receivable_type', None)

    if "initial_date" in filters:
        filters["rec.date"] = [">", filters["initial_date"]]
        filters.pop('initial_date', None)

    if "final_date" in filters:
        filters["rec.date"] = ["<", filters["final_date"]]
        filters.pop('final_date', None)



    where, values = frappe.db.build_conditions(filters)
    where = where.replace("`rec.receivable_type`", "rec.`receivable_type`")
    where = where.replace("`rec.territory`", "rec.`territory`")
    where = where.replace("`rec.date`", "rec.`date`")

    return "WHERE " + where, values
