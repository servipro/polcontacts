// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Fees and Extra Fees"] = {
	"filters": [
			{
				"fieldname": "territory",
				"label": __("Territory"),
				"fieldtype": "Link",
				"options": "Territory"
			},
			{
				"fieldname":"status_invoice",
				"label": __("Status"),
				"fieldtype": "Select",
				"options": " \nWallet\nRemittance\nUnpaid\nStopped"
			},
			{
				"fieldname": "receivable_type",
				"label": __("Receivable Type"),
				"fieldtype": "Select",
				"options": " \nSpecial\nOrdinary"
			},
			{
				"fieldname":"initial_date",
				"label": __("Initial Date"),
				"fieldtype": "Date"
			},
			{
				"fieldname":"final_date",
				"label": __("Final Date"),
				"fieldtype": "Date"
			},
			{
				"fieldname": "group_by",
				"label": __("Group By"),
				"fieldtype": "Select",
				"options": " \nRegion1\nRegion2\nQuote Origin"
			}
	]
};
