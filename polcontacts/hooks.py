# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "polcontacts"
app_title = "Pol Contacts"
app_publisher = "Pau Rosello"
app_description = "Politics Contacts"
app_icon = "octicon octicon-person"
app_color = "blue"
app_email = "paurosello@gmail.com"
app_license = "UNLICENSED"

fixtures = ["Custom Field", "Desktop Icon", "Translation", "Role Profile"]

permission_query_conditions = {
	"PolContact": "polcontacts.utils.polcontact_query_conditions",
	"Territory": "polcontacts.utils.entity_unique_query_conditions",
	"Entity": "polcontacts.utils.entity_query_conditions",
	"Contact Membership Request": "polcontacts.utils.contact_membership_query_conditions",
	"Contact Modification Request": "polcontacts.utils.territory_query_conditions",
	"Email Configuration": "polcontacts.utils.territory_query_conditions"
}

has_permission = {
	"Entity": "polcontacts.utils.has_permission_entity",
	"Contact Membership Request": "polcontacts.utils.has_permission_contact_membership",
}

app_include_js = [
	"assets/js/polcontacts.js",
	"assets/js/raven.js",
	"assets/js/pdf.js"
]
# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = [
	"/assets/polcontacts/css/polcontacts.css"
]
# app_include_js = "/assets/polcontacts/js/polcontacts.js"

# include js, css files in header of web template
# web_include_css = "/assets/polcontacts/css/polcontacts.css"
# web_include_js = "/assets/polcontacts/js/polcontacts.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "polcontacts.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "polcontacts.install.before_install"
after_install = "polcontacts.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

notification_config = "polcontacts.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"polcontacts.tasks.all"
# 	],
# 	"daily": [
# 		"polcontacts.tasks.daily"
# 	],
# 	"hourly": [
# 		"polcontacts.tasks.hourly"
# 	],
# 	"weekly": [
# 		"polcontacts.tasks.weekly"
# 	]
# 	"monthly": [
# 		"polcontacts.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "polcontacts.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "polcontacts.event.get_events"
# }

override_whitelisted_methods = {
    "uploadfile": "polcontacts.utils.uploadfile"
}
