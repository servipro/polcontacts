// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Simple Budget by Region"] = {
	"filters": [
		{
			"fieldname":"year",
			"label": __("Year"),
			"fieldtype": "Select",
			"options": "2016\n2017\n2018\n2019\n2020\n2021\n2022\n2023\n2024",
			"default": (new Date()).getFullYear()
		},
	]
}
