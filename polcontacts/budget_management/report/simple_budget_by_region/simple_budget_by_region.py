# coding=utf-8

# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt
# UPDATE `tabTerritory Code` set region1=territory;

from __future__ import unicode_literals
import frappe
from frappe import _

from polcontacts.utils import get_user_territories

def execute(filters=None):
	budget_settings = frappe.get_doc("Budget Management Settings")

	councilors_data = frappe.db.sql("""
			SELECT
				tc.region1,
				tc.councilor_coefficient,
				tc.census_percentage,
				tc.regulatory_coefficient
			from `tabTerritory Code` as tc
	""")
	councilors = {}
	for item in councilors_data:
		councilors[item[0]] = item[1]

	census = {}
	for item in councilors_data:
		census[item[0]] = item[2]

	regulatory_coefficient = {}
	for item in councilors_data:
		regulatory_coefficient[item[0]] = item[3]

	expenses_data = frappe.db.sql("""
			SELECT 
				region, 
				sum(amount) as expenses
			FROM `tabSpent Budget`
			GROUP BY region
	""")

	expenses = {}
	for item in expenses_data:
		expenses[item[0]] = item[1]

	budgets_data = frappe.db.sql("""
			SELECT 
				region.parent_territory, 
				sum(amount) as allocation
			FROM `tabBudget`
			JOIN `tabBudget Allocation Amount`
			ON `tabBudget Allocation Amount`.parent = `tabBudget`.name
			JOIN tabTerritory as city
			ON `tabBudget`.territory = city.name
			JOIN tabTerritory as region
			ON city.parent_territory = region.name
			GROUP BY territory
	""")
	budgets = {}
	for item in budgets_data:
		budgets[item[0]]= item[1]

	contributions_data_position = frappe.db.sql("""
			SELECT
				contact.region1,
				sum(receivable.receivable_total) as total
			from tabReceivable as receivable
			JOIN tabPolContact as contact
				ON receivable.contact = contact.name
			JOIN `tabContact Fee` as fee
				ON receivable.fee = fee.name
			LEFT JOIN `tabTerritory Code` as tc
				ON contact.region1 = tc.region1
			WHERE 
				receivable.year_period = %s AND
				receivable.receivable_type = "Special" AND
				receivable.territory NOT IN ('Exterior', 'Desconegut') AND
				fee.special_type="Position" AND
				(receivable.status_invoice = "Remittance" OR receivable.status_cash = "Paid")
			GROUP BY
				contact.region1
	""", filters["year"])
	contributions_position = {}
	for item in contributions_data_position:
		contributions_position[item[0]]= item[1] * budget_settings.position_coefficient / 100

	contributions_data_territory = frappe.db.sql("""
			SELECT
				contact.region1,
				sum(receivable.receivable_total) as total
			from tabReceivable as receivable
			JOIN tabPolContact as contact
				ON receivable.contact = contact.name
			JOIN `tabContact Fee` as fee
				ON receivable.fee = fee.name
			LEFT JOIN `tabTerritory Code` as tc
				ON contact.region1 = tc.region1
			WHERE 
				receivable.year_period = %s AND
				receivable.receivable_type = "Special" AND
				receivable.territory NOT IN ('Exterior', 'Desconegut') AND
				fee.special_type="Territory" AND
				(receivable.status_invoice = "Remittance" OR receivable.status_cash = "Paid")
			GROUP BY
				contact.region1
	""", filters["year"])
	contributions_territory = {}
	for item in contributions_data_territory:
		contributions_territory[item[0]]= item[1] * budget_settings.territory_coefficient / 100
		
	quotes_data = frappe.db.sql("""
			SELECT
				contact.region1,
				sum(receivable.receivable_total) as total
			from tabReceivable as receivable
			JOIN tabPolContact as contact
				ON receivable.contact = contact.name
			LEFT JOIN `tabTerritory Code` as tc
				ON contact.region1 = tc.region1
			WHERE 
				receivable.year_period = %s AND
				receivable.receivable_type = "Ordinary" AND
				receivable.territory NOT IN ('Exterior', 'Desconegut') AND
				(receivable.status_invoice = "Remittance" OR receivable.status_cash = "Paid")
			GROUP BY
				contact.region1
	""", filters["year"])

	quotes = {}
	for item in quotes_data:
		quotes[item[0]]= item[1] * budget_settings.quote_coefficient / 100

	data = []

	user_territories = get_user_territories(frappe.session.user)

	for region in councilors.keys():

		if region not in user_territories:
			continue

		if item in budgets.keys():
			budget = budgets[item] 
		else:
			budget = 0.0

		if region in expenses:
			expense = expenses[region]
		else:
			expense = 0

		remaining = budget - expense

		if region in contributions_position.keys():
			contribution_position = contributions_position[region] 
		else:
			contribution_position = 0.0

		if region in contributions_territory.keys():
			contribution_territory = contributions_territory[region] 
		else:
			contribution_territory = 0.0

		if region in quotes.keys():
			quote = quotes[region]
		else:
			quote = 0.0

		subtotal = quote + contribution_territory + contribution_position


		regulatory = (subtotal/2) - (subtotal/2*(1-regulatory_coefficient[region]+budget_settings.associates_objective))
		
		if region not in councilors:
			frappe.throw(region+" does not have a Territory Code defined")
		
		councilors_t = (subtotal/2) - (subtotal/2*(1-councilors[region]+budget_settings.position_objective))

		total = subtotal+regulatory+councilors_t

		balance =  total- expense

		data.append([
			frappe.db.get_value("Territory", region, "parent_territory")
			,region, 
			budget, 
			str(round(census[region], 2)), expense, 
			remaining,
			budget_settings.quote_coefficient, quote, 
			budget_settings.position_coefficient, contribution_position, 
			budget_settings.territory_coefficient, contribution_territory, 
			subtotal, 
			regulatory_coefficient[region], regulatory, 
			councilors[region], councilors_t, 
			total, balance])


	#Fix Federació Barcelona Ciutat
	for item in data:
		if item[0]=="Nacional":
			item[0]=u"Federació Barcelona Ciutat"

	columns = [
        _("Region2") + ":Data:200",
        _("Region1") + ":Data:200",
        _("Budget") + ":Currency:90",
        _("Expenses") + " %:Percentage:90",
        _("Expenses") + ":Currency:90",
		_("Remaining") + ":Currency:90",
		_("Quotes") + " %:Percentage:90",
        _("Quotes") + ":Currency:90",
        _("Contributions Position") + " %:Percentage:90",
        _("Contributions Position") + ":Currency:90",
        _("Contributions Territory") + " %:Percentage:90",
        _("Contributions Territory") + ":Currency:90",
        _("Subtotal") + ":Currency:90",
        _("Regulatory") + " %:Percentage:90",
        _("Regulatory") + ":Currency:90",
        _("Councilors") + " %:Percentage:90",
        _("Councilors") + ":Currency:90",
        _("Contributions Total") + ":Currency:90",
        _("Balance") + ":Currency:90"
    ]
	
	return columns, data
