# coding=utf-8

# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from polcontacts.budget_management.report.simple_budget_by_region.simple_budget_by_region import execute as simple_execute
from polcontacts.utils import get_user_territories
import datetime
import time

def execute(filters=None):
	budget_settings = frappe.get_doc("Budget Management Settings")
	budget_import = frappe.get_doc("Spent Budget Import")

	simple_data = simple_execute(filters)

	expenses_data = frappe.db.sql("""
			SELECT 
				region, 
				budget_allocation,
				sum(amount)
			FROM `tabSpent Budget`
			GROUP BY region, budget_allocation
			ORDER BY region, budget_allocation
	""")

	expenses = {}
	for item in expenses_data:
		if item[0] in expenses:
			expenses[item[0]].append((item[1], item[2]))
		else:
			expenses[item[0]] = [(item[1], item[2])]

	budgets_data = frappe.db.sql("""
			SELECT 
				region.parent_territory,
				`tabBudget Allocation Amount`.budget_allocation as budget_allocation,
				sum(amount)
			FROM `tabBudget`
			JOIN `tabBudget Allocation Amount`
			ON `tabBudget Allocation Amount`.parent = `tabBudget`.name
			JOIN tabTerritory as city
			ON `tabBudget`.territory = city.name
			JOIN tabTerritory as region
			ON city.parent_territory = region.name
			group BY region.name, budget_allocation
			ORDER BY territory, budget_allocation
	""")
	budgets = {}
	for item in budgets_data:
		if item[0] in budgets:
			budgets[item[0]].append((item[1], item[2]))
		else:
			budgets[item[0]] = [(item[1], item[2])]

	data_dict = {}
	for region in expenses.keys():
		for expense in expenses[region]:
			if region not in data_dict:
				data_dict[region]={}

			data_dict[region][expense[0]] = {
				"budget": 0,
				"expenses": expense[1]
			}

	for region in budgets.keys():
		for expense in budgets[region]:
			if region not in data_dict:
				data_dict[region]={}

			if expense[0] in data_dict[region]:
				data_dict[region][expense[0]]["budget"] = expense[1]
			else:
				data_dict[region][expense[0]] = {
					"budget": expense[1],
					"expenses": 0
				}
			

	regions = data_dict.keys()
	regions.sort()

	user_territories = get_user_territories(frappe.session.user)

	data=[]
	for region in regions:
		
		if region not in user_territories:
			continue

		budget_allocations = data_dict[region].keys()
		budget_allocations.sort()

		# budget=0
		# expense=0
		# remaining=0

		data.append([
			"<b>Comarca "+ region + "</b>",
			"","",""
		])

		data.append([
			"<b>INGRESSOS</b>",
			"","",""
		])

		for simple in simple_data[1]:
			if simple[1]==region:
				line = [
					"",
					"Participació en els ingressos de les quotes d'associats de la comarca",
					simple[6],
					simple[7]
					]
				data.append(line)
				line = [
					"",
					"Participació en els ingressos de les aportacions de càrrecs electes municipals de la comarca",
					simple[8],
					simple[9]
					]
				data.append(line)
				line = [
					"",
					"Participació en els ingressos de les aportacions de càrrecs públics associats a la comarca",
					simple[10],
					simple[11]
					]
				data.append(line)

				total_parcial=simple[7]+simple[9]+simple[11]

				# data.append([
				# 	"","<i>Total Parcial</i>",
				# 	simple[7]+simple[9]+simple[11]
				# ])

				corrector_associats=simple[14]
				data.append([
					"","<i>Coeficient de valoració del nombre total d'associats a la comarca</i>",
					"{:.2f}".format(simple[13]-budget_settings.associates_objective),
					corrector_associats
				])

				corrector_electes=simple[16]
				data.append([
					"","<i>Coeficient de valoració del nombre total de càrrecs electes municipals obtinguts a la comarca</i>",
					"{:.2f}".format(simple[15]-budget_settings.position_objective),
					corrector_electes
				])

				contribucio_total= total_parcial + corrector_electes + corrector_associats
				data.append([
					"","<b>TOTAL INGRESSOS</b>","",
					contribucio_total
				])

		data.append([
			"<b>DESPESES</b>",
			"","",""
		])
		subtotal_despeses=0
		for ba in budget_allocations:
			line = [
				"",
				ba,
				"",data_dict[region][ba]["expenses"],
				""]
			
			subtotal_despeses= subtotal_despeses + data_dict[region][ba]["expenses"]
			data.append(line)

		data.append([
			"","<b>TOTAL DESPESES</b>","",
			subtotal_despeses
		])

		data.append([
			"","<b>SALDO DISPONIBLE</b> a "+ datetime.datetime.strptime(budget_import.import_date, '%Y-%m-%d %H:%M:%S.%f').strftime("%d-%m-%Y %H:%M"),"",
			contribucio_total-subtotal_despeses
		])


	#Fix Federació Barcelona Ciutat
	for item in data:
		if item[0]=="Nacional":
			item[0]=u"Federació Barcelona Ciutat"

	columns = [
		_("Budget Allocation") + ":Data:150",
        "Concepte" + ":Data:550",
        "Tipus" + ":Data:70",
        "Import" + ":Currency:120",
    ]
	
	return columns, data
