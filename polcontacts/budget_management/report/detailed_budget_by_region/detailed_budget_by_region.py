# coding=utf-8

# Copyright (c) 2013, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from polcontacts.utils import get_user_territories

def execute(filters=None):
	expenses_data = frappe.db.sql("""
			SELECT 
				region, 
				budget_allocation,
				sum(amount)
			FROM `tabSpent Budget`
			GROUP BY region, budget_allocation
			ORDER BY region, budget_allocation
	""")

	expenses = {}
	for item in expenses_data:
		if item[0] in expenses:
			expenses[item[0]].append((item[1], item[2]))
		else:
			expenses[item[0]] = [(item[1], item[2])]

	budgets_data = frappe.db.sql("""
			SELECT 
				region.parent_territory,
				`tabBudget Allocation Amount`.budget_allocation as budget_allocation,
				sum(amount)
			FROM `tabBudget`
			JOIN `tabBudget Allocation Amount`
			ON `tabBudget Allocation Amount`.parent = `tabBudget`.name
			JOIN tabTerritory as city
			ON `tabBudget`.territory = city.name
			JOIN tabTerritory as region
			ON city.parent_territory = region.name
			group BY region.name, budget_allocation
			ORDER BY territory, budget_allocation
	""")
	budgets = {}
	for item in budgets_data:
		if item[0] in budgets:
			budgets[item[0]].append((item[1], item[2]))
		else:
			budgets[item[0]] = [(item[1], item[2])]

	data_dict = {}
	for region in expenses.keys():
		for expense in expenses[region]:
			if region not in data_dict:
				data_dict[region]={}

			data_dict[region][expense[0]] = {
				"budget": 0,
				"expenses": expense[1]
			}


	for region in budgets.keys():


		for expense in budgets[region]:
			if region not in data_dict:
				data_dict[region]={}

			if expense[0] in data_dict[region]:
				data_dict[region][expense[0]]["budget"] = expense[1]
			else:
				data_dict[region][expense[0]] = {
					"budget": expense[1],
					"expenses": 0
				}
			

	regions = data_dict.keys()
	regions.sort()

	data=[]
	user_territories = get_user_territories(frappe.session.user)

	for region in regions:
		if region not in user_territories:
			continue

		budget_allocations = data_dict[region].keys()
		budget_allocations.sort()

		# budget=0
		# expense=0
		# remaining=0

		for ba in budget_allocations:
			line = [
				frappe.db.get_value("Territory", region, "parent_territory"),
				region, 
				ba,
				data_dict[region][ba]["budget"], 
				data_dict[region][ba]["expenses"], 
				data_dict[region][ba]["budget"]-data_dict[region][ba]["expenses"]]
			
			# budget+=line[2]
			# expense+=line[3]
			# remaining+=line[4]

			data.append(line)

		# data.append([
		# 	region,
		# 	"Subtotal",
		# 	budget,
		# 	expense,
		# 	remaining
		# ])

	#Fix Federació Barcelona Ciutat
	for item in data:
		if item[0]=="Nacional":
			item[0]=u"Federació Barcelona Ciutat"

	columns = [
        _("Region2") + ":Data:200",
        _("Region1") + ":Data:200",
		_("Budget Allocation") + ":Data:200",
        _("Budget") + ":Currency:120",
        _("Expenses") + ":Currency:120",
		_("Remaining") + ":Currency:120"
    ]
	
	return columns, data