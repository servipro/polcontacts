# -*- coding: utf-8 -*-
# Copyright (c) 2018, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from polcontacts.utils import get_all_parents, get_parent_regions

class TerritoryCode(Document):
	def before_save(self):
		if self.councilors is None:
			self.councilors = 0
		if self.possible_councilor is None:
			self.possible_councilor = 0
			
		if self.councilors==0 or self.possible_councilor==0:
			self.councilor_percentage = 0
		else:
			self.councilor_percentage = float(self.councilors) / float(self.possible_councilor) * 100

		regions = get_parent_regions(self.region1)
		if self.region1 == regions[0]:
			self.region2 = regions[1]
		else:
			self.region2 = regions[0]

	def on_update(self):
		if self.region2_code and not self.flags.skip_update_census:
			update_census_percentage(self.region2_code)

def update_census_percentage(region2_code):
	census_total = frappe.db.sql("""
			select 
				sum(census) 
			from `tabTerritory Code` 
			where region2_code LIKE %s 
			group by region2_code;
	""",[region2_code])
	if len(census_total)!=1:
		return
	else:
		tcs = frappe.get_list("Territory Code", filters={"region2_code": region2_code})
		for tc_name in tcs:
			tc = frappe.get_doc("Territory Code", tc_name)
			tc.flags.skip_update_census = True
			tc.census_percentage = tc.census / census_total[0][0] * 100
			tc.save()