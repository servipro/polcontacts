frappe.listview_settings['Territory Code'] = {
    column_colspan: {"region2": 5, "census": 2, "external_code": 2},
    colwidths: {"subject": 1},
    add_fields: ["region2", "external_code", "census"],
    add_columns: ["region2","external_code", "census"]
}