// Copyright (c) 2018, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Spent Budget Import', {
	onload: function(frm){
		frm.fields_dict["csv_file"].df.is_private = 1;
	},
	refresh: function(frm) {
		if(frm.doc.status == "Importing"){
            frm.page.set_primary_action(__('Importing'), function () {
				show_alert(__('Please wait'))
			})
		}
		else{
			frm.page.set_primary_action(__('Import'), function () {
				frappe.call({
					method: "import_file",
					doc:frm.doc,
					args:{
						body: frm.doc.message
					},
					callback: function(r) {
						show_message(__('Import is in progess'))
						frm.reload_doc()
					}
				})
			})
		}
	}
});
