# -*- coding: utf-8 -*-
# Copyright (c) 2018, Pau Rosello and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _
import csv
import re

DELETE_FILE_AFTER_IMPORT = False
STEP_SAVES = 10
REGION_SPENT_CODE = "999999"

class SpentBudgetImport(Document):
	city_code_cache = {}
	account_allocation_cache = {}
	census_cache = {}

	def import_file(self):
		try:
			self.errors=""
			self.init_caches()

			with open(frappe.get_site_path() + self.csv_file, 'rb') as csvfile:
				reader = list(csv.reader(csvfile, delimiter=str(";")))[1:]

				self.total_rows = sum(1 for row in reader)
				self.pending_rows = self.total_rows
				self.status = "Importing"
				self.save()
				frappe.db.commit()

				frappe.db.sql("DELETE from `tabSpent Budget`")
				for idx, row in enumerate(reader):
					if idx%STEP_SAVES==0:
						self.pending_rows = self.total_rows-idx
						self.save()
						frappe.db.commit()
					row = row[:-1]
					self.import_line(idx, *row)
		finally:
			if self.errors!="":
				self.pending_rows=0
				self.status = "Error"
			else:
				self.errors=""
				self.pending_rows=0
				self.status = "Success"
				if DELETE_FILE_AFTER_IMPORT:
					self.csv_file = ""
			
			self.import_date = frappe.utils.now()
			self.save()
			frappe.db.commit()

	def divide_spenditure(self, idx, region_code, city, account, amount):
		cities = frappe.get_list("Territory Code", filters={"region2_code": region_code}, fields=["region1", "external_code", "census", "census_percentage"])
		if len(cities) == 0:
			self.errors += ("Line " + str(idx+2) + ":   Region with code "+ region_code +" not found for city "+ city +"<br>")
			return

		for city in cities:
			amount_float=float(amount)
			divided_amount=float(amount_float*city["census_percentage"]/100)
			self.import_line(idx, city["external_code"], city["region1"], REGION_SPENT_CODE, divided_amount)

	def import_line(self, idx, region_code, city, account, amount):
		if type(amount) is str:
			amount = float(amount.replace(".","").replace(",",".").replace("-",""))

		if region_code.startswith("0000") or region_code=="":
			return
		if "*" in region_code:
			self.divide_spenditure(idx, region_code, city, account, amount)
			return

		if region_code not in self.city_code_cache.keys():
			self.errors += ("Line " + str(idx+2) + ":   Region with code "+ region_code +" not found for city "+ re.sub(r'[^\x00-\x7f]',r'', city) +"<br>")
			return
		region = self.city_code_cache[region_code]

		if account not in self.account_allocation_cache.keys():
			self.errors += ("Line "+ str(idx+2) +":   Allocation for account "+account+" not found<br>")
			return
		budget_allocation = self.account_allocation_cache[account]

		line = frappe.new_doc("Spent Budget")
		line.region = region
		line.account = account
		line.budget_allocation = budget_allocation
		line.amount = amount
		line.save()
		
	def init_caches(self):
		self.init_city_code_cache()
		self.init_account_allocation_cache()
		self.init_census_cache()

	def init_account_allocation_cache(self):
		account_data = frappe.db.sql("""
			SELECT 
				line.account,
				allocation_name
			from `tabBudget Allocation` as allocation
			JOIN `tabMicrogestio Account Line` as line
			ON line.parent = allocation.name
		""")
		self.account_allocation_cache = {}

		for item in account_data:
			self.account_allocation_cache[item[0]]= item[1]

	def init_city_code_cache(self):
		codes_data = frappe.db.sql("""
			SELECT 
				external_code,
				region1
			FROM `tabTerritory Code`
		""")
		self.city_code_cache = {}
		for item in codes_data:
			self.city_code_cache[item[0]]= item[1]

	def init_census_cache(self):
		census_data = frappe.db.sql("""
			select 
				region2_code, 
				sum(census) 
			from `tabTerritory Code` 
			where region2_code LIKE '%*' 
			group by region2_code;
		""")
		self.census_cache = {}
		for item in census_data:
			self.census_cache[item[0]]= item[1]
