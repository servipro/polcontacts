# coding=utf-8
from datetime import datetime

from pyexcel_xls import get_data
import validators
import json
import frappe
import validation
import itertools

def translate_interest(name):

    if name == u"Econòmic":
        return "Economic"
    if name == u"Social":
        return "Social"
    if name == u"Relacions Internacionals":
        return "International Relations"
    if name == u"Territori i medi ambient":
        return "Geographic and Environment"
    if name == u"Relacions Institucionals":
        return "Institutional Relations"
    if name == u"Cultura i llengua":
        return "Culture and Language"
    if name == u"Esports":
        return "Sports"
    if name == u"Primer Sector":
        return "Agriculture Stockbreeding and Fishing"
    if name == u"Seguretat":
        return "Security"
    if name == u"Salut":
        return "Health"
    if name == u"Educació":
        return "Education"
    if name == u"Polítiques Digitals":
        return "Digital Politics"
    if name == u"Universitats, recerca i transferència del coneixement":
        return "Universities, research and knowledge transfer"
    if name == u"Gent Gran":
        return "Older People"
    if name == u"Feminisme i Igualtat":
        return "Feminism and Equality"
    if name == u"Dret i Justícia":
        return "Law and Justice"
    else:
        print name
        return "Error"


def val_entity(x):
    return x["entity"]


def aportacions(path):
    data = get_data(path)

    col_dni = 0
    col_fee = 1
    col_extra = 2
    col_origen = 3
    col_desti = 4

    for id, entry in enumerate(data["quotes_carrec"]):
        if not frappe.db.exists("PolContact", entry[col_dni]):
            print entry[col_dni] + " no Existeix"
            continue

        contact = frappe.get_doc("PolContact", entry[col_dni])

        contact.special_fee = entry[col_fee]
        if entry[col_extra]!="":
            contact.extra_special = float(entry[col_extra])
        else:
            contact.extra_special = 0.0
        contact.quote_origin = entry[col_origen]
        contact.quote_destination = entry[col_desti]

        contact.save()

    frappe.db.commit()

def update_country():
    from google_api_integration.places import autocomplete_adress

    dnis = frappe.get_all("PolContact", filters={}, fields=["dni"])

    for dni in dnis:
        print dni
        try:
            contact = frappe.get_doc("PolContact", dni)
            if contact.city!="" and contact.postal_code!="":
                auto_adress = autocomplete_adress(contact.city + " " + contact.postal_code)
                if auto_adress==[]:
                    print "Nothing found"
                    continue
                for item in auto_adress[0]["address_components"]:
                    if "administrative_area_level_2" in item["types"]:
                        contact.region = item["long_name"]
                    if "country" in item["types"]:
                        if item["long_name"] != "Espanya":
                            print item["long_name"]
                        contact.country = item["long_name"]
                contact.save()
        except:
            print "Error"


    frappe.db.commit()

def main3(path="", path_log=""):

    # data = get_data(path)
    # f = open(path_log, 'w')

    # data = get_data("/Users/pau/frappe-bench/apps/polcontacts/polcontacts/associats_last.xlsx")
    # f = open('/Users/pau/frappe-bench/apps/polcontacts/polcontacts/associats_last.txt', 'w')

    data = get_data("/home/frappe/frappe-bench/apps/polcontacts/polcontacts/associats_last.xlsx")
    f = open('/home/frappe/frappe-bench/apps/polcontacts/polcontacts/associats_last.txt', 'w')

    banks = frappe.get_list("Bank", fields=["name","entity"])
    entitats = map(val_entity, banks)

    for id, entry in enumerate(data["Associats"]):

        if id==0:
            col_dni = entry.index(u"dni")
            col_tipus_quota = entry.index(u"Quota")
            col_periodicitat = entry.index(u"Periodicitat")
            col_extra = entry.index(u"Extra Quota")
            col_IBAN = entry.index(u"IBAN")
            col_EntitatB = entry.index(u"Entitat")
            col_OficinaB = entry.index(u"Oficina")
            col_DCB = entry.index(u"DC")
            col_CompteB = entry.index(u"Compte")

        else:
            #Full dict
            empty_finish = [""] * (89-len(entry))
            entry = entry + empty_finish

            if frappe.db.exists("PolContact", entry[col_dni]):
                contact = frappe.get_doc("PolContact", entry[col_dni])

                # Quote
                map_quote = ""
                if entry[col_tipus_quota] == u"General":
                    map_quote = "General"
                elif entry[col_tipus_quota] == u"Reduïda":
                    map_quote = "Reduïda"
                elif entry[col_tipus_quota] == u"Social":
                    map_quote = "Social"
                else:
                    map_quote = "Indefinit"

                # Periodo
                map_periodo = ""
                if entry[col_periodicitat] == u"Anual":
                    map_periodo = "Annual"
                elif entry[col_periodicitat] == u"Trimestral":
                    map_periodo = "Quarterly"
                elif entry[col_periodicitat] == u"Semestral":
                    map_periodo = "Biannual"

                string_extra = entry[col_extra].replace(u"€", u"").replace(u",", u".")
                extra = 0
                if string_extra!= "":
                    extra=float(string_extra)

                iban = entry[col_IBAN] + entry[col_EntitatB] + entry[col_OficinaB] + entry[col_DCB] + entry[col_CompteB]
                if iban != "":
                    if len(iban) != 24 or not validators.iban(iban):
                        print entry[col_dni] + " IBAN incorrecte"
                        print >> f, entry[col_dni] + " IBAN incorrecte"
                        payment_form_map = "Undefined"

                    elif not validation.valid_account_number(iban):
                        print entry[col_dni] + " Compte Bancari incorrecte"
                        print >> f, entry[col_dni] + " Compte Bancari incorrecte"
                        payment_form_map = "Undefined"

                    ##check bank existeix
                    elif not entry[col_EntitatB] in entitats:
                        print entry[col_dni] + " No existeix el bank"
                        print >> f, entry[col_dni] + " No existeix el bank"
                        payment_form_map = "Undefined"

                    else:
                        payment_form_map = "Bank Transfer"
                        bank = [x for x in banks if x["entity"] == iban[4:8]][0]["name"]

                contact.ordinary_fee = map_quote
                contact.ordinary_period = map_periodo
                contact.estra_ordinary = extra
                contact.payment_form = payment_form_map
                contact.iban = iban
                contact.bank = bank

                contact.save()
                # print map_quote
                # print map_periodo
                # print extra

    #frappe.db.commit()
    f.close()

def main2(path="", path_log=""):

    # data = get_data(path)
    # f = open(path_log, 'w')

    data = get_data("/home/frappe/frappe-bench/apps/polcontacts/polcontacts/associats_last.xlsx")
    f = open('/home/frappe/frappe-bench/apps/polcontacts/polcontacts/associats_last.txt', 'w')

    # frappe.db.sql("Delete from  `tabContact Interests`")
    # frappe.db.sql("Delete from  `tabPolContact`")

    banks = frappe.get_list("Bank", fields=["name","entity"])
    entitats = map(val_entity, banks)

    for id, entry in enumerate(data["Associats"]):

        if id==0:
            col_nom = entry.index(u"nom")
            col_primercognom = entry.index(u"cognom 1")
            col_segoncognom = entry.index(u"cognom 2")
            col_datanaixement = entry.index(u"data naixement")
            col_dni = entry.index(u"dni")
            col_email = entry.index(u"correu")
            col_mobil = entry.index(u"mobil")
            col_fix = entry.index(u"fixe")
            col_adreca = entry.index(u"adreca")
            col_cp = entry.index(u"cp")
            col_poblacio = entry.index(u"territori")
            col_sexe = entry.index(u"sexe")
            #col_dataalta = entry.index(u"Data Alta")
            col_IBAN = entry.index(u"IBAN")
            col_EntitatB = entry.index(u"Entitat")
            col_OficinaB = entry.index(u"Oficina")
            col_DCB = entry.index(u"DC")
            col_CompteB = entry.index(u"Compte")

            col_tipus_quota = entry.index(u"Quota")
            col_periodicitat = entry.index(u"Periodicitat")
            col_extra = entry.index(u"Extra Quota")

            col_adscripcio = entry.index(u"territori")


        else:
            #Full dict
            empty_finish = [""] * (89-len(entry))
            entry = entry + empty_finish

            if frappe.db.exists("PolContact", entry[col_dni]):
                print entry[col_dni] + " Ja Existeix"
                print >> f, entry[col_dni] + " Ja Existeix"

                continue

            # Check DNI
            if not validation.validDNI(entry[col_dni]):
                print entry[col_dni] + " DNI incorrecte"
                print >> f, entry[col_dni] + " DNI incorrecte"

                continue

            #Check Sexe
            if(entry[col_sexe]!="Home" and entry[col_sexe]!="Dona"):
                print entry[col_dni] + " sexe no definit/incorrecte"
                print >> f,entry[col_dni] + " sexe no definit/incorrecte"

            #Check IBAN
            iban = entry[col_IBAN]+entry[col_EntitatB]+entry[col_OficinaB]+entry[col_DCB]+entry[col_CompteB]
            if iban!="":
                if len(iban)!=24 or not validators.iban(iban):
                    print entry[col_dni] + " IBAN incorrecte"
                    print >> f, entry[col_dni] + " IBAN incorrecte"
                    payment_form_map = "Undefined"

                elif not validation.valid_account_number(iban):
                    print entry[col_dni] + " Compte Bancari incorrecte"
                    print >> f, entry[col_dni] + " Compte Bancari incorrecte"
                    payment_form_map = "Undefined"

                ##check bank existeix
                elif not entry[col_EntitatB] in entitats:
                    print entry[col_dni] + " No existeix el bank"
                    print >> f,entry[col_dni] + " No existeix el bank"
                    payment_form_map = "Undefined"

                else:
                    payment_form_map = "Bank Transfer"
                    bank = [x for x in banks if x["entity"] == iban[4:8]][0]["name"]
            else:
                print entry[col_dni] + " IBAN no definit"
                print >> f,entry[col_dni] + " IBAN no definit"
                bank=""
                payment_form_map = "Undefined"

            #Sex translation
            sex_map = entry[col_sexe]
            if sex_map == "Home":
                sex_map = "Male"
            elif sex_map == "Dona":
                sex_map = "Female"
            else:
                sex_map = "No Information"

            #Quote
            map_quote = ""
            if entry[col_tipus_quota]==1:
                map_quote= "General"
            elif entry[col_tipus_quota]==2:
                map_quote= "Reduïda"
            elif entry[col_tipus_quota]==3:
                map_quote= "Social"
            else:
                map_quote= "Indefinit"

            #Periodo
            map_periodo = ""
            if entry[col_periodicitat] == u"Anual":
                map_periodo = "Annual"
            elif entry[col_periodicitat] == u"Trimestral":
                map_periodo = "Quarterly"
            elif entry[col_periodicitat] == u"Semestral":
                map_periodo = "Biannual"


            if entry[col_adscripcio].replace("-BCN", "").title()==u"Esquerra Eixample - Sant Antoni":
                entry[col_adscripcio] = u"Esquerra Eixample I Sant Antoni"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Barcelona":
                entry[col_adscripcio] = u"Barcelona Ciutat"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Dreta Eix., Sagrada Família I Fort Pienc":
                entry[col_adscripcio] = u"Dreta Eixample, Sagrada Família"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Salas De Pallars":
                entry[col_adscripcio] = u"Salàs Del Pallars"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Es Bordes":
                entry[col_adscripcio] = u"Les Bordes"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Les Valls De Valira":
                entry[col_adscripcio] = u"Valls De Valira"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Catalunya Nord":
                entry[col_adscripcio] = u"Desconegut"
            elif entry[col_adscripcio].replace("-BCN", "").title() == u"La Guingueta D'Àneu":
                entry[col_adscripcio] = u"Guingueta D'Àneu"
            elif entry[col_adscripcio].replace("-BCN", "").title() == u"Montferrer-Castellbó":
                entry[col_adscripcio] = u"Montferrer I Castellbó"
            elif entry[col_adscripcio].title() == u"Singapur":
                entry[col_adscripcio] = u"Desconegut"
            elif entry[col_adscripcio].title() == u"Madrid":
                entry[col_adscripcio] = u"Desconegut"

            doc = frappe.new_doc("PolContact")

            if entry[col_datanaixement]!='':
                birthdate = datetime.strptime(entry[col_datanaixement], '%d-%m-%Y')

            doc.update({"membership_date": datetime.now(),
                        "contact_status": "Current",
                        "category": "Activist",
                        "dni": entry[col_dni],
                        "contact_name": entry[col_nom].title(),
                        "surname1": entry[col_primercognom].title(),
                        "surname2": entry[col_segoncognom].title(),
                        "address": entry[col_adreca],
                        "postal_code": entry[col_cp],
                        "city": entry[col_poblacio].replace("-BCN", "").title(),
                        "address_blocked": False,
                        "territory": entry[col_adscripcio].replace("-BCN", "").title(),
                        "phone": entry[col_fix],
                        "phone_blocked": False,
                        "mobile_phone": entry[col_mobil],
                        "mobile_phone_blocked": False,
                        "email": entry[col_email],
                        "sex": sex_map,
                        "birthdate": birthdate,
                        "payment_form": payment_form_map,
                        "iban": iban,
                        "bank": bank,
                        "quota_import": entry[col_tipus_quota],
                        "avoid_request": False,
                        "ordinary_fee": map_quote,
                        "ordinary_period": map_periodo
                    })

            doc.insert()

    frappe.db.commit()
    f.close()

def main(path="", path_log=""):

    # data = get_data(path)
    # f = open(path_log, 'w')

    data = get_data("/home/frappe/frappe-bench/apps/polcontacts/polcontacts/associats_final.xlsx")
    f = open('/home/frappe/frappe-bench/apps/polcontacts/polcontacts/associats_log.txt', 'w')


    # frappe.db.sql("Delete from  `tabContact Interests`")
    # frappe.db.sql("Delete from  `tabPolContact`")

    banks = frappe.get_list("Bank", fields=["name","entity"])
    entitats = map(val_entity, banks)

    for id, entry in enumerate(data["Associats"]):

        if id==0:
            col_nom = entry.index(u"Nom")
            col_primercognom = entry.index(u"Primer Cognom")
            col_segoncognom = entry.index(u"Segon Cognom")
            col_datanaixement = entry.index(u"Data Naixement")
            col_dni = entry.index(u"NIF")
            col_email = entry.index(u"Email")
            col_mobil = entry.index(u"Telèfon mòbil")
            col_fix = entry.index(u"Telèfon Fix")
            col_adreca = entry.index(u"Adreça")
            col_cp = entry.index(u"CP")
            col_poblacio = entry.index(u"Població")
            col_procedencia = entry.index(u"Procedència")
            col_butlleta = entry.index(u"Butlleta")
            col_adscripcio = entry.index(u"Adscripció Local")
            col_nomail = entry.index(u"No enviar email")
            col_sexe = entry.index(u"Sexe")
            col_dataalta = entry.index(u"Data Alta")
            col_IBAN = entry.index(u"IBAN")
            col_EntitatB = entry.index(u"EntitatB")
            col_OficinaB = entry.index(u"OficinaB")
            col_DCB = entry.index(u"DCB")
            col_CompteB = entry.index(u"CompteB")
            col_tip_quota_cdc = entry.index(u"Tipus Quota CDC")
            col_periodicitat_cdc = entry.index(u"Periodicitat CDC")
            col_quota_cdc = entry.index(u"Quota CDC")
            col_titular_compte = entry.index(u"Titular Compte")
            col_instagram = entry.index(u"Instagram")
            col_twitter = entry.index(u"Twitter")
            col_tipus_quota = entry.index(u"Tipus Quota")
            col_periodicitat = entry.index(u"Periodicitat")
            col_donatiu = entry.index(u"Donatiu")
            col_motiu_qr = entry.index(u"Motiu QR")
            col_nom_familiar = entry.index(u"nom familiar")
            col_forma_pagament = entry.index(u"Forma Pagament")
            col_economia = entry.index(u"Economia")
            col_energia = entry.index(u"Energia")
            col_comerc = entry.index(u"Comerç")
            col_turisme = entry.index(u"Turisme")
            col_industria = entry.index(u"Indústria")
            col_tic = entry.index(u"TIC")
            col_innovacio = entry.index(u"Innovació")
            col_pot_social = entry.index(u"Protecció Social")
            col_salut = entry.index(u"Salut")
            col_immigracio = entry.index(u"Immigració")
            col_ocupacio = entry.index(u"Ocupació")
            col_pensions = entry.index(u"Pensions")
            col_politica_europea = entry.index(u"Política Europea")
            col_coop_internacional = entry.index(u"Cooperació Internacional")
            col_adm_pub = entry.index(u"Adm Públiques")
            col_juridic = entry.index(u"Jurídic")
            col_igualtat = entry.index(u"Igualtat")
            col_universitat = entry.index(u"Universitat")
            col_coneixement = entry.index(u"Coneixement")
            col_educacio = entry.index(u"Educació")
            col_agricultura = entry.index(u"Agricultura")
            col_alimentacio = entry.index(u"Alimentació")
            col_des_rural = entry.index(u"Desenvolupament rural")
            col_ramaderia = entry.index(u"Ramaderia")
            col_pesca = entry.index(u"Pesca")
            col_pot_civil = entry.index(u"Protecció civil")
            col_policia = entry.index(u"Policia")
            col_defensa = entry.index(u"Defensa")
            col_emergencies = entry.index(u"Emergències")
            col_ambit_ads1 = entry.index(u"AmbitsAdscrits1")
            col_ambit_ads2 = entry.index(u"AmbitsAdscrits2")
            col_ambit_inf1 = entry.index(u"AmbitsInf1")
            col_ambit_inf2 = entry.index(u"AmbitsInf2")
            col_ambit_inf3 = entry.index(u"AmbitsInf3")
            col_ambit_inf4 = entry.index(u"AmbitsInf4")
            col_ambit_inf5 = entry.index(u"AmbitsInf5")
            col_ambit_inf6 = entry.index(u"AmbitsInf6")
            col_ambit_inf7 = entry.index(u"AmbitsInf7")
            col_ambit_inf8 = entry.index(u"AmbitsInf8")
            col_ambit_inf9 = entry.index(u"AmbitsInf9")

        else:
            #Full dict
            empty_finish = [""] * (89-len(entry))
            entry = entry + empty_finish

            if frappe.db.exists("PolContact", entry[col_dni]):
                print entry[col_dni] + " Ja Existeix"
                print >> f, entry[col_dni] + " Ja Existeix"

                continue

            # Check DNI
            if not validation.validDNI(entry[col_dni]):
                print entry[col_dni] + " DNI incorrecte"
                print >> f, entry[col_dni] + " DNI incorrecte"

                continue

            #Check Sexe
            if(entry[col_sexe]!="H" and entry[col_sexe]!="D"):
                print entry[col_dni] + " sexe no definit/incorrecte"
                print >> f,entry[col_dni] + " sexe no definit/incorrecte"

            #Check IBAN
            iban = entry[col_IBAN]+entry[col_EntitatB]+entry[col_OficinaB]+entry[col_DCB]+entry[col_CompteB]
            if iban!="":
                if len(iban)!=24 or not validators.iban(iban):
                    print entry[col_dni] + " IBAN incorrecte"
                    print >> f, entry[col_dni] + " IBAN incorrecte"
                    payment_form_map = "Undefined"

                elif not validation.valid_account_number(iban):
                    print entry[col_dni] + " Compte Bancari incorrecte"
                    print >> f, entry[col_dni] + " Compte Bancari incorrecte"
                    payment_form_map = "Undefined"

                ##check bank existeix
                elif not entry[col_EntitatB] in entitats:
                    print entry[col_dni] + " No existeix el bank"
                    print >> f,entry[col_dni] + " No existeix el bank"
                    payment_form_map = "Undefined"

                else:
                    payment_form_map = "Bank Transfer"
                    bank = [x for x in banks if x["entity"] == iban[4:8]][0]["name"]
            else:
                print entry[col_dni] + " IBAN no definit"
                print >> f,entry[col_dni] + " IBAN no definit"
                bank=""
                payment_form_map = "Undefined"

            #Sex translation
            sex_map = entry[col_sexe]
            if sex_map == "H":
                sex_map = "Male"
            elif sex_map == "D":
                sex_map = "Female"
            else:
                sex_map = "No Information"

            #Quote
            map_quote = ""
            if entry[col_tipus_quota]==1:
                map_quote= "General"
            elif entry[col_tipus_quota]==2:
                map_quote= "Reduïda"
            elif entry[col_tipus_quota]==3:
                map_quote= "Social"
            else:
                map_quote= "Indefinit"

            # Reduida
            map_reduida = ""
            if entry[col_motiu_qr] == 1:
                map_reduida = "JNC"
            elif entry[col_motiu_qr] == 2:
                map_reduida = "Familiar"

            #Periodo
            map_periodo = ""
            if entry[col_periodicitat] == 1:
                map_periodo = "Annual"
            elif entry[col_periodicitat] == 2:
                map_periodo = "Quarterly"
            elif entry[col_periodicitat] == 3:
                map_periodo = "Biannual"


            if entry[col_adscripcio].replace("-BCN", "").title()==u"Esquerra Eixample - Sant Antoni":
                entry[col_adscripcio] = "Esquerra Eixample I Sant Antoni"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Barcelona":
                entry[col_adscripcio] = "Barcelona Ciutat"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Dreta Eix., Sagrada Família I Fort Pienc":
                entry[col_adscripcio] = "Dreta Eixample, Sagrada Família"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Salas De Pallars":
                entry[col_adscripcio] = "Salàs Del Pallars"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Es Bordes":
                entry[col_adscripcio] = "Les Bordes"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Les Valls De Valira":
                entry[col_adscripcio] = "Valls De Valira"
            elif entry[col_adscripcio].replace("-BCN", "").title()==u"Catalunya Nord":
                entry[col_adscripcio] = "Desconegut"
            elif entry[col_adscripcio].replace("-BCN", "").title() == u"La Guingueta D'Àneu":
                entry[col_adscripcio] = "Guingueta D'Àneu"
            elif entry[col_adscripcio].replace("-BCN", "").title() == u"Montferrer-Castellbó":
                entry[col_adscripcio] = "Montferrer I Castellbó"

            doc = frappe.new_doc("PolContact")

            doc.update({"membership_date": entry[col_dataalta],
                        "contact_status": "Current",
                        "category": "Activist",
                        "dni": entry[col_dni],
                        "contact_name": entry[col_nom].title(),
                        "surname1": entry[col_primercognom].title(),
                        "surname2": entry[col_segoncognom].title(),
                        "address": entry[col_adreca],
                        "postal_code": entry[col_cp],
                        "city": entry[col_poblacio].replace("-BCN", "").title(),
                        "address_blocked": False,
                        "territory": entry[col_adscripcio].replace("-BCN", "").title(),
                        "phone": entry[col_fix],
                        "phone_blocked": False,
                        "mobile_phone": entry[col_mobil],
                        "mobile_phone_blocked": False,
                        "email": entry[col_email],
                        "email_blocked": entry[col_nomail]=="TRUE",
                        "sex": sex_map,
                        "birthdate": entry[col_datanaixement],
                        "payment_form": payment_form_map,
                        "iban": iban,
                        "bank": bank,
                        "procedencia": entry[col_procedencia],
                        "quota_import": entry[col_tipus_quota],
                        "avoid_request": True,
                        "have_request": entry[col_butlleta],
                        "account_owner": entry[col_titular_compte],
                        "different_owner": entry[col_titular_compte]!="",
                        "ordinary_fee": map_quote,
                        "ordinary_period": map_periodo,
                        "ordinary_comment": map_reduida,
                        "twitter": entry[col_twitter],
                        "instagram": entry[col_instagram],
                        "origin": entry[col_procedencia]
                        })

            interests = []

            if entry[col_ambit_ads1]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_ads1]), "level":"Subscription"})
            if entry[col_ambit_ads2]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_ads2]), "level":"Subscription"})
            if entry[col_ambit_inf1]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf1]), "level":"Information"})
            if entry[col_ambit_inf2]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf2]), "level":"Information"})
            if entry[col_ambit_inf3]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf3]), "level":"Information"})
            if entry[col_ambit_inf4]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf4]), "level":"Information"})
            if entry[col_ambit_inf5]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf5]), "level":"Information"})
            if entry[col_ambit_inf6]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf6]), "level":"Information"})
            if entry[col_ambit_inf7]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf7]), "level":"Information"})
            if entry[col_ambit_inf8]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf8]), "level":"Information"})
            if entry[col_ambit_inf9]!="":
                interests.append({"field_of_interest":translate_interest(entry[col_ambit_inf9]), "level":"Information"})

            doc.update({
                "interests":interests
            })

            doc.insert()

    frappe.db.commit()
    f.close()

def add_user_interest():
    new_interest = "Older People"
    new_level = "Information"
    dnis = [
    ]

    for dni in dnis:
        user = frappe.get_doc("PolContact", dni)
        found=False
        for interest in user.interests:
            if interest.field_of_interest==new_interest:
                found = True
        
        if not found:
            user.append("interests", 
            {
                "field_of_interest": new_interest,
                "level": new_level
            })
            user.save()

if __name__ == '__main__':
    main()
