frappe.pages['new_sms'].on_page_load = function(wrapper) {

	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('New SMS'),
		single_column: true
	});

	$(frappe.render_template('new_sms')).appendTo(page.body);
	page.content = $(page.body).find('.filter-area');

	frappe.model.with_doctype("PolContact", function() {

		page.list = new frappe.ui.BaseList({
			hide_refresh: true,
			page: page,
			show_filters: true,
			doctype: "PolContact",
			parent: $("<div></div>").appendTo(page.content),
			run: function(more) {
					for (var i=0; i<page.list.filter_list.filters.length; ++i) {
						var filter = page.list.filter_list.filters[i];
						filter.freeze()
					}
					frappe.pages['new_sms'].refresh_count()
			}
		});


		page.set_primary_action(__("Enviar"), function() {
            frappe.confirm(
                __('Do you want to send the SMS?'),
                function(){
                    var message = frappe.pages['new_sms'].message.get_value()

                    if(message.length==0){
                           frappe.throw(__("You have to define a message"))
                    }
                    else{
                        frappe.call({
                                    "method": "polcontacts.communications.send_sms",
                                    freeze: true,
                                    freeze_message: __("Enviant"),
                                    args: {
                                        "message": message,
                                        "filters": frappe.pages['new_sms'].page.list.filter_list.get_filters()
                                    },
                                    callback: function (data) {
                                        //frappe.pages['new_email'].configuration.set_value("")

                                        console.log(data)
                                        msgprint(__("SMS Queued"), __("SMS Queued"))
                                    }
                        });
                    }
                },
                function(){
                    window.close();
                }
            )

		});

        var init_vars = function(){
            var attributes = ["full_name", "contact_name", "surname1", "surname2"]
            var meta = frappe.get_meta('PolContact');
            var lis = "";

            for(var i=0; i<attributes.length; i++){
                meta_field = $.grep(meta.fields, function(e){ return e.fieldname == attributes[i]; })[0];
                lis+= "<li style='cursor: pointer' class='attribute_add' data-field_name='"+attributes[i]+"'>"+ __(meta_field.label) +"</li>"
            }
            $('#attributes_sms').html("<ul>" + lis + "</ul>")
        };
        init_vars();

        $('.attribute_add').click(function(){
            var value = $(this).data("field_name")
            message.set_value(message.get_value()+"{{" + value + "}} ")
        });

		$('#view_contacts').click(function() {

		    var filters = {}
            for(var i=0; i<page.list.filter_list.get_filters().length;++i){
                item = page.list.filter_list.get_filters()[i]
                filters[item[1]] = [item[2], item[3]]
            }
            frappe.route_options = filters;
		    frappe.set_route("List", "PolContact")
		});

        page.list.filter_list.add_filter("PolContact", "contact_status", "=", "Current")
        page.list.filter_list.add_filter("PolContact", "mobile_phone_blocked", "=", "0")
        page.list.filter_list.add_filter("PolContact", "mobile_phone", "not like", "")
        page.list.filter_list.add_filter("PolContact", "category", "!=", "External")

        page.list.run();



        frappe.pages['new_sms'].page = page;
	});




    var message = frappe.ui.form.make_control({
		parent: $(".message"),
		df: {
			fieldtype: "Text",
			fieldname: "message",
			placeholder: __("Message")
		},
		only_input: true
	});

	message.refresh();

	frappe.pages['new_sms'].message = message;
};


frappe.pages['new_sms'].on_page_show = function(wrapper) {
    debugger;

    var page = frappe.pages['new_sms'].page;
    if(typeof page.list!=='undefined' && typeof page.list.filter_list!=='undefined'){
        frappe.pages['new_sms'].refresh_count(wrapper)
    }
}

frappe.pages['new_sms'].refresh_count = function(wrapper) {
    var page = frappe.pages['new_sms'].page;
	frappe.call({
            "method": "polcontacts.utils.count",
            args: {
                 "doctype": "PolContact",
                 "filters": page.list.filter_list.get_filters()
            },
            callback: function (data) {
		    	if($.isEmptyObject(data)) {
		    		data.message = "0";
		    	}
		    	$(page.wrapper).find("#n_contacts").html(String(data.message));
            }
    });
}