frappe.pages['new_email'].on_page_load = function(wrapper) {

	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('New Email'),
		single_column: true
	});

	$(frappe.render_template('new_email')).appendTo(page.body);
	page.content = $(page.body).find('.filter-area');

	frappe.model.with_doctype("PolContact", function() {

		page.list = new frappe.ui.BaseList({
			hide_refresh: true,
			page: page,
			show_filters: true,
			doctype: "PolContact",
			parent: $("<div></div>").appendTo(page.content),
			run: function(more) {
					for (var i=0; i<page.list.filter_list.filters.length; ++i) {
						var filter = page.list.filter_list.filters[i];
						filter.freeze()
					}
					frappe.pages['new_email'].refresh_count()
			}
		});


		page.set_primary_action(__("Enviar"), function() {
            frappe.confirm(
                __('Do you want to send the email?'),
                function(){
                    var content = frappe.pages['new_email'].summernote.summernote('code');
                    var configuration = frappe.pages['new_email'].configuration.get_value();
                    var template = frappe.pages['new_email'].template.get_value();
                    var subject = frappe.pages['new_email'].subject.get_value();
                    var file1 = frappe.pages['new_email'].archiu1.get_value();
                    var file2 = frappe.pages['new_email'].archiu2.get_value();
                    var file3 = frappe.pages['new_email'].archiu3.get_value();
                    var file4 = frappe.pages['new_email'].archiu4.get_value();

                    if(subject.length==0){
                           frappe.throw(__("You have to define a subject"))
                    }
                    else if(configuration.length==0){
                           frappe.throw(__("You have to provide a mail configuration"))
                    }
                    else if(template.length==0){
                           frappe.throw(__("You have to provide a mail template"))
                    }
                    else if(content.length==0 || content=="<p><br></p>"){
                           frappe.throw(__("You have to create some content"))
                    }
                    else{
                        frappe.call({
                                    "method": "polcontacts.communications.send_mass_mail",
                                    freeze: true,
                                    freeze_message: __("Enviant"),
                                    args: {
                                        "content": content,
                                        "filters": frappe.pages['new_email'].page.list.filter_list.get_filters(),
                                        "configuration": configuration,
                                        "template": template,
                                        "subject": subject,
										"file1": file1,
										"file2": file2,
										"file3": file3,
										"file4": file4
                                    },
                                    callback: function (data) {
                                        // frappe.pages['new_email'].subject.set_value("")
                                        // frappe.pages['new_email'].summernote.summernote('code', "")
                                        // frappe.pages['new_email'].configuration.set_value("")

                                        console.log(data)
                                        msgprint("Email enviat", 'Email Enviat')
                                    }
                        });
                    }
                },
                function(){
                    window.close();
                }
            )

		});


        var init_vars = function(){
            var attributes = ["full_name", "contact_name", "surname1", "surname2"]
            var meta = frappe.get_meta('PolContact');
            var lis = "";

            for(var i=0; i<attributes.length; i++){
                meta_field = $.grep(meta.fields, function(e){ return e.fieldname == attributes[i]; })[0];
                lis+= "<li style='cursor: pointer' class='attribute_add' data-field_name='"+attributes[i]+"'>"+ __(meta_field.label) +"</li>"
            }
            $('#attributes').html("<ul>" + lis + "</ul>")
        }
        init_vars();


		$('#view_contacts').click(function() {

            var filters = {}
            for(var i=0; i<page.list.filter_list.get_filters().length;++i){
                item = page.list.filter_list.get_filters()[i];
                if(item[0]!=="PolContact") {
                    filters[item[0]+"."+item[1]] = [item[2], item[3]]
                }
                else{
                    filters[item[1]] = [item[2], item[3]]
                }
            }
            debugger;
            frappe.route_options = filters;
		    frappe.set_route("List", "PolContact")
		});

        page.list.filter_list.add_filter("PolContact", "email_blocked", "=", "0")
        page.list.filter_list.add_filter("PolContact", "email", "not like", "")
        page.list.filter_list.add_filter("PolContact", "contact_status", "=", "Current")
		page.list.filter_list.add_filter("PolContact", "category", "!=", "External")
		
		setTimeout(function() {
                    page.list.filter_list.get_filters();
                    frappe.pages['new_email'].refresh_count();
          }, 200);


        frappe.pages['new_email'].page = page;


	});


	frappe.pages['new_email'].summernote = $('#summernote').summernote({
		lang: 'ca-ES',
		height: 300,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,
		toolbar: [
			// [groupName, [list of button]]
			['history', ['undo', 'redo']],
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['insert', ['link','table','picture','hr']],
			['misc', ['fullscreen', 'codeview']]
		],
		hint: {
            words: ["Nom Complet", "Nom Contacte", "Cognom 1", "Cognom 2", "Adreça", "Codi Postal", "Municipi"],
            match: /\b(\w{1,})$/,
            search: function (keyword, callback) {
              callback($.grep(this.words.map(function(elem) { return elem.toLowerCase(); }), function (item) {
                return item.indexOf(keyword.toLowerCase()) === 0;
              }));
            },
            content: function (item) {
              var values= ["{{full_name}}", "{{contact_name}}", "{{surname1}}", "{{surname2}}", "{{c_address}}", "{{postal_code}}", "{{city}}"]
              return values[this.words.map(function(elem) { return elem.toLowerCase(); }).indexOf(item.toLowerCase())];
            }
        },
        icons: {
				'align': 'fa fa-align',
				'alignCenter': 'fa fa-align-center',
				'alignJustify': 'fa fa-align-justify',
				'alignLeft': 'fa fa-align-left',
				'alignRight': 'fa fa-align-right',
				'indent': 'fa fa-indent',
				'outdent': 'fa fa-outdent',
				'arrowsAlt': 'fa fa-arrows-alt',
				'bold': 'fa fa-bold',
				'caret': 'caret',
				'circle': 'fa fa-circle',
				'close': 'fa fa-close',
				'code': 'fa fa-code',
				'eraser': 'fa fa-eraser',
				'font': 'fa fa-font',
				'frame': 'fa fa-frame',
				'italic': 'fa fa-italic',
				'link': 'fa fa-link',
				'unlink': 'fa fa-chain-broken',
				'magic': 'fa fa-magic',
				'menuCheck': 'fa fa-check',
				'minus': 'fa fa-minus',
				'orderedlist': 'fa fa-list-ol',
				'pencil': 'fa fa-pencil',
				'picture': 'fa fa-image',
				'question': 'fa fa-question',
				'redo': 'fa fa-redo',
				'square': 'fa fa-square',
				'strikethrough': 'fa fa-strikethrough',
				'subscript': 'fa fa-subscript',
				'superscript': 'fa fa-superscript',
				'table': 'fa fa-table',
				'textHeight': 'fa fa-text-height',
				'trash': 'fa fa-trash',
				'underline': 'fa fa-underline',
				'undo': 'fa fa-undo',
				'unorderedlist': 'fa fa-list-ul',
				'video': 'fa fa-video-camera'
        }
	});

    var subject = frappe.ui.form.make_control({
		parent: $(".subject"),
		df: {
			fieldtype: "Data",
			fieldname: "subject",
			placeholder: __("Subject"),
            reqd:1
		},
		only_input: true
	});

	subject.refresh();

	frappe.pages['new_email'].subject = subject;

	var configuration = frappe.ui.form.make_control({
		parent: $(".configuration"),
		df: {
			fieldtype: "Link",
			options: "Email Configuration",
			fieldname: "configuration",
			placeholder: __("Email Configuration"),
            reqd:1
		},
		only_input: true
	});

	configuration.refresh();

	configuration.$input.on("change", function() {
	     console.log(configuration.get_value())
	     frappe.call({
            method:"frappe.client.get",
            args:{
                doctype:"Email Configuration",
                name: configuration.get_value()
            },
            callback: function(r) {
                $("#info_footer").show()
                $("#footer").html(r.message.footer);
                $("#from").html(r.message.from)
                $("#reply_to").html(r.message.reply_to)
                $("#bcc").html(r.message.bcc)
            }
        });


	})

	frappe.pages['new_email'].configuration = configuration;

    var template = frappe.ui.form.make_control({
		parent: $(".template"),
		df: {
			fieldtype: "Link",
			options: "Email Template",
			fieldname: "template",
			placeholder: __("Email Template"),
            reqd:1
		},
		only_input: true
	});
    template.refresh();
    frappe.pages['new_email'].template = template;

    var archiu1 = frappe.ui.form.make_control({
		parent: $(".archiu1"),
		df: {
			fieldtype: "Attach",
			fieldname: "archiu1",
			placeholder: __("Arxiu 1"),
			is_private: 1
		},
		only_input: true
	});
    archiu1.refresh();
    frappe.pages['new_email'].archiu1 = archiu1;

    var archiu2 = frappe.ui.form.make_control({
		parent: $(".archiu2"),
		df: {
			fieldtype: "Attach",
			fieldname: "archiu2",
			placeholder: __("Arxiu 2"),
			is_private: 1
		},
		only_input: true
	});
    archiu2.refresh();
    frappe.pages['new_email'].archiu2 = archiu2;

    var archiu3 = frappe.ui.form.make_control({
		parent: $(".archiu3"),
		df: {
			fieldtype: "Attach",
			fieldname: "archiu3",
			placeholder: __("Arxiu 3"),
			is_private: 1
		},
		only_input: true
	});
    archiu3.refresh();
    frappe.pages['new_email'].archiu3 = archiu3;

    var archiu4 = frappe.ui.form.make_control({
		parent: $(".archiu4"),
		df: {
			fieldtype: "Attach",
			fieldname: "archiu4",
			placeholder: __("Arxiu 4"),
			is_private: 1
		},
		only_input: true
	});
    archiu4.refresh();
    frappe.pages['new_email'].archiu4 = archiu4;
};

frappe.pages['new_email'].refresh_count = function() {
    var page = frappe.pages['new_email'].page;
	frappe.call({
            "method": "polcontacts.utils.count",
            args: {
                 "doctype": "PolContact",
                 "filters": page.list.filter_list.get_filters()
            },
            callback: function (data) {
		    	if($.isEmptyObject(data)) {
		    		data.message = "0";
		    	}
		    	$(page.wrapper).find("#n_contacts").html(String(data.message));
            }
    });
}