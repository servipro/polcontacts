# coding=utf-8
import base64
import json

from datetime import datetime
import os

import frappe
import magic
from polcontacts.communications.doctype.communications_configuration import get_config_level, get_config_level_sms
from requests.auth import HTTPBasicAuth
from sparkpost_integration.utils import send_email_template, getSparkClient
import re
from frappe import _
from frappe.utils.background_jobs import enqueue

from BeautifulSoup import BeautifulSoup
from slugify import slugify
import requests
from frappe.utils import fmt_money

def get_test_recipients():
    return [{
                "address": {
                    "email": "pau@servipro.eu"
                },
                "substitution_data": {
                    "full_name": "Pau Roselló Van Schoor",
                    "contact_name": "Pau",
                    "surname1": "Roselló",
                    "surname2": "Van Schoor",
                    "address": "Saragossa 90",
                    "postal_code": "43870",
                    "city": "Amposta"
                }
            }]

def send_sms_job(number, message, log_item,from_sms, receivable=None):
    sms_configuration = frappe.get_doc("SMS Configuration")

    headers = {'Content-Type': 'application/json', "Accept": "application/json"}
    request = requests.post(sms_configuration.sms_url,
                            json={
                                'to': [number],
                                'text': message,
                                'from': from_sms
                            },
                            headers=headers,
                            auth = HTTPBasicAuth(sms_configuration.sms_user,sms_configuration.sms_password))
    response = request.json()

    log = log_item
    if 'error' in response:
        log.status = "Error"
        log.more_info = str(response["error"]["code"]) + "\n\n" + response["error"]["description"]
    else:
        if receivable!=None:
            receivable.sms_sent+=1
            receivable.save()
            
        log.status = "Sent"
    log.save()

def telegram_log(content, chat_id, user, comm_id):
    log = frappe.new_doc("Communication Log")
    log.update({
        "date": datetime.now(),
        "communication_type": "Telegram",
        "user": user,
        "territory": frappe.db.get_value("User", user, "territory"),
        "sent_status": 1,
        "error_status": 0,
        "pending_status": 0,
        "content": content,
        "filters": "",
        "communication_id": comm_id,
        "chat":chat_id
    })
    log.save()
    frappe.db.commit()

@frappe.whitelist()
def send_sms(message, filters):
    not_current =[]
    phone_blocked =[]
    phone_incorrect =[]

    contacts = frappe.get_list("PolContact", filters=filters, fields=["name", "contact_status", "mobile_phone_blocked", "full_name", "mobile_phone", "contact_name", "surname1", "surname2", "address", "postal_code", "city"], distinct=True)
    recipients = []

    attributes = ["full_name", "contact_name", "surname1", "surname2", "address", "postal_code", "city"]


    log = frappe.new_doc("Communication Log")
    log.update({
        "date": datetime.now(),
        "communication_type": "SMS",
        "user": frappe.session.user,
        "territory": frappe.db.get_value("User", frappe.session.user, "territory"),
        "sent_status": 0,
        "error_status": 0,
        "pending_status": 0,
        "sms_cost": 0,
        "content": message,
        "filters": filters
    })
    log.save()

    from_sms = frappe.get_doc("Communications Configuration").from_sms
    for contact in contacts:
        custom_message = message
        for attribute in attributes:
            if contact[attribute]:
                custom_message = custom_message.replace("{{" + attribute + "}}", contact[attribute])

        if contact.contact_status != "Current":
            not_current.append({
                "name": contact.full_name,
                "email": contact.mobile_phone
            })
            continue

        if contact.mobile_phone_blocked:
            phone_blocked.append({
                "name": contact.full_name,
                "email": contact.mobile_phone
            })
            continue

        pattern = re.compile("[6,7][\d]{8}\Z")
        if not pattern.match(contact.mobile_phone):
            phone_incorrect.append({
                "name": contact.full_name,
                "email": contact.mobile_phone
            })
            continue

        if pattern.match(contact.mobile_phone):
            recipients.append("34"+contact.mobile_phone)
            log_item = frappe.new_doc("Communication Log Item")
            log_item.update({
                "date": datetime.now(),
                "contact": contact.name,
                "status": "Pending",
                "parent": log.name,
                "parenttype": "Communication Log",
                "parentfield": "communications_table"
            })
            log_item.save()
            number = "34" + contact.mobile_phone
            #send_sms_job(number, message, log_item)
            #frappe.db.commit()
            enqueue('polcontacts.communications.send_sms_job', queue='short', number=number, message=custom_message, log_item=log_item, from_sms=from_sms)

    if len(recipients) == 0:
        frappe.throw(_("No contacts can receive SMS"))
        log.delete()

    return {
        "not_current":not_current,
        "phone_blocked":phone_blocked,
        "phone_incorrect": phone_incorrect,
        "log": log.name
    }

@frappe.whitelist()
def send_unpaid_sms(date_repayment, notification_level, receivable_type, only_if_not_email, remittance_name=None):

    enqueue('polcontacts.communications.send_unpaid_job_sms', queue='long',
            date_repayment=date_repayment,
            notification_level=notification_level,
            receivable_type=receivable_type,
            remittance_name=remittance_name,
            only_if_not_email=only_if_not_email
            )

@frappe.whitelist()
def send_unpaid(date_repayment, notification_level, receivable_type, remittance_name=None):

    enqueue('polcontacts.communications.send_unpaid_job', queue='long',
            date_repayment=date_repayment,
            notification_level=notification_level,
            receivable_type=receivable_type,
            remittance_name=remittance_name
            )


@frappe.whitelist()
def send_unpaid_job_sms(date_repayment, notification_level, receivable_type, only_if_not_email, remittance_name=None):
    filters = [{"status_invoice": "Unpaid"}]
    unpaid_configuration = frappe.get_doc("Communications Configuration")
    frappe.db.set_default("number_format", "#.###,##")

    if remittance_name != None:
        filters.append({"remittance": remittance_name})
    if notification_level != "All":
        filters.append({"sms_sent": int(notification_level) - 1})
    if receivable_type != "All":
        filters.append({"receivable_type": receivable_type})
    if only_if_not_email == '1':
        filters.append({"notifications_sent": 0})

    receivables = frappe.get_list("Receivable", filters=filters, fields=["name"])

    if len(receivables) == 0:
        frappe.throw(_("No Unpaid Receivables"))

    log = frappe.new_doc("Communication Log")
    log.update({
        "date": datetime.now(),
        "communication_type": "SMS",
        "user": frappe.session.user,
        "territory": frappe.db.get_value("User", frappe.session.user, "territory"),
        "sent_status": 0,
        "error_status": 0,
        "pending_status": 0,
        "content": "Unpaid SMS",
        "filters": repr(filters)
    })
    log.save()

    if remittance_name != None:
        remittance = frappe.get_doc("Remittance", remittance_name)

    for item in receivables:
        receivable = frappe.get_doc("Receivable", item.name)

        if remittance_name == None:
            remittance = frappe.get_doc("Remittance", receivable.remittance)

        from_sms, content = get_config_level_sms(receivable.sms_sent,unpaid_configuration,receivable.receivable_type)

        contact = frappe.get_doc("PolContact", receivable.contact)
        if contact.mobile_phone == "":
            continue

        log_item = frappe.new_doc("Communication Log Item")
        log_item.update({
            "date": datetime.now(),
            "contact": contact.name,
            "status": "Pending",
            "parent": log.name,
            "parenttype": "Communication Log",
            "parentfield": "communications_table"
        })
        log_item.save()
        frappe.db.commit()

        content = content.replace("{{contact_name}}", contact.contact_name)
        content = content.replace("{{receivable_name}}", receivable.name)
        content = content.replace("{{remittance_date}}", receivable.remittance_date.strftime('%Y/%m/%d'))
        content = content.replace("{{receivable_description}}", remittance.description)
        content = content.replace("{{receivable_total}}", fmt_money(receivable.amount + receivable.extra + receivable.management_fee, precision=2))
        content = content.replace("{{iban}}", receivable.iban[0:8] + "****" + receivable.iban[-4:])
        content = content.replace("{{date}}", date_repayment)
        content = content.replace("{{bank}}", receivable.bank)
        content = content.replace("{{origin}}", receivable.quote_origin or "")

        enqueue('polcontacts.communications.send_sms_job', queue='short',
                number="34"+contact.mobile_phone,
                message=content,
                log_item=log_item,
                from_sms=from_sms,
                receivable=receivable)

def send_unpaid_job(date_repayment, notification_level, receivable_type, remittance_name=None):
    filters = [{"status_invoice": "Unpaid"}]
    unpaid_configuration = frappe.get_doc("Communications Configuration")
    frappe.db.set_default("number_format", "#.###,##")

    if remittance_name!=None:
        filters.append({"remittance": remittance_name})
    if notification_level!="All":
        filters.append({"notifications_sent": int(notification_level)-1})
    if receivable_type!="All":
        filters.append({"receivable_type": receivable_type})



    receivables = frappe.get_list("Receivable", filters=filters, fields=["name"])

    if len(receivables)==0:
        frappe.throw(_("No Unpaid Receivables"))

    log = frappe.new_doc("Communication Log")
    log.update({
        "date": datetime.now(),
        "communication_type": "Email",
        "user": frappe.session.user,
        "territory": frappe.db.get_value("User", frappe.session.user, "territory"),
        "sent_status": 0,
        "error_status": 0,
        "pending_status": 0,
        "content": "Unpaid",
        "filters": repr(filters)
    })
    log.save()

    if remittance_name!=None:
        remittance = frappe.get_doc("Remittance", remittance_name)

    for item in receivables:
        try:
            receivable = frappe.get_doc("Receivable", item.name)

            if remittance_name == None:
                remittance = frappe.get_doc("Remittance", receivable.remittance)

            template,configuration,unpaid_content,subject = get_config_level(receivable.notifications_sent, unpaid_configuration, receivable.receivable_type)

            contact = frappe.get_doc("PolContact", receivable.contact)

            if contact.email is None or contact.email=="":
                continue

            log_item = frappe.new_doc("Communication Log Item")
            log_item.update({
                "date": datetime.now(),
                "contact": contact.name,
                "status": "Pending",
                "parent": log.name,
                "parenttype": "Communication Log",
                "parentfield": "communications_table"
            })
            log_item.save()
            frappe.db.commit()

            receivable.notifications_sent = receivable.notifications_sent + 1
            receivable.save()

            enqueue('polcontacts.communications.send_mail', queue='short',
                    content=unpaid_content,
                    recipients=[{
                        "address": {
                            "email": contact.email
                        },
                        "substitution_data": {
                            "contact_name": contact.contact_name,
                            "receivable_name": receivable.name,
                            "remittance_date": receivable.remittance_date.strftime('%Y/%m/%d'),
                            "receivable_description": remittance.description,
                            "receivable_total": fmt_money(receivable.amount+receivable.extra+receivable.management_fee, precision=2),
                            "iban": receivable.iban[0:8] + "****" + receivable.iban[-4:],
                            "date": date_repayment,
                            "bank": receivable.bank,
                            "origin": receivable.quote_origin
                        }
                    }],
                    configuration=configuration,
                    template=template,
                    subject=subject,
                    attachments=[],
                    not_sent=[],
                    log_name=None,
                    log_item=log_item.name,
                    transactional=True)
        except Exception as error:
            frappe.log("Error al enviar notificacio: {0}".format(error))
            continue


@frappe.whitelist()
def send_mass_mail(content, filters, configuration, subject, template, file1=None, file2=None, file3=None, file4=None):
    not_current =[]
    email_blocked =[]

    if not template or template=="":
        frappe.throw(_("Configure Communications Template first"))
    else:

        log = frappe.new_doc("Communication Log")
        log.update({
            "date": datetime.now(),
            "communication_type": "Email",
            "user": frappe.session.user,
            "territory": frappe.db.get_value("User", frappe.session.user, "territory"),
            "sent_status": 0,
            "error_status": 0,
            "pending_status": 0,
            "content": content,
            "filters": filters
        })
        log.save()


        contacts = frappe.get_list("PolContact", filters=filters, fields=["name", "contact_status", "email_blocked",
                                                                          "full_name", "email", "surname1", "surname2",
                                                                          "address", "postal_code", "city", "contact_name"], distinct=True)
        recipients = []

        for contact in contacts:
            if contact["contact_status"] != "Current":
                not_current.append({
                    "name": contact["full_name"],
                    "email": contact["email"]
                })
                continue
            if contact["email_blocked"]:
                email_blocked.append({
                    "name": contact["full_name"],
                    "email": contact["email"]
                })
                continue

            recipients.append({
                "address": {
                    "email": contact["email"]
                },
                "substitution_data": {
                    "full_name": contact["full_name"] or "",
                    "contact_name": contact["contact_name"] or "",
                    "surname1": contact["surname1"] or "",
                    "surname2": contact["surname2"] or "",
                    "c_address": contact["address"] or "",
                    "postal_code": contact["postal_code"] or "",
                    "city": contact["city"] or ""
                }
            })

        if frappe.db.get_singles_dict("Sparkpost Configuration").mode_test == u"1":
            recipients = get_test_recipients()

        if len(recipients) == 0:
            frappe.throw(_("No contacts can receive emails"))

        not_sent = {
            "not_current":not_current,
            "email_blocked":email_blocked
        }

        attachments = []

        for item in [file1, file2, file3, file4]:
            if ";" not in item:
                array_path = item.split("/")
                path = frappe.get_site_path(*array_path)
                if os.path.isfile(path):
                    file = open(path,'r')
                    attachments.append(
                        {
                            "name": os.path.basename(path),
                            "type": magic.from_file(path, mime=True),
                            "data": base64.b64encode(file.read())
                        }
                    )
            else:
                name = item.split(";")[0].split(",")[0]
                type = item.split(";")[0].split(",")[1]
                data = item.split(";")[1].split(",")[1]

                attachments.append(
                    {
                        "name": name,
                        "type": type,
                        "data": data
                    }
                )

        send_mail(content=content,
                  recipients=recipients,
                  configuration=configuration,
                  template=template,
                  subject=subject,
                  attachments=attachments,
                  not_sent=not_sent,
                  log_name=log.name,
                  log_item=None,
                  transactional=False)

@frappe.whitelist()
def send_mail(content, recipients, configuration, template, subject, attachments, not_sent=None, transactional=True, log_name=None, log_item=None):

    configuration = frappe.get_doc("Email Configuration", configuration)
    template = frappe.get_doc("Email Template", template)

    sp = getSparkClient()

    try:
        cc=None
        if configuration.cc:
            cc=configuration.cc.split(",") if configuration.cc != u'' else []

        bcc=None
        if configuration.bcc:
            bcc=configuration.bcc.split(",") if configuration.bcc != u'' else []

        content_img, inline_images = parse_images(content)

        response = sp.transmissions.send(
            recipients=recipients,
            cc=cc,
            bcc=bcc,
            html=re.sub('{{\s*content\s*}}', unicode(content_img), template.template_html),
            text=re.sub('{{\s*content\s*}}', unicode(content_img), template.template_text),
            from_email=configuration.get_value("from"),
            subject=subject,
            description=subject,
            # custom_headers={
            #     'X-CUSTOM-HEADER': 'foo bar'
            # },
            track_opens=False,
            track_clicks=False,
            attachments=attachments,
            # campaign='sdk example',
            inline_images = inline_images,
            metadata={
                'user': frappe.session.user
            },
            substitution_data={
                'footer': configuration.footer
            },
            reply_to=configuration.reply_to,
            transactional=transactional
        )

        if log_name != None:
            log = frappe.get_doc("Communication Log", log_name)
            log.communication_id = response["id"]
            log.response = json.dumps(response)
            log.sent_status = int(response["total_accepted_recipients"])
            log.error_status = int(response["total_rejected_recipients"])
            log.save()

        if log_item != None:
            log = frappe.get_doc("Communication Log Item", log_item)

            if int(response["total_rejected_recipients"]) > 0:
                log.status = "Error"
                log.more_info = str(response["error"]["code"]) + "\n\n" + response["error"]["description"]
            else:
                log.status = "Sent"
            log.save()

        frappe.log("Sent mail to {0}".format(repr(recipients)))

        return {
            "recipients": recipients,
            "not_sent": not_sent
        }
    except Exception as error:
        frappe.log("Send Mail error {0}".format(error))
        frappe.throw("Error al enviar l'email: {0}".format(error), title="Error")

@frappe.whitelist()
def getdef_template():
    return frappe.db.get_singles_dict("Communications Configuration").communicate_template


def parse_images(content):
    inline_images = []
    soup = BeautifulSoup(content)
    images = soup.findAll('img')

    for image in images:
        if "data:" in image['src']:
            name = slugify(image["data-filename"])
            type = re.search('data.*\;',image['src']).group(0).replace("data:", "").replace(";","")
            data = image['src'].split(",")[1]
            inline_images.append({
                "type": type,
                "name": name,
                "data": data
            })

            image['src']= "cid:"+name


    return soup, inline_images
