import frappe
from frappe import _

def get_config_level(level, unpaid_configuration, receivable_type):
    higher_level = -1
    higher_config = None

    for config in unpaid_configuration.unpaid_configuration_table:
        if config.notification_number==level and config.receivable_type==receivable_type:
            return config.unpaid_email_template, config.unpaid_email_configuration, config.unpaid_content, config.subject
        elif config.receivable_type==receivable_type and config.notification_number>higher_level:
            higher_config = config
            higher_level = config.notification_number

    if higher_level!=-1:
        return higher_config.unpaid_email_template, higher_config.unpaid_email_configuration, higher_config.unpaid_content, higher_config.subject

    frappe.throw(_("Unpaid configuration level {0} for receivable type {1} not configured".format(level, receivable_type)))

def get_config_level_sms(level, unpaid_configuration, receivable_type):
    higher_level = -1
    higher_config = None

    for config in unpaid_configuration.sms_unpaid_configuration_table:
        if config.notification_number==level and config.receivable_type==receivable_type:
            return config.from_sms, config.content
        elif config.receivable_type==receivable_type and config.notification_number>higher_level:
            higher_config = config
            higher_level = config.notification_number

    if higher_level!=-1:
        return higher_config.from_sms, higher_config.content

    frappe.throw(_("SMS Unpaid configuration level {0} for receivable type {1} not configured".format(level, receivable_type)))
