// Copyright (c) 2016, Pau Rosello and contributors
// For license information, please see license.txt

frappe.ui.form.on('Communication Log', {
	refresh: function(frm) {
        var sent_status = 0;
        var error_status = 0;
        var pending_status = 0;

        if(frm.doc.communications_table.length>0){
                $.each(frm.doc.communications_table, function( index, log ) {
                      if(log.status=="Pending"){
                        pending_status+=1;
                      }
                      else if(log.status=="Error"){
                        error_status+=1;
                      }
                      else if(log.status=="Sent"){
                        sent_status+=1;
                      }
                });

                frm.set_value("sent_status", sent_status);
                frm.set_value("error_status", error_status);
                frm.set_value("pending_status", pending_status);
        }
	    if(frm.doc.communication_type=="SMS"){
            frm.set_value("sms_cost", sent_status*0.065);
        }
	}
});
