import cProfile

def profileit(name):
    def inner(func):
        def wrapper(*args, **kwargs):
            print("profiling")
            prof = cProfile.Profile()
            retval = prof.runcall(func, *args, **kwargs)
            prof.dump_stats(name)
            return retval
        return wrapper
    return inner