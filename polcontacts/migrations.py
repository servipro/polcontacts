import frappe
from frappe import _

# 09-02-2017 update regions for all contacts
def update_origin_dest_receivables():
    receivables = frappe.get_all("Receivable", {"receivable_type": "Special"}, ["name", "contact"])
    i=0
    total=len(receivables)
    for receivable in receivables:
        i+=1
        print str(i) + "/" + str(total)
        contact = frappe.get_doc("PolContact", receivable.contact)
        receivable = frappe.get_doc("Receivable", receivable.name)
        receivable.quote_origin = contact.quote_origin
        receivable.quote_destination = contact.quote_destination
        receivable.db_update()
    frappe.db.commit()

# 09-02-2017 update regions for all contacts
def update_metropolitan():
    contacts = frappe.get_all("PolContact", {}, ["name"])
    i=0
    total=len(contacts)
    for contact in contacts:
        i+=1
        print str(i) + "/" + str(total)
        contact = frappe.get_doc("PolContact", contact.name)
        territory = frappe.get_doc("Territory", contact.territory)
        contact.metropolitan_area = territory.metropolitan_area
        contact.db_update()
    frappe.db.commit()

# 09-02-2017 update regions for all contacts
def update_regions():
    contacts = frappe.get_all("PolContact", {}, ["name"])
    i=0
    total=len(contacts)
    for contact in contacts:
        i+=1
        print str(i) + "/" + str(total)
        contact = frappe.get_doc("PolContact", contact.name)
        contact.update_regions()
        contact.db_update()
    frappe.db.commit()

def update_mayus_name():
    contacts = frappe.get_all("PolContact", {}, ["name"])
    i=0
    total=len(contacts)
    for contact in contacts:
        i+=1
        print str(i) + "/" + str(total)
        contact = frappe.get_doc("PolContact", contact.name)
        contact.fix_name()
        contact.db_update()
    frappe.db.commit()

def update_minus_email():
    contacts = frappe.get_all("PolContact", {}, ["name"])
    i=0
    total=len(contacts)
    for contact in contacts:
        i+=1
        print str(i) + "/" + str(total)
        contact = frappe.get_doc("PolContact", contact.name)
        contact.email_lower()
        contact.db_update()
    frappe.db.commit()

def update_remm_amounts():
    remitances = frappe.get_all("Remittance", {}, ["name"])
    i=0
    total=len(remitances)
    for remitance in remitances:
        i+=1
        print str(i) + "/" + str(total)
        remitance = frappe.get_doc("Remittance", remitance.name)
        remitance.update_totals()
        remitance.db_update()
    frappe.db.commit()