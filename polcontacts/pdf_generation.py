import frappe
from frappe.utils.print_format import download_pdf
import json
from frappe import _, ValidationError


@frappe.whitelist()
def download_pdf_custom(doctype, name, format=None, doc=None):
    if doctype in ["Scoring Template"]:
        document = frappe.get_doc(doctype, name)
        frappe.local.response.filename = u"{name}.pdf".format(name=name.replace(" ", "-").replace("/", "-"))
        frappe.local.response.filecontent = document.gen_pdf()
        frappe.local.response.type = "download"
    else:
        download_pdf(doctype, name, format, doc)

@frappe.whitelist()
def pdf_doc_function(doctype, name, function, arguments, donwload_name):
    document = frappe.get_doc(doctype, name)
    try:
        pdf_function = getattr(document, function)
        frappe.local.response.filename = u"{name}.pdf".format(name=donwload_name.replace(" ", "-").replace("/", "-"))
        frappe.local.response.filecontent = pdf_function(json.loads(arguments))
        frappe.local.response.type = "download"
    except ValidationError as e:
        return frappe.respond_as_web_page(_("Report Error"),
                                   e.message,
                                   http_status_code=404, indicator_color='red')