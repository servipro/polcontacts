from __future__ import unicode_literals

import frappe
from frappe import _


def get_data():
    items = [
        {
            "label": _("Contact Management"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("PolContact"),
                    "name": "PolContact"
                },
                {
                    "type": "doctype",
                    "label": _("Contact Membership Request"),
                    "name": "Contact Membership Request"
                },
                {
                    "type": "doctype",
                    "label": _("Contact Modification Request"),
                    "name": "Contact Modification Request"
                },
            ]
        },
        {
            "label": _("Quote Management"),
            "items": [
                {
                    "type": "doctype",
                    "name": "Contact Fee",
                    "label": _("Contact Fees"),
                },
                {
                    "type": "doctype",
                    "label": _("Bank Account"),
                    "name": "Bank Account",
                },
                {
                    "type": "doctype",
                    "label": _("Receivable"),
                    "name": "Receivable"
                },
                {
                    "type": "doctype",
                    "name": "Generate Remittance",
                    "label": _("Generate Remittance"),
                },
                {
                    "type": "doctype",
                    "name": "Generate Extra Remittance",
                    "label": _("Generate Extra Remittance"),
                },
                {
                    "type": "doctype",
                    "label": _("Remittance"),
                    "name": "Remittance"
                },
                {
                    "type": "doctype",
                    "label": _("Quote Origin"),
                    "name": "Quote Origin"
                },
                {
                    "type": "doctype",
                    "label": _("Quote Destination"),
                    "name": "Quote Destination"
                },
                {
                    "type": "page",
                    "name": "import_unpaid",
                    "label": _("Import Unpaid")
                }
            ]
        },
        {
            "label": _("Auxiliar Documents"),
            "items": [
                {
                    "type": "doctype",
                    "name": "Territory",
                    "label": _("Territory"),
                },
                {
                    "type": "doctype",
                    "name": "Neighborhood",
                    "label": _("Neighborhood"),
                },
                {
                    "type": "doctype",
                    "name": "Electoral District",
                    "label": _("Electoral District"),
                },
                {
                    "type": "doctype",
                    "name": "Jurisdiction",
                    "label": _("Jurisdiction"),
                },
                {
                    "type": "doctype",
                    "name": "Job",
                    "label": _("Job"),
                },
                {
                    "type": "doctype",
                    "name": "Studies",
                    "label": _("Studies"),
                },
                {
                    "type": "doctype",
                    "name": "Studies Level",
                    "label": _("Studies Level"),
                },
                {
                    "type": "doctype",
                    "name": "Laboral Experience",
                    "label": _("Laboral Experience"),
                },
                {
                    "type": "doctype",
                    "name": "Entity",
                    "label": _("Entity"),
                },
                {
                    "type": "doctype",
                    "name": "Voluntary Type",
                    "label": _("Voluntary Type"),
                },
                {
                    "type": "doctype",
                    "name": "Bank",
                    "label": _("Bank"),
                },
                {
                    "type": "doctype",
                    "name": "Role Political",
                    "label": _("Role Political"),
                }
            ]
        },
        {
            "label": _("Reports"),
            "items": [
                {
                    "type": "report",
                    "doctype": "Receivable",
                    "is_query_report": True,
                    "name": "Remittance Receivables"
                },
                {
                    "type": "report",
                    "doctype": "Receivable",
                    "is_query_report": True,
                    "name": "Cash Receivables"
                },
                {
                    "type": "report",
                    "doctype": "Receivable",
                    "is_query_report": True,
                    "name": "Unpaid Receivables"
                },
                {
                    "type": "report",
                    "doctype": "Receivable",
                    "is_query_report": True,
                    "name": "Aportacions"
                },
                {
                    "type": "report",
                    "doctype": "PolContact",
                    "is_query_report": True,
                    "name": "Unpaid Contacts"
                },
                {
                    "type": "report",
                    "doctype": "PolContact",
                    "is_query_report": True,
                    "name": "Contact Status"
                },
                {
                    "type": "report",
                    "doctype": "PolContact",
                    "is_query_report": True,
                    "name": "Membership Status"
                },
                {
                    "type": "report",
                    "doctype": "PolContact",
                    "is_query_report": True,
                    "name": "Voting Right"
                },
                {
                    "type": "report",
                    "doctype": "PolContact",
                    "is_query_report": True,
                    "name": "Electronic Voting Right"
                },
                {
                    "type": "report",
                    "doctype": "Territory",
                    "is_query_report": True,
                    "name": "Territory Contact Status"
                },
                {
                    "type": "report",
                    "doctype": "PolContact",
                    "is_query_report": True,
                    "name": "Contacts List"
                },
                {
                    "type": "report",
                    "doctype": "Receivable",
                    "is_query_report": True,
                    "name": "Fees and Extra Fees"
                },
                {
                    "type": "report",
                    "doctype": "Receivable",
                    "is_query_report": True,
                    "name": "Model 182"
                },
                {
                    "type": "report",
                    "doctype": "Receivable",
                    "is_query_report": True,
                    "name": "Rebuts impagats contactes baixa"
                },
                {
                    "type": "report",
                    "doctype": "Budget Overview",
                    "is_query_report": True,
                    "label": _("Budget Overview"),
                    "name": "Budget Overview"
                }
            ]
        },
        {
            "label": _("Stickers Configuration"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Postal Stickers Configuration"),
                    "name": "Postal Stickers Configuration"
                },
            ]
        }
    ]

    if not "SIGEAS Configuration" in frappe.get_roles():
        items = list(filter(lambda x: x["label"]!= _("Auxiliar Documents"), items))

    if  not "SIGEAS Economic" in frappe.get_roles() and not "SIGEAS Economic ReadOnly" in frappe.get_roles():
        items = list(filter(lambda x: x["label"]!= _("Quote Management"), items))

    return items