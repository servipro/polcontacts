from __future__ import unicode_literals

import frappe
from frappe import _


def get_data():
    items = [
        {
            "label": _("Documents"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Budget Management Settings"),
                    "name": "Budget Management Settings"
                },
                {
                    "type": "doctype",
                    "label": _("Territory Code"),
                    "name": "Territory Code"
                },
                {
                    "type": "doctype",
                    "label": _("Budget Allocation"),
                    "name": "Budget Allocation"
                },
                {
                    "type": "doctype",
                    "label": _("Budget"),
                    "name": "Budget"
                },
                {
                    "type": "doctype",
                    "label": _("Spent Budget"),
                    "name": "Spent Budget"
                },
                {
                    "type": "doctype",
                    "label": _("Spent Budget Import"),
                    "name": "Spent Budget Import"
                }
            ]
        },
        {
            "label": _("Reports"),
            "items": [
                {
                    "type": "report",
                    "doctype": "Spent Budget",
                    "is_query_report": True,
                    "label": _("Detailed Budget by Region"),
                    "name": "Detailed Budget by Region"
                },
                {
                    "type": "report",
                    "doctype": "Spent Budget",
                    "is_query_report": True,
                    "label": _("Simple Budget by Region"),
                    "name": "Simple Budget by Region"
                },
                {
                    "type": "report",
                    "doctype": "Budget Overview",
                    "is_query_report": True,
                    "label": _("Budget Overview"),
                    "name": "Budget Overview"
                },
            ]
        },
    ]

    return items