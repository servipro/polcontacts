# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
    return [
        {
            "module_name": "Pol Contacts",
            "color": "#226C99",
            "icon": "octicon octicon-person",
            "type": "module",
            "label": _("Pol Contacts")
        },
        {
            "module_name": "Budget Management",
            "color": "#AA42F4",
            "icon": "octicon octicon-inbox",
            "type": "module",
            "label": _("Budget Management")
        },
        {
            "module_name": "PolContact",
            "_doctype": "PolContact",
            "color": "green",
            "icon": "octicon octicon-person",
            "type": "link",
            "link": "List/PolContact",
            "label": _("PolContact")
        },
        {
            "module_name": "Receivable",
            "_doctype": "Receivable",
            "color": "brown",
            "icon": "octicon octicon-briefcase",
            "type": "link",
            "link": "List/Receivable",
            "label": _("Receivable")
        },
        {
            "module_name": "Remittance",
            "_doctype": "Remittance",
            "color": "orange",
            "icon": "octicon octicon-repo",
            "type": "link",
            "link": "List/Remittance",
            "label": _("Remittance")
        },
        {
            "module_name": "Communications",
            "color": "indigo",
            "icon": "octicon octicon-broadcast",
            "type": "module",
            "label": _("Communications")
        }
    ]
