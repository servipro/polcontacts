from __future__ import unicode_literals

import frappe
from frappe import _


def get_data():
    items = [
        {
            "label": _("Communications"),
            "items": [
                {
                    "type": "page",
                    "name": "new_email",
                    "label": _("New Email")
                },
                {
                    "type": "page",
                    "name": "new_telegram",
                    "label": _("New Telegram")
                },
                {
                    "type": "page",
                    "name": "new_sms",
                    "label": _("New SMS")
                }
            ]
        },
        {
            "label": _("Auxiliar Documents"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Email Configuration"),
                    "name": "Email Configuration"
                },
                {
                    "type": "doctype",
                    "label": _("Email Template"),
                    "name": "Email Template"
                },
                {
                    "type": "doctype",
                    "label": _("Communication Log"),
                    "name": "Communication Log"
                }
            ]
        },
        {
            "label": _("Configuration"),
            "items": [
                {
                    "type": "doctype",
                    "label": _("Communications Configuration"),
                    "name": "Communications Configuration"
                },
                {
                    "type": "doctype",
                    "label": _("SMS Configuration"),
                    "name": "SMS Configuration"
                }
            ]
        },
    ]

    if not "SIGEAS Communications Manager" in frappe.get_roles():
        items = list(filter(lambda x: x["label"]!= _("Auxiliar Documents"), items))
        items = list(filter(lambda x: x["label"]!= _("Configuration"), items))

    return items