/**
 * Created by pau on 19/10/2016.
 */


frappe.provide('frappe.utils');

$.extend(frappe.utils, {
	guess_style: function(text, default_style, _colour) {
		var style = default_style || "default";
		var colour = "darkgrey";
		if(text) {
			if(has_words(["Pending", "Review", "Medium", "Not Approved", "En Curs", "Unsubscribe Process"], text)) {
				style = "warning";
				colour = "orange";
			} else if(has_words(["Open", "Urgent", "High", "Baixa", "Unsubscribed"], text)) {
				style = "danger";
				colour = "red";
			} else if(has_words(["Closed", "Finished", "Converted", "Completed", "Confirmed",
					"Approved", "Yes", "Active", "Available", "Paid", "Current"], text)) {
				style = "success";
				colour = "green";
			} else if(has_words(["Submitted"], text)) {
				style = "info";
				colour = "blue";
			}
		}
		return _colour ? colour : style;
	}
});

if(!window.polcontacts)
	window.polcontacts = {};

frappe.provide("polcontacts.utils");

$.extend(polcontacts.utils, {
	set_contact_indicators: function (frm) {
		if (frm.doc.__onload && frm.doc.__onload.dashboard_info) {
			var info = frm.doc.__onload.dashboard_info;
			if(info.total_ordinary>0){
			    if ($.inArray("SIGEAS Nacional", frappe.user_roles)>=0){
                    frm.dashboard.add_indicator(__('Total Unpaid Ordinary: {0} €',
                        [format_currency(info.total_ordinary, frm.doc.default_currency)]),
                        info.total_ordinary ? 'orange' : 'green');
                }
                else{
                    frm.dashboard.add_indicator(__('Unpaid Receivables Ordinary'),'orange');
                }
			}
			else{
			    frm.dashboard.add_indicator(__('No Unpaid Receivables Ordinary'), 'green');
			}

			if(info.total_special>0){
			    if ($.inArray( "SIGEAS Special Fee", frappe.user_roles)>=0){
                    frm.dashboard.add_indicator(__('Total Unpaid Special: {0} €',
                        [format_currency(info.total_special, frm.doc.default_currency)]),
                        info.total_special ? 'orange' : 'green');
                }
                else if ($.inArray( "SIGEAS Economic", frappe.user_roles)>=0){
                    frm.dashboard.add_indicator(__('Unpaid Receivables Special'),'orange');
                }
			}

		}
	},
	has_value: function(input){
		return typeof input!='undefined' && input!=null && input!="";
	}
});

//frappe.ui.FilterList = frappe.ui.FilterList.extend({
//    add_filter: function(doctype, fieldname, condition, value, hidden) {
//
//    }
//})

frappe.form.link_formatters['PolContact'] = function(value, doc) {
	if(doc && doc.full_name) {
		return doc.full_name;
	} else {
		return value;
	}
};

frappe.form.link_formatters['Entity'] = function(value, doc) {
	if(doc && doc.entity_name) {
		return doc.entity_name;
	} else {
		return value;
	}
};