frappe.provide("polcontacts.validation");

$.extend(polcontacts.validation, {
    dni: function(value){
        var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
        var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var str = value.toString().toUpperCase();

        var isNIF = nifRexp.test(str);
        var isNIE = nieRexp.test(str);
        if (!isNIF && !isNIE) return false;

        var nie = str
          .replace(/^[X]/, '0')
          .replace(/^[Y]/, '1')
          .replace(/^[Z]/, '2');

        var letter = str.substr(-1);
        var charIndex = parseInt(nie.substr(0, 8)) % 23;

        if (validChars.charAt(charIndex) === letter) {
          return true;
        }

        return false;
    },
    cif: function(cif){
        //Quitamos el primer caracter y el ultimo digito
        var valueCif=cif.substr(1,cif.length-2);

        var suma=0;

        //Sumamos las cifras pares de la cadena
        for(i=1;i<valueCif.length;i=i+2)
        {
            suma=suma+parseInt(valueCif.substr(i,1));
        }

        var suma2=0;

        //Sumamos las cifras impares de la cadena
        for(i=0;i<valueCif.length;i=i+2)
        {
            result=parseInt(valueCif.substr(i,1))*2;
            if(String(result).length==1)
            {
                // Un solo caracter
                suma2=suma2+parseInt(result);
            }else{
                // Dos caracteres. Los sumamos...
                suma2=suma2+parseInt(String(result).substr(0,1))+parseInt(String(result).substr(1,1));
            }
        }

        // Sumamos las dos sumas que hemos realizado
        suma=suma+suma2;

        var unidad=String(suma).substr(1,1)
        unidad=10-parseInt(unidad);

        var primerCaracter=cif.substr(0,1).toUpperCase();

        if(primerCaracter.match(/^[FJKNPQRSUVW]$/))
        {
            //Empieza por .... Comparamos la ultima letra
            if(String.fromCharCode(64+unidad).toUpperCase()==cif.substr(cif.length-1,1).toUpperCase())
                return true;
        }else if(primerCaracter.match(/^[ABCDEFGHLM]$/)){
            //Se revisa que el ultimo valor coincida con el calculo
            if(unidad==10)
                unidad=0;
            if(cif.substr(cif.length-1,1)==String(unidad))
                return true;
        }else{
            //Se valida como un dni
            return polcontacts.validation.dni(cif);
        }
        return false;
    },
    iban: function(value){
        return SEPA.validateIBAN(value);
    },
    sepa: function(value){
        return SEPA.validateCreditorID(value);
    }
})