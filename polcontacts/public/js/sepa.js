/* https://github.com/kewisch/sepa.js
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * Portions Copyright (C) Philipp Kewisch, 2014-2015 */

/**
 * This is sepa.js. Its module exports the following functions:
 *
 * SEPA.Document               -- class for creating SEPA XML Documents
 * SEPA.PaymentInfo            -- class for SEPA payment information blocks
 * SEPA.Transaction            -- class for generic transactions
 * SEPA.validateIBAN           -- function to validate an IBAN
 * SEPA.checksumIBAN           -- function to calculate the IBAN checksum
 * SEPA.validateCreditorID     -- function to validate a creditor id
 * SEPA.checksumCreditorID     -- function to calculate the creditor id checksum
 * SEPA.setIDSeparator         -- function to customize the ID separator when needed (defaults to '.')
 * SEPA.enableValidations      -- function to enable/disable fields validation
 */
(function(exports) {

  /**
   * Replace letters with numbers using the SEPA scheme A=10, B=11, ...
   * Non-alphanumerical characters are dropped.
   *
   * @param str     The alphanumerical input string
   * @return        The input string with letters replaced
   */
  function _replaceChars(str) {
    var res = '';
    for (var i = 0, l = str.length; i < l; ++i) {
      var cc = str.charCodeAt(i);
      if (cc >= 65 && cc <= 90) {
        res += (cc - 55).toString();
      } else if (cc >= 97 && cc <= 122) {
        res += (cc - 87).toString();
      } else if (cc >= 48 && cc <= 57) {
        res += str[i];
      }
    }
    return res;
  }

  /**
   * mod97 function for large numbers
   *
   * @param str     The number as a string.
   * @return        The number mod 97.
   */
  function _txtMod97(str) {
    var res = 0;
    for (var i = 0, l = str.length; i < l; ++i) {
      res = (res * 10 + parseInt(str[i], 10)) % 97;
    }
    return res;
  }

  /**
   * Checks if an IBAN is valid (no country specific checks are done).
   *
   * @param iban        The IBAN to check.
   * @return            True, if the IBAN is valid.
   */
  function validateIBAN(iban) {
    var ibrev = iban.substr(4) + iban.substr(0, 4);
    return _txtMod97(_replaceChars(ibrev)) === 1;
  }

  /**
   * Calculates the checksum for the given IBAN. The input IBAN should pass 00
   * as the checksum digits, a full iban with the corrected checksum will be
   * returned.
   *
   * Example: DE00123456781234567890 -> DE87123456781234567890
   *
   * @param iban        The IBAN to calculate the checksum for.
   * @return            The corrected IBAN.
   */
  function checksumIBAN(iban) {
    var ibrev = iban.substr(4) + iban.substr(0, 2) + '00';
    var mod = _txtMod97(_replaceChars(ibrev));
    return iban.substr(0, 2) + ('0' + (98 - mod)).substr(-2,2) + iban.substr(4);
  }

  /**
   * Checks if a Creditor ID is valid (no country specific checks are done).
   *
   * @param iban        The Creditor ID to check.
   * @return            True, if the Creditor IDis valid.
   */
  function validateCreditorID(cid) {
    var cidrev = cid.substr(7) + cid.substr(0, 4);
    return _txtMod97(_replaceChars(cidrev)) === 1;
  }

  /**
   * Calculates the checksum for the given Creditor ID . The input Creditor ID
   * should pass 00 as the checksum digits, a full Creditor ID with the
   * corrected checksum will be returned.
   *
   * Example: DE00ZZZ09999999999 -> DE98ZZZ09999999999
   *
   * @param iban        The IBAN to calculate the checksum for.
   * @return            The corrected IBAN.
   */
  function checksumCreditorID(cid) {
    var cidrev = cid.substr(7) + cid.substr(0, 2) + '00';
    var mod = _txtMod97(_replaceChars(cidrev));
    return cid.substr(0, 2) + ('0' + (98 - mod)).substr(-2,2) + cid.substr(4);
  }


  exports.validateIBAN           = validateIBAN;
  exports.checksumIBAN           = checksumIBAN;
  exports.validateCreditorID     = validateCreditorID;
  exports.checksumCreditorID     = checksumCreditorID;

})(typeof exports === 'undefined' ? SEPA = {} : exports);