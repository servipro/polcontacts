import frappe

def execute():
	print "Migrating social"
	
	frappe.db.sql("""
		UPDATE `tabContact Interests`
		SET field_of_interest = "Social Diversity and Citizenship"
		WHERE field_of_interest = "Social"
	""")
	
	print "migration done"