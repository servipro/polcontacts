import frappe

def execute():
	frappe.reload_doc("Pol Contacts", "doctype", "PolContact")
	
	frappe.db.sql("""
		UPDATE tabPolContact
		SET ordinary_fee_start = membership_date
	""")
	
	print "migration done"