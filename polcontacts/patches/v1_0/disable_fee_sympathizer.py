import frappe

def execute():
    contacts = frappe.get_all("PolContact", fields=["name"], filters=[["category","=","Sympathizer"], ["ordinary_fee","not like",""]])
    total = str(len(contacts))
    i=0
    for contact_name in contacts:
        contact = frappe.get_doc("PolContact", contact_name.name)
        contact.ordinary_fee = ""
        contact.db_update()
        i+=1
        print str(i)+"/"+total
