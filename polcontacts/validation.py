# -*- coding: latin-1 -*-
import string

def validDNI(dni):
    tabla = "TRWAGMYFPDXBNJZSQVHLCKE"
    dig_ext = "XYZ"
    reemp_dig_ext = {'X':'0', 'Y':'1', 'Z':'2'}
    numeros = "1234567890"
    dni = dni.upper()
    if len(dni) == 9:
        dig_control = dni[8]
        dni = dni[:8]
        if dni[0] in dig_ext:
            dni = dni.replace(dni[0], reemp_dig_ext[dni[0]])
        return len(dni) == len([n for n in dni if n in numeros]) \
            and tabla[int(dni)%23] == dig_control
    return False


def valid_account_number(iban):
    cuenta=iban[14:24]
    entidad=iban[4:8]
    oficina=iban[8:12]
    dc=iban[12:14]

    def proc(digitos):
        resultado = 11 - sum(int(d) * 2 ** i for i, d in enumerate(digitos)) % 11
        return resultado if resultado < 10 else 11 - resultado

    return '%d%d' % (proc('00' + entidad + oficina), proc(cuenta)) == dc