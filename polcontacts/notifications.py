import frappe

def get_notification_config():
	return {
		"for_doctype": {
			"Contact Modification Request": {"status": "Pending"},
			"Contact Membership Request": {"status": "Ongoing"},
            "Receivable": {
                "status_cash": ("not in", ("Paid", "Stopped")),
                "status_invoice": ("not in", ("Wallet", "Remittance", "Stopped"))
            },
        }
	}