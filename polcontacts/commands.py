# -*- coding: utf-8 -*-
import click
import frappe
from frappe.commands import pass_context, get_site
import datetime
from unidecode import unidecode

@click.command('fix_territory_receivables')
@pass_context
@click.option('--commit', default=False, is_flag=True)
def fix_territory_receivables(context, commit=False):
    site = get_site(context)
    print site
    frappe.connect(site=site)

    modified = frappe.db.sql("""
        update tabReceivable rec
        inner join tabPolContact con on
            rec.contact = con.name
        set rec.territory = con.territory
        WHERE rec.territory is null
    """)

    print modified

    if commit:
        frappe.db.commit()
    frappe.destroy()

@click.command('import_carrecs')
@pass_context
@click.option('--file', default=False)
@click.option('--commit', default=False, is_flag=True)
def import_carrecs(context, file=False, commit=False):
    site = get_site(context)
    print site
    frappe.connect(site=site)

    import csv

    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for item in csv_reader:
            try:
                contact = frappe.get_doc("PolContact", item[0])
                contact.electoral_list.pop()
                contact.save()
            except frappe.exceptions.DoesNotExistError:
                print(item[0] + " does not exist")


    if commit:
        frappe.db.commit()
    frappe.destroy()

@click.command('importar_llista_electoral')
@pass_context
@click.option('--file', default=False)
@click.option('--commit', default=False, is_flag=True)
def importar_llista_electoral(context, file=False, commit=False):
    site = get_site(context)
    print site
    frappe.connect(site=site)

    importar_llista_electoral_fn(file, commit)
    frappe.destroy()

def importar_llista_electoral_fn(file, commit):
    import unicodecsv

    with open(file) as csv_file:
        csv_reader = unicodecsv.reader(csv_file, delimiter=',')
        for item in csv_reader:
            if not frappe.db.exists('PolContact', item[0]):
                print(item[0] + " does not exist")
                continue
            contact = frappe.get_doc("PolContact", item[0])
            contact.append("political_responsibility", {
                "city": item[2],
                "initial_date": "2019-11-23",
                "position": "Sense representació municipal",
                "type": "Institucional - Chosen"
            })
            print(contact.dni)
            contact.save()
    if commit:
        frappe.db.commit()

@click.command('importar_carrecs_nous')
@pass_context
@click.option('--file', default=False)
@click.option('--commit', default=False, is_flag=True)
def importar_carrecs_nous(context, file=False, commit=False):
    site = get_site(context)
    print site
    frappe.connect(site=site)

    importar_carrecs_nous_fn(file, commit)
    frappe.destroy()

def importar_carrecs_nous_fn(file, commit):
    import unicodecsv

    with open(file) as csv_file:
        csv_reader = unicodecsv.reader(csv_file, delimiter=',')
        for item in csv_reader:
            #linea 172 falta dni

            if frappe.db.exists('PolContact', item[0]):
                print(item[0] + " already exists")
                continue

            sex_map = item[13]
            if sex_map == "H":
                sex_map = "Male"
            elif sex_map == "D":
                sex_map = "Female"
            else:
                sex_map = "Male"

            contact = frappe.new_doc("PolContact")
            contact.dni = item[0]
            contact.membership_date = "2019-11-23"
            contact.contact_status = "Current"
            contact.category = "External"
            contact.contact_name = unidecode(item[1])
            contact.surname1 = unidecode(item[2])
            contact.surname2 = unidecode(item[3])
            contact.address = item[9]
            contact.postal_code = item[10]
            contact.city = item[11]
            contact.address_blocked = False
            contact.territory = item[6]
            contact.phone= item[14]
            contact.phone_blocked= False
            contact.mobile_phone= item[13]
            contact.mobile_phone_blocked= False
            contact.email= item[5]
            contact.email_blocked= False
            contact.sex=sex_map
            contact.birthdate= datetime.datetime(1990, 1, 1)
            
            print(contact.dni)
            contact.save()
    if commit:
        frappe.db.commit()

@click.command('aprobar_carrecs_nous')
@pass_context
@click.option('--file', default=False)
@click.option('--commit', default=False, is_flag=True)
def aprobar_carrecs_nous(context, file=False, commit=False):
    site = get_site(context)
    print site
    frappe.connect(site=site)

    import unicodecsv

    with open(file) as csv_file:
        csv_reader = unicodecsv.reader(csv_file, delimiter=',')
        for item in csv_reader:
            contact = frappe.get_doc("PolContact", item[0])
            contact.contact_status = "Current"
            contact.db_update()

            pet_alta = frappe.get_list("Contact Membership Request", filters={"contact": item[0]})
            for item in pet_alta:
                if item:
                    frappe.delete_doc("Contact Membership Request", item.name)
    if commit:
        frappe.db.commit()
commands = [fix_territory_receivables, import_carrecs, importar_carrecs_nous, aprobar_carrecs_nous, importar_llista_electoral]
